package delta

import delta.Delta.*
import io.circe.Json
import io.circe.Json.obj
import io.circe.syntax.*
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import uk.co.unclealex.days.DSL
import uk.co.unclealex.days.Syntax.*

import java.nio.file.Paths
import java.time.ZoneId

class DeltaSpec extends AnyWordSpec with Matchers with DSL {

  private val track: Track = Track(
    trackNumber = 1,
    totalTracks = 8,
    title = "Radio GaGa",
    album = "The Works",
    artist = "Queen"
  )

  private val trackJson: Json = obj(
    "trackNumber" -> 1.asJson,
    "totalTracks" -> 8.asJson,
    "title" -> "Radio GaGa".asJson,
    "album" -> "The Works".asJson,
    "artist" -> "Queen".asJson
  )

  val removeFileDelta: Delta = RemoveFileDelta(
    path = Paths.get("path"),
    ordering = 1,
    track = Some(track),
    summaryPath = Paths.get("summaryPath"),
    last = true
  )

  val removeFileDeltaJson: Json = obj(
    "path" -> "path".asJson,
    "ordering" -> 1.asJson,
    "track" -> trackJson,
    "summaryPath" -> "summaryPath".asJson,
    "last" -> true.asJson,
    "type" -> "removeFile".asJson
  )

  "A remove file delta" should {
    "serialise correctly" in {
      removeFileDelta.asJson shouldEqual removeFileDeltaJson
    }
    "deserialise correctly" in {
       removeFileDeltaJson.as[Delta] shouldEqual Right(removeFileDelta)
    }
  }

  implicit val zoneId: ZoneId = ZoneId.of("GMT")

  val createFileDelta: Delta = CreateFileDelta(
    lastSynchronised = September(5, 1972).at(9._12.am),
    path = Paths.get("path"),
    url = "https://example.com/path",
    mimeType = "audio/mp3",
    ordering = 1,
    track =
      track,
    summaryPath = Paths.get("summaryPath"),
    last = true
  )
  
  val createFileDeltaJson: Json = obj(
    "lastSynchronised" -> "1972-09-05T09:12:00Z".asJson,
    "path" -> "path".asJson,
    "url" -> "https://example.com/path".asJson,
    "mimeType" -> "audio/mp3".asJson,
    "ordering" -> 1.asJson,
    "track" -> trackJson,
    "summaryPath" -> "summaryPath".asJson,
    "last" -> true.asJson,
    "type" -> "createFile".asJson
  )

  "A create file delta" should {
    "serialise correctly" in {
      createFileDelta.asJson shouldEqual createFileDeltaJson
    }
    "deserialise correctly" in {
      createFileDeltaJson.as[Delta] shouldEqual Right(createFileDelta)
    }
  }
}
