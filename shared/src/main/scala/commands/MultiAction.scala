/*
 * Copyright 2017 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package commands

import enumeratum.{Enum, EnumEntry}
import io.circe.Codec
import json.*
import uk.co.unclealex.stringlike.StringLike

import scala.collection.immutable.IndexedSeq

/**
  * The different ways of handling a multi disc album.
  */
sealed trait MultiAction extends EnumEntry {
  val token: String
}

/**
  * The different types of repository that clients need to understand.
  */
object MultiAction extends Enum[MultiAction] {

  /**
    * An object that indicates multi disc albums should be split in two with all extra disks combined as one.
    */
  case object Extras extends MultiAction {
    override val token: String = "extras"
  }

  /**
   * An object that indicates multi disc albums should be split with all extra disks renamed with either
   * a subtitle or a disk number post-pended.
   */
  case object Rename extends MultiAction {
    override val token: String = "rename"
  }

  /**
    * An object that indicates multi disc albums should be joined.
    */
  case object Join extends MultiAction {
    override val token: String = "join"
  }

  val values: IndexedSeq[MultiAction] = findValues

  def fromToken(token: String): Either[String, MultiAction] = {
    values.find(v => v.token == token).toRight(s"$token is not a valid multi action")
  }

  given StringLike[MultiAction] = StringLike.fromEither(_.token, fromToken)
  
  given Codec[MultiAction] = Codec.derived[MultiAction]
}
