package commands

import io.circe.generic.semiauto._
import io.circe.syntax._
import io.circe.{Codec, Decoder, Encoder}

sealed trait CommandResponse
case class MessageResponse(message: String) extends CommandResponse
case class KeepAliveResponse(keepAlive: Boolean = true) extends CommandResponse

object CommandResponse {
  implicit val commandResponseCodec: Codec[CommandResponse] = {
    implicit val messageResponseEncoder: Encoder[MessageResponse] = deriveEncoder[MessageResponse]
    implicit val keepAliveEncoder: Encoder[KeepAliveResponse] = deriveEncoder[KeepAliveResponse]
    implicit val messageResponseDecoder: Decoder[MessageResponse] = deriveDecoder[MessageResponse]
    implicit val keepAliveDecoder: Decoder[KeepAliveResponse] = deriveDecoder[KeepAliveResponse]

    val encoder: Encoder[CommandResponse] = {
      case mr: MessageResponse => mr.asJson
      case kar: KeepAliveResponse => kar.asJson
    }

    val decoder: Decoder[CommandResponse] =
      Decoder[MessageResponse].map(mr => mr).or(Decoder[KeepAliveResponse].map(kar => kar))

    Codec.from(decoder, encoder)
  }

}

