/*
 * Copyright 2017 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package commands

import io.circe.Decoder.Result
import io.circe.derivation.{Configuration, ConfiguredCodec}
import io.circe.generic.*
import io.circe.{ACursor, Codec, Decoder, Encoder}

import java.nio.file.Path
import scala.collection.SortedMap
import json.*
import json.given
import uk.co.unclealex.stringlike.StringLikeJsonSupport.given

/**
  * A marker trait for server command parameters. Each command parameter object is an RPC style JSON payload that
  * describes what command should be executed on the server and also includes the arguments that should be sent
  * to the command.
  **/
sealed trait Command

/**
  * The `checkin` command
  * @param relativeDirectories A list of paths relative to the staging repository.
  * @param owners A list of owners who will be added to the files.
  * @param allowUnowned A flag to indicate whether files without owners can be checked in.
  */
case class CheckinCommand(
                           relativeDirectories: Seq[PathAndRepository] = Seq.empty,
                           owners: Seq[String] = Seq.empty,
                           allowUnowned: Boolean = false) extends Command

/**
  * The `checkout` command
  * @param relativeDirectories A list of paths relative to the flac repository.
  */
case class CheckoutCommand(
                            relativeDirectories: Seq[PathAndRepository] = Seq.empty) extends Command

/**
 * The `checkout` command
 * @param relativeDirectories A list of paths relative to the flac repository.
 */
case class JoinCommand(
                            relativeDirectories: Seq[PathAndRepository] = Seq.empty) extends Command

/**
  * The `own` command
  * @param relativeDirectories A list of paths relative to the staging or flac repository.
  * @param owners The names of the users who will own the albums.
  */
case class OwnCommand(
                       relativeDirectories: Seq[PathAndRepository] = Seq.empty,
                       owners: Seq[String] = Seq.empty) extends Command

/**
  * The `unown` command
  * @param relativeDirectories A list of paths relative to the staging or flac repository.
  * @param owners The names of the users who will no longer own the albums.
  */
case class UnownCommand(relativeDirectories: Seq[PathAndRepository] = Seq.empty,
                        owners: Seq[String] = Seq.empty) extends Command

/**
  * The `multidisc` command
  * @param relativeDirectories A list of paths relative to the staging repository.
  * @param maybeMultiAction An enumeration used to decide whether multi disc albums should be split or joined.
  */
case class MultiDiscCommand(relativeDirectories: Seq[PathAndRepository] = Seq.empty,
                            maybeMultiAction: Option[MultiAction] = None) extends Command

/**
  * The `remove device` command
  * @param paths The root directories of the devices keyed by their device identifier.
  */
case class SynchroniseCommand(paths: SortedMap[String, Path] = SortedMap.empty) extends Command

/**
 * Yhe `health check` command
 */
case class HealthCheckCommand() extends Command


/**
  * JSON serialisers and deserialisers.
  */
object Command {
  private given Configuration =
    Configuration.default.withDiscriminator("command").copy(transformConstructorNames = _.replace("Command", "").toLowerCase()

    )

  given Encoder[SortedMap[String, Path]] = Encoder[Map[String, Path]].contramap(_.toMap)
  given Codec[Command] = ConfiguredCodec.derived[Command]
}
