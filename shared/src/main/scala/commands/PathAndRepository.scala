package commands

import io.circe._
import io.circe.generic.semiauto._
import json.given 
import java.nio.file.Path
import uk.co.unclealex.stringlike.StringLikeJsonSupport.given 

/** A class to hold a relative path an the repository it is relative to.
  *
  * @param path The relative path.
  * @param repositoryType The type of repository the path is relative to.
  */
case class PathAndRepository(
    path: Path,
    repositoryType: RepositoryType
) derives Codec
