import io.circe.{Codec, Decoder, Encoder}
import org.bson.types.ObjectId
import uk.co.unclealex.stringlike.StringLike

import java.nio.file.{Path, Paths}
import java.time.Instant
import java.time.format.DateTimeFormatter
import scala.collection.SortedMap
import scala.util.Try

package object json {

  given StringLike[Path] = StringLike.fromTry(_.toString, s => Try(Paths.get(s)))

  given StringLike[Instant] = {
    val df = DateTimeFormatter.ISO_INSTANT
    def encoder(instant: Instant): String = df.format(instant)
    StringLike.fromTry(df.format, str => Try(df.parse(str, s => Instant.from(s))))
  }
  given [V](using Codec[V]): Codec[SortedMap[String, V]] = {
    val decoder: Decoder[SortedMap[String, V]] = Decoder[Map[String, V]].map { m =>
      val empty: SortedMap[String, V] = SortedMap.empty
      m.foldLeft(empty) { case (acc, (k, v)) =>
        acc + (k -> v)
      }
    }
    val encoder: Encoder[SortedMap[String, V]] = Encoder[Map[String, V]].contramap(_.toMap)
    Codec.from(decoder, encoder)
  }
}

