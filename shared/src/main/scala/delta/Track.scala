package delta

import io.circe._
import io.circe.generic.semiauto._

case class Track(
    trackNumber: Int,
    totalTracks: Int,
    title: String,
    album: String,
    artist: String
)

object Track {

  implicit val trackDecoder: Decoder[Track] = deriveDecoder[Track]
  implicit val trackEncoder: Encoder[Track] = deriveEncoder[Track]
}