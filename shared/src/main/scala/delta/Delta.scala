package delta

import io.circe.derivation.{Configuration, ConfiguredCodec}
import io.circe.{Codec, Decoder, Encoder}

import java.nio.file.{FileSystem, Path}
import java.time.Instant
import json.given 
import uk.co.unclealex.stringlike.StringLikeJsonSupport.given 

sealed trait Delta {
  val path: Path
  val ordering: Int
  val summaryPath: Path
  val last: Boolean
  def withOrdering(ordering: Int): Delta
  def withLast(last: Boolean): Delta
  def decorateUrl(decorator: String => String): Delta
}

case class CreateFileDelta(lastSynchronised: Instant, path: Path, url: String, mimeType: String, ordering: Int, track: Track, summaryPath: Path, last: Boolean) extends Delta {
  override def withOrdering(ordering: Int): Delta = copy(ordering = ordering)
  override def withLast(last: Boolean): Delta = copy(last = last)

  override def decorateUrl(decorator: String => String): Delta = copy(url = decorator(url))
}
case class RemoveFileDelta(path: Path, ordering: Int, track: Option[Track], summaryPath: Path, last: Boolean) extends Delta {
  override def withOrdering(ordering: Int): Delta = copy(ordering = ordering)
  override def withLast(last: Boolean): Delta = copy(last = last)
  override def decorateUrl(decorator: String => String): Delta = this
}

object Delta {

  given Configuration = Configuration.default.withDiscriminator("type").copy(
    transformConstructorNames = {
      case "CreateFileDelta" => "createFile"
      case "RemoveFileDelta" => "removeFile"
    }
  )

  given Codec[Delta] = ConfiguredCodec.derived[Delta]
}