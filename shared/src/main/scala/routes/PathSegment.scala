package routes

import enumeratum.*
import org.apache.pekko.http.scaladsl.server.{PathMatcher, PathMatcher0}

import java.net.URI
import scala.language.implicitConversions

sealed trait PathSegment extends EnumEntry {

  val segmentName: String
}

object PathSegment extends Enum[PathSegment] {
  case object API extends PathSegment {
    override val segmentName: String = "api"
  }

  case object MUSIC extends PathSegment {
    val segmentName: String = "music"
  }

  case object COVER_ART extends PathSegment {
    val segmentName: String = "coverArt"
  }

  case object DAV extends PathSegment {
    val segmentName: String = "dav"
  }

  case object DATUM extends PathSegment {
    val segmentName: String = "datum"
  }

  case object DEVICES extends PathSegment {
    val segmentName: String = "devices"
  }

  case object INFO extends PathSegment {
    val segmentName: String = "info"
  }

  case object LAST_SYNCHRONISED extends PathSegment {
    val segmentName: String = "lastSynchronised"
  }

  case object OFFSET extends PathSegment {
    val segmentName: String = "offset"
  }

  case object INITIALISE extends PathSegment {
    val segmentName: String = "initialise.sh"
  }

  case object COMMANDS extends PathSegment {
    val segmentName: String = "commands"
  }

  case object DELTAS extends PathSegment {
    val segmentName: String = "deltas"
  }

  case object FULL_SEGMENT extends PathSegment {
    val segmentName: String = "full"
  }

  case object SUMMARIES_SEGMENT extends PathSegment {
    val segmentName: String = "summaries"
  }

  override def values: IndexedSeq[PathSegment] = findValues

  trait ServerExtensions {
    implicit def routeSegmentIsPathMatcher0(rs: PathSegment): PathMatcher0 = {
      rs.segmentName
    }

  }

  trait ClientExtensions {

    given Conversion[PathSegment, String] = _.segmentName

    class UriPath(val path: String) {
      override def toString: String = path

      def /(uriPath: UriPath): UriPath = new UriPath(
        s"$path/${uriPath.toString}"
      )
    }

    given Conversion[String, UriPath] = new UriPath(_)
    given Conversion[PathSegment, UriPath] = ps => new UriPath(ps.segmentName)

    extension (uri: URI) {
      def withPath(uriPath: UriPath): String = {
        uri.resolve(uriPath.toString).toASCIIString
      }
    }
  }
}
