Changes
=

Changes and change trees are used to keep track of changes to the repository. Changes can
be replayed so that it is possible to find out the state of the repository at any given time.
