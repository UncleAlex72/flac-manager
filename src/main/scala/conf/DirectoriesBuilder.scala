package conf

import cats.data.ValidatedNel
import com.typesafe.scalalogging.StrictLogging
import files.Directories
import validation.{Validations, catsSyntaxValidatedId}

import java.nio.file.attribute.{
  FileAttribute,
  PosixFilePermission,
  PosixFilePermissions
}
import java.nio.file.{Files, Path, Paths}
import scala.util.Try

object DirectoriesBuilder extends StrictLogging:

  def validateProperty(
      predicate: Path => Boolean,
      failureMessage: Path => String
  ): Path => ValidatedNel[String, Unit] = { path =>
    if Try(predicate(path)).toOption.getOrElse(false) then {}.validNel
    else failureMessage(path).invalidNel
  }

  def apply(configuration: Configuration): ValidatedNel[String, Directories] =

    /** The parent directory for all repositories.
      */
    val musicDirectory: Path =
      Paths.get(configuration.musicDirectory)

    val _backupPath: Path =
      Paths.get(configuration.backupDirectory)

    val _temporaryPath: Path =
      val fileAttributes: FileAttribute[java.util.Set[PosixFilePermission]] =
        PosixFilePermissions.asFileAttribute(
          PosixFilePermissions.fromString("rwxr--r--")
        )
      val rootTmpDir: Path = musicDirectory.resolve("tmp")
      val tmp: Path =
        if Files.isDirectory(rootTmpDir) then
          Files
            .createTempDirectory(rootTmpDir, "flac-manager-", fileAttributes)
            .toAbsolutePath
        else
          Files
            .createTempDirectory("flac-manager-", fileAttributes)
            .toAbsolutePath
      logger.info(s"Created temporary directory $tmp")
      tmp

    val _datumPath: Path = musicDirectory.resolve(
      ".flac-manager-datum-file-" + java.util.UUID.randomUUID.toString
    )

    // Create the datum file
    Files.createFile(_datumPath)

    val exists: Path => ValidatedNel[String, Unit] =
      validateProperty(Files.exists(_), path => s"$path does not exist")
    val isDirectory: Path => ValidatedNel[String, Unit] = validateProperty(
      Files.isDirectory(_),
      path => s"$path is not a directory"
    )
    val isWriteable: Path => ValidatedNel[String, Unit] =
      validateProperty(Files.isWritable, path => s"$path is not writeable")

    val directories: Directories = new Directories:
      override def temporaryPath: Path = _temporaryPath
      override def maybeBackupPath: Option[Path] = Some(_backupPath)
      override def datumPath: Path = _datumPath

    val existingDirectoryValidations = for
      path <- Seq(
        directories.flacPath,
        directories.stagingPath,
        directories.encodedPath
      )
      validationFunction <- Seq(exists, isDirectory)
    yield validationFunction(path)
    Validations
      .sequence(
        existingDirectoryValidations :+ isWriteable(directories.stagingPath)
      )
      .map(_ => directories)
