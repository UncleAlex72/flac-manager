package conf

import io.circe.Decoder
import io.circe.generic.semiauto._

case class Configuration(
    musicDirectory: String,
    backupDirectory: String,
    allowMultiDiscs: Boolean,
    encoderCommand: String,
    mongodbUri: String,
    production: String
):
  val isProduction: Boolean = production.nonEmpty

object Configuration:

  implicit val configurationDecoder: Decoder[Configuration] =
    deriveDecoder[Configuration]
