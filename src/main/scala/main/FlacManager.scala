package main

import cats.data.Validated.{Invalid, Valid}
import cats.data.ValidatedNel
import cats.syntax.either.*
import com.typesafe.scalalogging.StrictLogging
import conf.{Configuration, DirectoriesBuilder}
import controllers.*
import delta.{DeltaCacheServiceImpl, DeltaServiceImpl}
import devices.*
import devices.Extension.{M4A, MP3}
import files.*
import health.HealthCheckServiceImpl
import io.circe.config.parser
import music.{FixDateService, FixDateServiceImpl, JaudioTaggerTagsService}
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.event.Logging
import org.apache.pekko.http.scaladsl.Http
import org.apache.pekko.http.scaladsl.server.Directives.*
import org.apache.pekko.http.scaladsl.server.Route
import org.apache.pekko.http.scaladsl.server.directives.DebuggingDirectives
import org.apache.pekko.http.scaladsl.settings.ParserSettings
import processor.*
import text.{
  Normaliser,
  NumberNormaliser,
  NumberNormaliserImpl,
  RegularExpressionNormaliser
}
import uk.co.unclealex.mongodb.dao.MongoDatabaseFactory
import users.{OwnerService, OwnerServiceImpl}
import versioning.ProcessGitRepository
import webdav.WebDavResourceBuilderImpl

import java.nio.file.{Files, Path, FileSystem as JFileSystem}
import java.time.Clock
import java.util.concurrent.TimeUnit
import scala.collection.concurrent.TrieMap
import scala.concurrent.duration.{Duration, FiniteDuration}
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.io.StdIn
import scala.util.Try

/** Create all the components needed to run this application.
  *
  * @param context
  *   The Play Context.
  */
@main def FlacManager(): Unit =
  val validatedConfigurationAndDirectories
      : ValidatedNel[String, (Configuration, Directories)] = parser
    .decode[Configuration]()
    .left
    .map(e => e.getMessage)
    .toValidatedNel
    .andThen { configuration =>
      DirectoriesBuilder(configuration).map { directories =>
        (configuration, directories)
      }
    }

  validatedConfigurationAndDirectories match
    case Valid((configuration, directories)) => go(directories, configuration)
    case Invalid(messages) =>
      throw new IllegalStateException(
        "Could not start FlacManager:\n" + messages.toList.mkString("\n")
      )

  def go(directories: Directories, configuration: Configuration): Unit =
    implicit val actorSystem: ActorSystem = ActorSystem()
    implicit val ec: ExecutionContext = actorSystem.dispatcher

    implicit val numberNormaliser: NumberNormaliser = new NumberNormaliserImpl
    implicit val normaliser: Normaliser = new RegularExpressionNormaliser

    val (eventuallyCloseable, eventualDatabase) = MongoDatabaseFactory(
      configuration.mongodbUri
    )

    val tagsService = new JaudioTaggerTagsService()

    implicit val clock: Clock = Clock.systemDefaultZone()

    implicit val jfs: JFileSystem = java.nio.file.FileSystems.getDefault

    val deviceDao =
      new MongoDbDeviceDao(eventualDatabase = eventualDatabase, clock = clock)

    val flacFileChecker = new FlacFileCheckerImpl()

    val fileSystem: FileSystem =
      val protectionAwareFileSystem = new ProtectionAwareFileSystem(
        FileSystem()
      )
      new BackupAwareFileSystem(protectionAwareFileSystem)

    val repositories = new RepositoriesImpl(
      directories = directories,
      tagsService = tagsService,
      flacFileChecker = flacFileChecker
    )

    val repository: versioning.Repository = new ProcessGitRepository(
      repositories = repositories,
      root = directories.hashPath
    )

    val encoders: Map[LossyExtension, LossyEncoder] =
      val ffmpegCommand = configuration.encoderCommand
      val ffmpegEncoder = new FfmpegEncoder(ffmpegCommand.split("_").toList)
      Map[LossyExtension, LossyEncoder](
        MP3 -> new Mp3LossyEncoder(ffmpegEncoder),
        M4A -> new M4aLossyEncoder(ffmpegEncoder)
      )

    val deviceIdentifierService = new DeviceIdentifierServiceImpl(deviceDao)

    val ownerService: OwnerService =
      new OwnerServiceImpl(deviceDao)

    val fixDateService: FixDateService = new FixDateServiceImpl

    val deltaCacheService = new DeltaCacheServiceImpl(
      TrieMap.empty,
      repositories = repositories,
      repository = repository
    )

    val deviceService: DeviceService = new DeviceServiceImpl(
      deviceDao = deviceDao,
      deviceIdentifierService = deviceIdentifierService,
      deltaCacheService = deltaCacheService,
      repository = repository,
      repositories = repositories,
      clock = clock
    )

    val commandParser: CommandParser =
      val parametersValidator = new ParametersValidatorImpl(
        ownerService = ownerService,
        repositories = repositories
      )
      val allowMulti =
        configuration.allowMultiDiscs
      val checkinCommandParser = new CheckinCommandParser(
        allowMulti = allowMulti,
        ownerService = ownerService,
        fixDateService = fixDateService,
        parametersValidator = parametersValidator
      )
      val ownCommandsParser = new OwnCommandsParser(
        ownerService = ownerService,
        parametersValidator = parametersValidator
      )
      val checkoutCommandParser = new CheckoutCommandParser(
        parametersValidator = parametersValidator,
        ownerService = ownerService
      )

      val multiDiscCommandParser = new MultiDiscCommandParser(
        parametersValidator
      )
      val joinCommandParser = new JoinCommandParser(
        parametersValidator = parametersValidator
      )
      val healthCheckCommandParser = new HealthCheckCommandParser()

      new CommandParserImpl(
        checkinCommandParser,
        checkoutCommandParser,
        ownCommandsParser,
        multiDiscCommandParser,
        joinCommandParser,
        healthCheckCommandParser
      )

    val actionExecutor: ActionExecutor =
      val checkinFileActionExecutor = new CheckinFileActionExecutor(
        clock = clock,
        encoders = encoders,
        fs = fileSystem,
        repositories = repositories
      )
      val hashEncoder = new GuavaHashEncoder(fileSystem)
      val createHashFileActionExecutor = new CreateHashFileActionExecutor(
        hashEncoder = hashEncoder
      )
      val removeHashFileActionExecutor = new RemoveHashFileActionExecutor(
        hashEncoder = hashEncoder,
        repository = repository
      )
      val commitActionExecutor =
        new CommitActionExecutor(repository = repository, clock = clock)
      val clearCachesActionExecutor = new ClearCachesActionExecutor(
        deltaCacheService = deltaCacheService
      )
      val deleteFileActionExecutor = new DeleteFileActionExecutor(
        fs = fileSystem
      )
      val checkoutFileActionExecutor = new CheckoutFileActionExecutor(
        clock = clock,
        encoders = encoders,
        fs = fileSystem
      )

      val alterTagsActionExecutor = new AlterTagsActionExecutor
      val progressActionExecutor = new ProgressActionExecutor
      val healthCheckActionExecutor =
        val healthCheckService = new HealthCheckServiceImpl(
          repositories = repositories,
          ownerService = ownerService
        )
        new HealthCheckActionExecutor(healthCheckService = healthCheckService)
      new ActionExecutorImpl(
        checkinFileActionExecutor,
        createHashFileActionExecutor,
        removeHashFileActionExecutor,
        commitActionExecutor,
        clearCachesActionExecutor,
        deleteFileActionExecutor,
        checkoutFileActionExecutor,
        alterTagsActionExecutor,
        healthCheckActionExecutor,
        progressActionExecutor
      )

    val jobIdGenerator: JobIdGenerator = new SequentialJobIdGenerator()

    val (commandProcessor, shutdown): (CommandProcessor, Shutdown) =
      val commandProcessorAndShutdown = new CommandProcessorImpl(
        commandParser = commandParser,
        actionExecutor = actionExecutor,
        jobIdGenerator = jobIdGenerator
      ).logAll()
      (commandProcessorAndShutdown, commandProcessorAndShutdown)

    val datumController: DatumController = new DatumController(
      flacDirectories = directories
    )

    val commandsController = new CommandsController(
      commandProcessor = commandProcessor,
      repository = repository
    )

    val deltaService = new DeltaServiceImpl(
      deltaCacheService = deltaCacheService
    )

    val deltasController = new DeltasController(
      deviceDao = deviceDao,
      deltaService = deltaService
    )

    val resourceBuilder = new WebDavResourceBuilderImpl(
      deviceDao = deviceDao,
      repositories = repositories
    )
    val webDavController = new WebDavController(
      resourceBuilder = resourceBuilder
    )

    val devicesController = new DevicesController(
      deviceService = deviceService,
      deviceDao = deviceDao
    )

    val musicController = new MusicController(
      repositories = repositories,
      tagsService = tagsService
    )

    def route: Route =
      DebuggingDirectives.logRequestResult(("http", Logging.InfoLevel)):
        concat(
          datumController.route,
          commandsController.route,
          musicController.route,
          deltasController.route,
          devicesController.route,
          webDavController.route
        )
    val parserSettings =
      ParserSettings.forServer.withCustomMethods(CustomMethods.PROPFIND)
    val bindingFuture = Http()
      .newServerAt("0.0.0.0", 9000)
      .adaptSettings(_.withParserSettings(parserSettings))
      .bind(route)

    def runShutdown(): Unit =
      Await.ready(
        {
          for
            binding <- bindingFuture
            _ <- binding.unbind()
            _ <- Future { shutdown.shutdown() }
            _ <- actorSystem.terminate()
            autocloseable <- eventuallyCloseable
          yield {
            autocloseable.close()
            val datumPath: Path = directories.datumPath
            Try(Files.deleteIfExists(datumPath))
          }
        },
        FiniteDuration(1, TimeUnit.MINUTES)
      )
    sys.addShutdownHook:
      runShutdown()

    if configuration.isProduction then Await.result(Future.never, Duration.Inf)
    else
      println(
        s"Server now online. Please navigate to http://localhost:9000/\nPress RETURN to stop..."
      )
      StdIn.readLine() // let it run until user presses return
      runShutdown()
