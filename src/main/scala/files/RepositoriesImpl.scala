/*
 * Copyright 2018 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package files

import java.io.IOException
import java.nio.file._
import java.nio.file.attribute.BasicFileAttributes
import java.time.Instant
import cats.data.{Validated, ValidatedNel}
import devices.Extension._
import devices.LossyExtension
import logging.Messages
import music.{Tags, TagsService}
import text.{Normaliser, NumberNormaliser}
import validation._

import scala.jdk.CollectionConverters._
import scala.collection.immutable.{SortedMap, SortedSet}
import scala.collection.mutable

/** The default implementation of [[Repositories]]
  */
class RepositoriesImpl(
    val directories: Directories,
    val tagsService: TagsService,
    val flacFileChecker: FlacFileChecker
)(implicit val normaliser: Normaliser, numberNormaliser: NumberNormaliser)
    extends Repositories:

  override def flac: FlacRepository =
    new RepositoryImpl[FlacFile](
      "flac",
      directories.flacPath,
      new FlacFileImpl(_, new PathTagsContainer(_))
    ) with FlacRepository

  override def staging: StagingRepository =
    new RepositoryImpl[StagingFile](
      "staging",
      directories.stagingPath,
      new StagingFileImpl(_, new PathTagsContainer(_))
    ) with StagingRepository

  override def encoded(extension: LossyExtension): EncodedRepository =
    new RepositoryImpl[EncodedFile](
      "encoded",
      directories.encodedPath.resolve(extension.extension),
      new EncodedFileImpl(extension, _, new PathTagsContainer(_))
    ) with EncodedRepository

  override def hash(owner: String): HashRepository =
    new RepositoryImpl[HashFile](
      "hash",
      directories.hashPath.resolve(owner),
      new HashFileImpl(owner, _, _ => NoTagsContainer)
    ) with HashRepository

  /** A tags container where the tags are stored in a file on the file system.
    *
    * @param absolutePath
    *   The absolute path of the file where the tags are stored.
    */
  class PathTagsContainer(absolutePath: Path) extends TagsContainer:
    lazy val tags: ValidatedNel[String, Tags] = tagsService.read(absolutePath)

    override def read(): ValidatedNel[String, Tags] = tags

  /** A tags container that stores a static set of tags. This is used for
    * transferring tags between different types of file.
    *
    * @param staticTags
    *   The tags stored by this container.
    */
  class StaticTagsContainer(staticTags: Tags) extends TagsContainer:
    val tags: ValidatedNel[String, Tags] = Validated.valid(staticTags)

    override def read(): ValidatedNel[String, Tags] = tags

  case object NoTagsContainer extends TagsContainer:
    val tags: ValidatedNel[String, Tags] =
      Validated.invalidNel(
        Messages.error("Tags cannot be read from a hash file")
      )

    override def read(): ValidatedNel[String, Tags] = tags

  /** The base class for implementations of [[File]]
    *
    * @param readOnly
    *   True if this file should be read only, false otherwise.
    * @param rootPath
    *   The top level directory of the repository this file is in.
    * @param basePath
    *   The base directory of the repository this file is in.
    * @param relativePath
    *   The path this file points to.
    * @param tagsContainerProvider
    *   A function to generate a tags container from a path.
    */
  abstract class FileImpl(
      val readOnly: Boolean,
      val rootPath: Path,
      val basePath: Path,
      val relativePath: Path,
      tagsContainerProvider: Path => TagsContainer
  ):

    lazy val absolutePath: Path = basePath.resolve(relativePath)
    lazy val exists: Boolean =
      Files.isSymbolicLink(absolutePath) || Files.exists(absolutePath)
    lazy val lastModified: Instant = Files
      .getLastModifiedTime(absolutePath, LinkOption.NOFOLLOW_LINKS)
      .toInstant
    lazy val size: Long = Files.size(absolutePath)
    lazy val treePath: Seq[String] =
      Range(0, relativePath.getNameCount).map(idx =>
        relativePath.getName(idx).toString
      )
    val tags: TagsContainer = tagsContainerProvider(absolutePath)

    /** Compare files by their relative path.
      *
      * @param other
      *   The file to compare.
      * @return
      *   True if the two files have the same path, false otherwise.
      */
    override def equals(other: Any): Boolean = other match
      case f: File => absolutePath.equals(f.absolutePath)
      case _       => false

    def toBackupFile: Option[BackupFile] = None

    override def hashCode(): Int = absolutePath.hashCode()

    override def toString: String = absolutePath.toString

  /** The base class for implementations of [[Directory]]
    *
    * @param basePath
    *   The base path of the repository this directory is in.
    * @param relativePath
    *   The relative path of the directory in the repository.
    * @param fileBuilder
    *   A function used to create a [[File]] from a path in this directory's
    *   repository.
    * @tparam F
    *   The type of files in the repository.
    */
  class DirectoryImpl[F <: File](
      val basePath: Path,
      val relativePath: Path,
      fileBuilder: Path => F
  ) extends Directory[F]:
    override val absolutePath: Path = basePath.resolve(relativePath)

    override def toString: String = absolutePath.toString

    override def list: SortedSet[F] = list(Int.MaxValue)

    override def list(maxDepth: Int): SortedSet[F] =
      val empty: SortedSet[F] = SortedSet.empty
      walk(maxDepth).foldLeft(empty) { (files, directoryOrFile) =>
        val maybeFile: Option[F] = directoryOrFile.fold(_ => None, Some(_))
        files ++ maybeFile
      }

    /** Walk through a directory.
      *
      * @param maxDepth
      *   The maximum depth to look for files or directories.
      * @return
      *   A sequence of the directories or files found.
      */
    def walk(maxDepth: Int): Seq[Either[Directory[F], F]] =
      val paths: mutable.Buffer[Either[Directory[F], F]] = mutable.Buffer.empty
      def relativise(path: Path): Path = basePath.relativize(path)
      val visitor: FileVisitor[Path] = new FileVisitor[Path]:
        override def postVisitDirectory(
            dir: Path,
            exc: IOException
        ): FileVisitResult = FileVisitResult.CONTINUE

        override def visitFile(
            file: Path,
            attrs: BasicFileAttributes
        ): FileVisitResult =
          if !Files.isHidden(file) then
            paths += Right(fileBuilder(relativise(file)))
          FileVisitResult.CONTINUE

        override def visitFileFailed(
            file: Path,
            exc: IOException
        ): FileVisitResult = FileVisitResult.TERMINATE

        override def preVisitDirectory(
            dir: Path,
            attrs: BasicFileAttributes
        ): FileVisitResult =
          if Files.isHidden(dir) then FileVisitResult.SKIP_SUBTREE
          else
            paths += Left(
              new DirectoryImpl[F](basePath, relativise(dir), fileBuilder)
            )
            FileVisitResult.CONTINUE

      Files.walkFileTree(
        absolutePath,
        mutable.Set.empty[FileVisitOption].asJava,
        maxDepth,
        visitor
      )
      paths.toSeq

    override def group: SortedMap[Directory[F], SortedSet[F]] =
      case class State(
          directories: Map[Path, Directory[F]] = Map.empty,
          groupedFiles: SortedMap[Directory[F], SortedSet[F]] = SortedMap.empty
      ):
        def directory(directory: Directory[F]): State = copy(directories =
          directories + (directory.relativePath -> directory)
        )

        def +(file: F): State =
          directories.get(file.relativePath.getParent) match
            case Some(directory) =>
              val files: SortedSet[F] =
                groupedFiles.getOrElse(directory, SortedSet.empty[F]) + file
              copy(groupedFiles = groupedFiles + (directory -> files))
            case None => this
      val state: State = walk(Int.MaxValue).foldLeft(State()):
        (state, directoryOrFile) =>
          directoryOrFile match
            case Left(directory) => state.directory(directory)
            case Right(file)     => state + file
      state.groupedFiles

  /** The default implementation of [[FlacFile]]
    *
    * @param relativePath
    *   The path this file points to.
    * @param tagsContainerProvider
    *   A function to generate a tags container from a path.
    */
  class FlacFileImpl(
      override val relativePath: Path,
      tagsContainerProvider: Path => TagsContainer
  ) extends FileImpl(
        true,
        directories.flacPath,
        directories.flacPath,
        relativePath,
        tagsContainerProvider
      )
      with FlacFile:

    override def toHashFile(owner: String): HashFile =
      new HashFileImpl(owner, relativePath.withExtension("hash"), _ => tags)

    override def toStagingFile: StagingFile =
      new StagingFileImpl(relativePath, _ => tags)

    override def toEncodedFile(extension: LossyExtension): EncodedFile =
      new EncodedFileImpl(
        extension,
        relativePath.withExtension(extension),
        _ => tags
      )

    override def toBackupFile: Option[BackupFile] =
      directories.maybeBackupPath.map { backupPath =>
        new FileImpl(
          true,
          backupPath,
          backupPath,
          relativePath,
          tagsContainerProvider
        ) with BackupFile
      }

  /** The default implementation of [[HashFile]]
    *
    * @param relativePath
    *   The path this file points to.
    * @param tagsContainerProvider
    *   A function to generate a tags container from a path.
    */
  class HashFileImpl(
      val owner: String,
      override val relativePath: Path,
      tagsContainerProvider: Path => TagsContainer
  ) extends FileImpl(
        false,
        directories.hashPath,
        directories.hashPath.resolve(owner),
        relativePath,
        tagsContainerProvider
      )
      with HashFile:

    override def toEncodedFile(extension: LossyExtension): EncodedFile =
      new EncodedFileImpl(
        extension,
        relativePath.withExtension(extension),
        path => new PathTagsContainer(path)
      )

  /** The default implementation of [[StagingFile]]
    *
    * @param relativePath
    *   The path this file points to.
    * @param tagsContainerProvider
    *   A function to generate a tags container from a path.
    */
  class StagingFileImpl(
      override val relativePath: Path,
      tagsContainerProvider: Path => TagsContainer
  )(implicit normaliser: Normaliser, numberNormaliser: NumberNormaliser)
      extends FileImpl(
        false,
        directories.stagingPath,
        directories.stagingPath,
        relativePath,
        tagsContainerProvider
      )
      with StagingFile:
    override def isFlacFile: Boolean = flacFileChecker.isFlacFile(absolutePath)

    override def toFlacFileAndTags: ValidatedNel[String, (FlacFile, Tags)] =
      tags.read().map { tags =>
        val flacFile = new FlacFileImpl(
          tags.asPath(relativePath.getFileSystem, FLAC),
          _ => new StaticTagsContainer(tags)
        )
        (flacFile, tags)
      }

    override def writeTags(tags: Tags): StagingFile =
      tagsService.write(absolutePath, tags)
      new StagingFileImpl(relativePath, _ => new StaticTagsContainer(tags))

  /** The default implementation of [[EncodedFile]]
    *
    * @param relativePath
    *   The path this file points to.
    * @param tagsContainerProvider
    *   A function to generate a tags container from a path.
    */
  class EncodedFileImpl(
      override val extension: LossyExtension,
      override val relativePath: Path,
      tagsContainerProvider: Path => TagsContainer
  ) extends FileImpl(
        true,
        directories.encodedPath,
        directories.encodedPath.resolve(extension.extension),
        relativePath,
        tagsContainerProvider
      )
      with EncodedFile:

    override def toHashFile(owner: String): HashFile =
      new HashFileImpl(
        owner,
        relativePath.withExtension("hash"),
        tagsContainerProvider
      )

    override def toTempFile: TempFile =
      val baseDirectory: Path = directories.temporaryPath
      val tempPath: Path = Files.createTempFile(
        baseDirectory,
        "flacmanager-encoding-",
        s".${extension.extension}"
      )
      new TempFileImpl(
        baseDirectory,
        baseDirectory.resolve(tempPath),
        _ => tags
      )

    override def toFlacFile: FlacFile =
      new FlacFileImpl(relativePath.withExtension(FLAC), tagsContainerProvider)

    override def encodedRepoPath: Path =
      directories.encodedPath.getFileName.resolve(extension.extension)

  /** The default implementation of [[TempFile]]
    *
    * @param basePath
    *   The base directory of the repository this file is in.
    * @param relativePath
    *   The path this file points to.
    * @param tagsContainerProvider
    *   A function to generate a tags container from a path.
    */
  class TempFileImpl(
      override val basePath: Path,
      override val relativePath: Path,
      tagsContainerProvider: Path => TagsContainer
  ) extends FileImpl(
        false,
        basePath,
        basePath,
        relativePath,
        tagsContainerProvider
      )
      with TempFile:

    override def writeTags(): TempFile =
      tags.read().foreach { tags =>
        tagsService.write(absolutePath, tags)
      }
      new TempFileImpl(basePath: Path, relativePath, _ => tags)

  /** The default implementation of [[Repository]]
    *
    * @param repositoryType
    *   The type of the repository used only to identify it in logs and
    *   messages.
    * @param basePath
    *   The base path of the repository.
    * @param fileBuilder
    *   A function used to build files in this repositories from paths.
    * @tparam F
    *   The type of files in this repository.
    */
  class RepositoryImpl[F <: File](
      val repositoryType: String,
      basePath: Path,
      fileBuilder: Path => F
  ) extends Repository[F]:
    override def root: ValidatedNel[String, Directory[F]] =
      directory(basePath.relativize(basePath))

    override def directory(path: Path): ValidatedNel[String, Directory[F]] =
      val newDirectory = new DirectoryImpl[F](basePath, path, fileBuilder)
      if Files.isDirectory(newDirectory.absolutePath) || !Files.exists(
          newDirectory.absolutePath
        )
      then newDirectory.validNel
      else Messages.notADirectory(path, repositoryType).invalidNel

    override def file(path: Path): ValidatedNel[String, F] =
      val newFile = fileBuilder(path)
      if Files.isDirectory(newFile.absolutePath) then
        Messages.notAFile(path, repositoryType).invalidNel
      else newFile.validNel
