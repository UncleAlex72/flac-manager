package files

/** A file system that will handle the adding and removing of backup files.
  * @param delegate
  *   The underlying file system.
  */
class BackupAwareFileSystem(val delegate: FileSystem) extends FileSystem:

  /** Move a path from a source directory to a target directory using an atomic
    * file system move, creating any required directories. Any directories left
    * empty in the source base path due to the move operation will be removed.
    *
    * @param sourceFile
    *   The source file location
    * @param targetFile
    *   The target file location
    */
  override def move(sourceFile: File, targetFile: File): Unit =
    delegate.move(sourceFile, targetFile)
    sourceFile.toBackupFile.foreach { backupFile =>
      delegate.remove(backupFile)
    }
    targetFile.toBackupFile.foreach { backupFile =>
      delegate.copy(targetFile, backupFile)
    }

  /** Make a file readable by anybody.
    *
    * @param file
    *   The file to make readable.
    */
  override def makeWorldReadable(file: File): Unit =
    delegate.makeWorldReadable(file)

  /** Copy a path from a source directory to a target directory using an atomic
    * file system copy, creating any required directories.
    *
    * @param sourceFile
    *   The source file location
    * @param targetFile
    *   The target file location
    */
  override def copy(sourceFile: File, targetFile: File): Unit =
    delegate.copy(sourceFile, targetFile)
    targetFile.toBackupFile.foreach { backupFile =>
      delegate.copy(targetFile, backupFile)
    }

  /** Remove directories if they are empty and recurse up the directory tree.
    *
    * @param file
    *   The file location to remove.
    */
  override def remove(file: File): Unit =
    delegate.remove(file)
    file.toBackupFile.foreach { backupFile =>
      delegate.remove(backupFile)
    }

  /** Write a string to a file
    *
    * @param targetFile
    *   The file to write to
    * @param contents
    *   The contents to write
    */
  override def writeString(targetFile: File, contents: String): Unit =
    delegate.writeString(targetFile, contents)
