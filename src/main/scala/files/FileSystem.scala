/*
 * Copyright 2018 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package files

import java.io.IOException
import java.nio.charset.StandardCharsets
import java.nio.file.attribute.PosixFilePermission.{
  GROUP_READ,
  OTHERS_READ,
  OWNER_READ
}
import java.nio.file.attribute.{
  BasicFileAttributes,
  FileAttribute,
  PosixFilePermission,
  PosixFilePermissions
}
import java.nio.file._
import java.util.stream
import scala.util.Try

/** Utilities for manipulating files and directories in ways not directly
  * supported by the JDK.
  *
  * @author
  *   alex
  */
trait FileSystem:

  /** Write a string to a file
    * @param targetFile
    *   The file to write to
    * @param contents
    *   The contents to write
    */
  def writeString(targetFile: File, contents: String): Unit

  /** Move a path from a source directory to a target directory using an atomic
    * file system move, creating any required directories. Any directories left
    * empty in the source base path due to the move operation will be removed.
    *
    * @param sourceFile
    *   The source file location
    * @param targetFile
    *   The target file location
    */
  def move(sourceFile: File, targetFile: File): Unit

  /** Make a file readable by anybody.
    * @param file
    *   The file to make readable.
    */
  def makeWorldReadable(file: File): Unit

  /** Copy a path from a source directory to a target directory using an atomic
    * file system copy, creating any required directories.
    *
    * @param sourceFile
    *   The source file location
    * @param targetFile
    *   The target file location
    */
  def copy(sourceFile: File, targetFile: File): Unit

  /** Remove directories if they are empty and recurse up the directory tree.
    *
    * @param file
    *   The file location to remove.
    */
  def remove(file: File): Unit

object FileSystem:

  import java.util.{stream, Set => JSet}

  import scala.jdk.CollectionConverters._
  import scala.jdk.StreamConverters._

  def apply(): FileSystem = new FileSystem:
    case class Permissions(posixFilePermission: Set[PosixFilePermission]):
      val posixPermissions: JSet[PosixFilePermission] =
        posixFilePermission.asJava
      val fileAttribute: FileAttribute[?] =
        PosixFilePermissions.asFileAttribute(posixPermissions)
    object Permissions:
      def apply(posixFilePermissions: PosixFilePermission*): Permissions =
        Permissions(posixFilePermissions.toSet)

    val filePermissions: Permissions = Permissions(
      PosixFilePermission.OWNER_WRITE,
      PosixFilePermission.OWNER_READ,
      PosixFilePermission.GROUP_READ,
      PosixFilePermission.OTHERS_READ
    )

    val directoryPermissions: Permissions = Permissions(
      PosixFilePermission.OWNER_WRITE,
      PosixFilePermission.OWNER_EXECUTE,
      PosixFilePermission.OWNER_READ,
      PosixFilePermission.GROUP_EXECUTE,
      PosixFilePermission.GROUP_READ,
      PosixFilePermission.OTHERS_EXECUTE,
      PosixFilePermission.OTHERS_READ
    )

    override def writeString(targetFile: File, contents: String): Unit =
      val targetPath = targetFile.absolutePath
      createDirectories(targetPath.getParent)
      Files.writeString(targetPath, contents, StandardCharsets.UTF_8)

    /** @inheritdoc
      */
    override def move(sourceFile: File, targetFile: File): Unit =
      val sourcePath: Path = sourceFile.absolutePath
      val targetPath: Path = targetFile.absolutePath
      createDirectories(targetPath.getParent)
      tryAtomicMove(sourcePath, targetPath)
      val currentDirectory: Path = sourcePath.getParent
      removeRecursively(sourceFile.basePath, currentDirectory)

    /** @inheritdoc
      */
    override def copy(sourceFile: File, targetFile: File): Unit =
      val sourcePath: Path = sourceFile.absolutePath
      val targetPath: Path = targetFile.absolutePath
      val parentTargetPath: Path = targetPath.getParent
      createDirectories(parentTargetPath)
      val tempPath: Path =
        Files.createTempFile(parentTargetPath, "device-file-", ".tmp")
      Files.copy(sourcePath, tempPath, StandardCopyOption.REPLACE_EXISTING)
      tryAtomicMove(tempPath, targetPath, StandardCopyOption.REPLACE_EXISTING)
      val currentDirectory: Path = sourcePath.getParent
      removeRecursively(sourceFile.basePath, currentDirectory)

    private def createDirectories(path: Path): Path =
      val missingDirectories: LazyList[Path] = LazyList
        .iterate(path)(_.getParent)
        .takeWhile(path => !Files.exists(path))
        .reverse
      missingDirectories.foreach { dir =>
        Files.createDirectories(dir)
        Files.setPosixFilePermissions(
          dir,
          directoryPermissions.posixPermissions
        )
      }
      path

    /** @inheritdoc
      */
    override def makeWorldReadable(file: File): Unit =
      val path: Path = file.absolutePath
      val currentPermissions: Set[PosixFilePermission] =
        Files
          .getPosixFilePermissions(path, LinkOption.NOFOLLOW_LINKS)
          .asScala
          .toSet
      val newPermissions: Set[PosixFilePermission] =
        currentPermissions ++ Seq(OWNER_READ, GROUP_READ, OTHERS_READ)
      Files.setPosixFilePermissions(path, newPermissions.asJava)

    /** Try and move a file using an atomic move operation. If this fails, move
      * it non-atomically.
      *
      * @param sourcePath
      *   The source path.
      * @param targetPath
      *   The target path.
      * @param options
      *   The [[StandardCopyOption]] passed to the file system.
      */
    def tryAtomicMove(
        sourcePath: Path,
        targetPath: Path,
        options: StandardCopyOption*
    ): Unit =
      try
        val optionsWithAtomicMove: Seq[StandardCopyOption] =
          options :+ StandardCopyOption.ATOMIC_MOVE
        Files.move(sourcePath, targetPath, optionsWithAtomicMove*)
      catch
        case _: AtomicMoveNotSupportedException =>
          Files.move(sourcePath, targetPath, options*)
      Files.setPosixFilePermissions(
        targetPath,
        filePermissions.posixPermissions
      )

    /** @inheritdoc
      */
    override def remove(file: File): Unit =
      removeRecursively(file.basePath, file.absolutePath)

    /** Remove a path and, if it's directory is now empty, remove that and
      * recurse up.
      * @param basePath
      *   The base path at which to no longer try to remove directories.
      * @param path
      *   The path to remove.
      * @return
      *   A [[Try]] containing an exception if one occurred.
      */
    def removeRecursively(basePath: Path, path: Path): Try[Unit] = Try:
      if basePath.equals(path) then {
        // Do nothing
      } else if Files.isDirectory(path) then
        val dir: stream.Stream[Path] = Files.list(path)
        val directoryIsEmpty: Boolean = dir.toScala(Seq).forall(Files.isHidden)
        if directoryIsEmpty then
          recursivelyDelete(path)
          removeRecursively(basePath, path.getParent)
        dir.close()
      else
        Files.deleteIfExists(path)
        removeRecursively(basePath, path.getParent)

    def recursivelyDelete(path: Path): Unit =
      def deleteAndContinue(path: Path): FileVisitResult =
        Files.delete(path)
        FileVisitResult.CONTINUE
      Files.walkFileTree(
        path,
        new SimpleFileVisitor[Path] {
          override def postVisitDirectory(
              dir: Path,
              exc: IOException
          ): FileVisitResult =
            deleteAndContinue(dir)
          override def visitFile(
              file: Path,
              attrs: BasicFileAttributes
          ): FileVisitResult =
            deleteAndContinue(file)
        }
      )
