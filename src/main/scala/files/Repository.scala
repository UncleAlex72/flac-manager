/*
 * Copyright 2018 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package files

import cats.data.ValidatedNel
import devices.LossyExtension
import music.Tags
import validation.Validations

import java.nio.file.Path
import java.time.Instant
import scala.collection.{SortedMap, SortedSet}
import scala.jdk.CollectionConverters._

/** A repository is a collection of files on a filesystem. This is the main
  * concept within flac manager as there are four repositories: one for FLAC
  * files, one for staging files, one for encoded files and one for devices that
  * contain symbolic links to encoded files. Defining both different file and
  * directory types for each repository allows for command arguments to be type
  * safe.
  */
trait Repository[FILE <: File]:

  /** Get the root of the repository if it exits.
    * @return
    *   The root or the repository or messages if not found.
    */
  def root: ValidatedNel[String, Directory[FILE]]

  /** Get a directory within a repository.
    * @param path
    *   A relative path.
    * @return
    *   The directory within the repository or an error if the path does not
    *   point to a directory.
    */
  def directory(path: Path): ValidatedNel[String, Directory[FILE]]

  /** Get a file within a repository.
    * @param path
    *   A relative path.
    * @return
    *   The directory within the repository or an error if the path does not
    *   point to a file.
    */
  def file(path: Path): ValidatedNel[String, FILE]

/** The repository of FLAC files.
  */
trait FlacRepository extends Repository[FlacFile]

/** The repository for FLAC files that have yet to be checked in.
  */
trait StagingRepository extends Repository[StagingFile]

/** The repository for encoded files.
  */
trait EncodedRepository extends Repository[EncodedFile]

/** The repository for encoded files.
  */
trait HashRepository extends Repository[HashFile]

/** A container for the four repositories
  */
trait Repositories:

  /** Get the FLAC repository.
    * @return
    *   The FLAC repository.
    */
  def flac: FlacRepository

  /** Get the staging repository.
    * @return
    *   The staging repository.
    */
  def staging: StagingRepository

  /** Get the encoded repository.
    * @param extension
    *   The type of lossy file.
    * @return
    *   The encoded repository.
    */
  def encoded(extension: LossyExtension): EncodedRepository

  def hash(owner: String): HashRepository

/** An abstraction of a file. Each file is relative to the root of a repository.
  */
sealed trait File:

  /** True if this file should be read only, false otherwise.
    */
  val readOnly: Boolean

  /** The top level directory for this file. This is the top level directory
    * whose read/write permissions need to be altered to be able to write this
    * file.
    */
  val rootPath: Path

  /** The base directory for this file. This is the root file of the repository.
    */
  val basePath: Path

  /** The relative path of this file. Together with the base path, this
    * comprises the actual path of the file.
    */
  val relativePath: Path

  /** The absolute path of this file.
    */
  lazy val absolutePath: Path

  /** A container for the audio tags for this file.
    */
  val tags: TagsContainer

  /** True if this file is an actual file or false otherwise.
    */
  lazy val exists: Boolean

  /** The last modified time of this file.
    */
  lazy val lastModified: Instant

  /** The size of this file
    */
  lazy val size: Long

  /** The sequence of strings used to find this file in a tree.
    */
  lazy val treePath: Seq[String]

  def toBackupFile: Option[BackupFile]

/** Contains orderings for files.
  */
object File:

  /** Order files by their absolute path.
    * @tparam F
    *   The type of file to order.
    * @return
    *   An ordering that orders file by their absolute path.
    */
  implicit def fileOrdering[F <: File]: Ordering[F] =
    Ordering.by(_.absolutePath.toString)

/** A container for audio tags for a file. This allows for tags to be read only
  * once and evaluated lazily.
  */
trait TagsContainer:

  /** Read the audio tags from a file
    * @return
    *   The tags for the file or error messages if they cannot be read.
    */
  def read(): ValidatedNel[String, Tags]

/** A file inside the FLAC repository.
  */
trait FlacFile extends File:

  def toHashFile(owner: String): HashFile

  /** The location of this file should it be checked out.
    * @return
    *   The location of this file should it be checked out.
    */
  def toStagingFile: StagingFile

  /** The location of where the encoded version of this file resides.
    * @param extension
    *   The type of lossy encoded used to encode the file.
    * @return
    *   The location of where the encoded version of this file resides.
    */
  def toEncodedFile(extension: LossyExtension): EncodedFile

trait HashFile extends File:
  def toEncodedFile(extension: LossyExtension): EncodedFile

  /** The owner of the FLAC file this hash file represents.
    */
  val owner: String

/** A file in the staging repository.
  */
trait StagingFile extends File:

  /** Determine whether this file is a valid FLAC file.
    * @return
    *   True if this file is a FLAC file, false otherwise.
    */
  def isFlacFile: Boolean

  /** Read this file's audio tags and convert it to a file in the FLAC
    * repository.
    * @return
    *   Either the relevant FLAC file and its tags or errors.
    */
  def toFlacFileAndTags: ValidatedNel[String, (FlacFile, Tags)]

  /** Update this staging file with new tags.
    * @param tags
    *   The new tags to write.
    * @return
    *   This staging file with its new tags.
    */
  def writeTags(tags: Tags): StagingFile

/** A file in the encoded repository.
  */
trait EncodedFile extends File:
  def toHashFile(owner: String): HashFile

  /** The extension of the file.
    */
  val extension: LossyExtension

  /** Create a temporary file that can be used to create this file.
    * @return
    *   A new temporary file.
    */
  def toTempFile: TempFile

  def toFlacFile: FlacFile

  def encodedRepoPath: Path

/** A temporary file.
  */
trait TempFile extends File:

  /** Write this file's tags to the file system.
    * @return
    *   This file with its tags written.
    */
  def writeTags(): TempFile

/** A backup file.
  */
trait BackupFile extends File {
  // No extra methods
}

/** A directory within a repository.
  * @tparam FILE
  *   The type of files in the repository.
  */
trait Directory[FILE <: File]:

  /** The absolute path of this directory.
    */
  val absolutePath: Path

  /** The path of this directory relative to the repository.
    */
  val relativePath: Path

  /** List all files in this directory and its sub-directories.
    * @return
    *   All the files in this directory and its sub-directories.
    */
  def list: SortedSet[FILE]

  /** List all files in this directory and its sub-directories up to a maximum
    * depth.
    * @param maxDepth
    *   The maximum depth to search for files.
    * @return
    *   All the files in this directory and its sub-directories.
    */
  def list(maxDepth: Int): SortedSet[FILE]

  /** Group all files underneath this directory by their parent directory.
    * @return
    *   A map of all the files underneath this directory, grouped by their
    *   parent directory.
    */
  def group: SortedMap[Directory[FILE], SortedSet[FILE]]

/** Contains type aliases for the four different types of directory as well as
  * ordering.
  */
object Directory:
  type FlacDirectory = Directory[FlacFile]
  type StagingDirectory = Directory[StagingFile]
  type EncodedDirectory = Directory[EncodedFile]
  type HashDirectory = Directory[HashFile]

  val pathOrdering: Ordering[Path] =
    implicit def listOrdering: Ordering[List[String]] =
      case (Nil, Nil) => 0
      case (_, Nil)   => 1
      case (Nil, _)   => -1
      case (x :: xs, y :: ys) =>
        val cmp = x.compare(y)
        if cmp == 0 then listOrdering.compare(xs, ys)
        else cmp
    Ordering.by(path => path.iterator().asScala.map(_.toString).toList)

  /** Order directories by their absolute path.
    * @tparam F
    *   The type of file in the directory.
    * @return
    *   An ordering that orders directories by their absolute path.
    */
  implicit def directoryOrdering[F <: File]: Ordering[Directory[F]] =
    Ordering.by((d: Directory[F]) => d.absolutePath)(pathOrdering)

  /** Order two different types of directory by their absolute path.
    * @tparam F1
    *   The type of files in the first directory.
    * @tparam F2
    *   The type of files in the second directory.
    * @return
    *   An ordering that orders directories by their absolute path.
    */
  implicit def eitherDirectoryOrdering[F1 <: File, F2 <: File]
      : Ordering[Either[Directory[F1], Directory[F2]]] =
    Ordering.by((dd: Either[Directory[F1], Directory[F2]]) =>
      dd.fold(_.absolutePath, _.absolutePath)
    )(pathOrdering)
