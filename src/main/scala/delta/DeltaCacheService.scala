package delta

import org.apache.pekko.Done
import devices.Device

import scala.concurrent.Future

trait DeltaCacheService:

  def clear(): Future[Done]

  def clear(device: Device): Future[Done]

  def get(device: Device): Future[Seq[Delta]]
