package delta

import org.apache.pekko.Done
import devices.Device

import scala.concurrent.Future

trait DeltaService:

  def deltas(
      device: Device
  ): Future[Seq[Delta]]

  def summary(
      device: Device
  ): Future[Seq[DeltaSummary]]
