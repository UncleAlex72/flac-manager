package delta

import java.nio.file.Path
import enumeratum._
import io.circe._
import io.circe.generic.semiauto._
import scala.collection.immutable

case class DeltaSummary(
    path: Path,
    ordering: Int,
    action: DeltaSummaryAction,
    album: Option[DeltaSummaryAlbum]
):
  def withOrdering(ordering: Int): DeltaSummary = copy(ordering = ordering)
  def decorateCoverArtUrl(decorator: String => String): DeltaSummary = copy(
    album = album.map { a =>
      a.copy(coverArtUrl = decorator(a.coverArtUrl))
    }
  )

sealed trait DeltaSummaryAction extends EnumEntry:
  val token: String

case class DeltaSummaryAlbum(
    artist: String,
    title: String,
    tracks: Int,
    coverArtUrl: String
)

object DeltaSummaryAction extends Enum[DeltaSummaryAction]:

  object Added extends DeltaSummaryAction { val token = "added" }
  object Removed extends DeltaSummaryAction { val token = "removed" }

  override def values: immutable.IndexedSeq[DeltaSummaryAction] = findValues

object DeltaSummary:

  implicit val deltaSummaryEncoder: Encoder[DeltaSummary] =
    implicit val pathEncoder: Encoder[Path] =
      Encoder[String].contramap(_.toString)
    implicit val deltaSummaryActionEncoder: Encoder[DeltaSummaryAction] =
      Encoder[String].contramap(_.token)
    implicit val deltaSummaryAlbumEncoder: Encoder[DeltaSummaryAlbum] =
      deriveEncoder[DeltaSummaryAlbum]
    deriveEncoder[DeltaSummary]
