package delta

import org.apache.pekko.Done
import com.typesafe.scalalogging.StrictLogging
import devices.Device
import files.Repositories
import versioning.{Repository, Revision}

import scala.collection.concurrent.Map
import java.nio.file.Path
import scala.concurrent.{ExecutionContext, Future}

class DeltaCacheServiceImpl(
    cache: Map[String, Seq[Delta]],
    repositories: Repositories,
    repository: Repository
)(implicit ec: ExecutionContext)
    extends DeltaCacheService
    with StrictLogging:

  override def clear(): Future[Done] =
    Future:
      cache.clear()
      Done

  override def clear(device: Device): Future[Done] =
    Future:
      cache.remove(device.identifier)
      Done

  override def get(device: Device): Future[Seq[Delta]] =
    Future:
      cache.getOrElseUpdate(device.identifier, build(device))

  private def build(device: Device): Seq[Delta] =
    implicit val revisionOrdering: Ordering[Revision] =
      def typeWeight(revision: Revision) = revision match
        case _: Revision.Added   => 1
        case _: Revision.Removed => -1

      Ordering.by { revision => (typeWeight(revision), revision.hashFile) }

    val hashRepository = repositories.hash(device.owner)
    hashRepository.root.toOption.toSeq.flatMap { hashRootDirectory =>
      val revisions = device.lastCommit match
        case Some(commit) =>
          repository.revisions(device.owner, commit)
        case None =>
          for
            (_, hashFiles) <- hashRootDirectory.group.toSeq
            hashFile <- hashFiles.toSeq
          yield Revision.Added(hashFile)

      val deltas: Seq[Option[Delta]] =
        for revision <- revisions
        yield
          val hashFile = revision.hashFile
          val encodedFile = hashFile.toEncodedFile(device.extension)
          val maybeTrack = if encodedFile.exists then
            val tags = encodedFile.tags.read().toOption.get
            Some(
              Track(
                trackNumber = tags.trackNumber,
                totalTracks = tags.totalTracks,
                title = tags.title,
                album = tags.album,
                artist = tags.albumArtist
              )
            )
          else None
          (revision, maybeTrack) match
            case (_: Revision.Added, Some(track)) =>
              Some(
                CreateFileDelta(
                  lastSynchronised = hashFile.lastModified,
                  path = encodedFile.relativePath,
                  url = encodedFile.relativePath.toString,
                  mimeType = device.extension.mimeType,
                  ordering = 0,
                  track = track,
                  summaryPath = encodedFile.relativePath.getParent,
                  last = false
                )
              )
            case (_: Revision.Removed, _) =>
              Some(
                RemoveFileDelta(
                  path = encodedFile.relativePath,
                  ordering = 0,
                  track = maybeTrack,
                  summaryPath = encodedFile.relativePath.getParent,
                  last = false
                )
              )
            case _ =>
              logger.error(s"Cannot find encoded file ${encodedFile}")
              None
      val indexedDeltas: Seq[Delta] = deltas.flatten.zipWithIndex
        .map { case (delta, index) => delta.withOrdering(index) }
      case class State(
          maybePreviousSummary: Option[Path] = None,
          deltas: Seq[Delta] = Seq.empty
      )
      indexedDeltas
        .foldRight(State()) { case (delta, state) =>
          val isLast = state.maybePreviousSummary match
            case Some(previousSummary) => previousSummary != delta.summaryPath
            case None                  => true
          State(
            maybePreviousSummary = Some(delta.summaryPath),
            deltas = delta.withLast(isLast) +: state.deltas
          )
        }
        .deltas
    }
