package delta
import devices._

import scala.concurrent.{ExecutionContext, Future}

class DeltaServiceImpl(deltaCacheService: DeltaCacheService)(implicit
    ec: ExecutionContext
) extends DeltaService:

  override def deltas(
      device: Device
  ): Future[Seq[Delta]] =
    deltaCacheService.get(device).map { deltas =>
      deltas.drop(device.offset.map(_ + 1).getOrElse(0))
    }

  override def summary(
      device: Device
  ): Future[Seq[DeltaSummary]] =
    val eventualSummariesWithoutOrdering: Future[Seq[DeltaSummary]] =
      deltas(device).map { deltas =>
        deltas.map { delta =>
          val album: Option[DeltaSummaryAlbum] =
            val maybeTrack = delta match
              case create: CreateFileDelta => Some(create.track)
              case remove: RemoveFileDelta => remove.track
            maybeTrack.map { track =>
              DeltaSummaryAlbum(
                artist = track.artist,
                title = track.album,
                tracks = track.totalTracks,
                coverArtUrl = delta.path.getParent.toString
              )
            }
          val action: DeltaSummaryAction = delta match
            case _: CreateFileDelta => DeltaSummaryAction.Added
            case _: RemoveFileDelta => DeltaSummaryAction.Removed
          DeltaSummary(
            path = delta.path.getParent,
            action = action,
            album = album,
            ordering = 0
          )
        }.distinct
      }
    eventualSummariesWithoutOrdering.map { summariesWithoutOrdering =>
      summariesWithoutOrdering.zipWithIndex.map { case (summary, ordering) =>
        summary.withOrdering(ordering)
      }
    }
