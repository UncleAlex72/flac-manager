/*
 * Copyright 2018 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package music

import cats.data.ValidatedNel

import java.nio.file.{FileSystem, Path}
import cats.syntax.option._
import devices.Extension
import io.circe.Codec
import io.circe.generic.semiauto._
import text.{Normaliser, NumberNormaliser}
import validation._

/** A case class that encapsulates information about a music file.
  * @param albumArtistSort
  *   The string used to sort the artist for the album of this track.
  * @param albumArtist
  *   The artist for the entire album
  * @param album
  *   The name of the album for this track
  * @param artist
  *   The name of the artist who recorded this track
  * @param artistSort
  *   The string used to sort the artist who recorded this track
  * @param title
  *   The title of this track
  * @param totalDiscs
  *   The total number of discs included in the release for this track
  * @param totalTracks
  *   The total number of tracks included in the release for this track
  * @param discNumber
  *   The disc number of the disc that contains this track
  * @param albumArtistId
  *   The [[http://www.musicbrainz.org MusicBrainz]] ID of the artist for the
  *   album for this track
  * @param albumId
  *   The [[http://www.musicbrainz.org MusicBrainz]] ID of the album for this
  *   track
  * @param artistId
  *   The [[http://www.musicbrainz.org MusicBrainz]] ID of the artist who
  *   recorded this track
  * @param trackId
  *   The [[http://www.musicbrainz.org MusicBrainz]] ID of this track
  * @param asin
  *   The Amazon identifier for the album of this track
  * @param trackNumber
  *   The number of this track on its album
  * @param coverArt
  *   The cover art for this track
  */
case class Tags(
    albumArtistSort: String,
    albumArtist: String,
    album: String,
    artist: String,
    artistSort: String,
    title: String,
    totalDiscs: Int,
    totalTracks: Int,
    discNumber: Int,
    discSubtitle: Option[String],
    albumArtistId: String,
    albumId: String,
    artistId: String,
    trackId: Option[String],
    asin: Option[String],
    trackNumber: Int,
    coverArt: CoverArt,
    originalDate: Option[String],
    year: Option[String],
    date: Option[String]
):

  /** Create the following relative path:
    * `firstLetterOfSortedAlbumArtist/sortedAlbumArtist/album/trackNumber
    * title.ext` in ASCII
    * @param fs
    *   The underlying file system.
    * @param extension
    *   The extension for the path.
    */
  def asPath(fs: FileSystem, extension: Extension)(implicit
      normaliser: Normaliser,
      numberNormaliser: NumberNormaliser
  ): Path =
    val normalisedAlbumArtistSort = normaliser.normalise(albumArtistSort)
    val firstLetter = normalisedAlbumArtistSort.substring(0, 1)
    val normalisedAlbum = normaliser.normalise(album)
    val normalisedTrack = numberNormaliser.normalise(totalTracks, trackNumber)
    val normalisedTitle = normaliser.normalise(f"$normalisedTrack $title%s")
    fs.getPath(
      firstLetter,
      normalisedAlbumArtistSort,
      normalisedAlbum,
      normalisedTitle + "." + extension.extension
    )

/** Used to generate a validator for tags.
  */
object Tags:

  /** A validator for tags. The following must be present:
    *   1. \- album artist sort,
    *   1. \- album artist,
    *   1. \- album,
    *   1. \- artist,
    *   1. \- artist sort,
    *   1. \- title,
    *   1. \- total disks,
    *   1. \- disc number,
    *   1. \- album artist ID,
    *   1. \- artist ID,
    *   1. \- album ID,
    *   1. \- track ID,
    *   1. \- track number,
    *   1. \- cover art.
    */
  def validate(tags: Tags): ValidatedNel[String, Tags] =
    def v[V](
        p: V => Boolean
    )(v: V, message: String): ValidatedNel[String, V] =
      Option(v).filter(p).toValidNel(message)
    def notNull[V]: (V, String) => ValidatedNel[String, V] = v[V](_ => true)
    def notEmpty: (String, String) => ValidatedNel[String, String] =
      v[String](_.nonEmpty)
    def positive: (Int, String) => ValidatedNel[String, Int] =
      v[Int](n => n > 0)

    (
      notEmpty(tags.albumArtistSort, "Album artist sort is required"),
      notEmpty(tags.albumArtist, "Album artist is required"),
      notEmpty(tags.album, "Album is required"),
      notEmpty(tags.artistSort, "Artist sort is required"),
      notEmpty(tags.title, "Title is required"),
      positive(tags.totalDiscs, "Total discs must be greater than zero"),
      positive(tags.totalTracks, "Total tracks must be greater than zero"),
      positive(tags.discNumber, "Disc number must be greater than zero"),
      notEmpty(tags.albumArtistId, "Album artist ID is required"),
      notEmpty(tags.artistId, "Artist ID is required"),
      notEmpty(tags.albumId, "Album ID is required"),
      positive(tags.discNumber, "Track number must be greater than zero"),
      notNull(tags.coverArt, "Cover art is required")
    ).mapN((_, _, _, _, _, _, _, _, _, _, _, _, _) => tags)

  implicit val tagsCodec: Codec[Tags] = deriveCodec[Tags]
