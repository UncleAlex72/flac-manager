/*
 * Copyright 2018 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package music

import org.apache.pekko.http.scaladsl.model.MediaType
import io.circe.Codec
import io.circe.generic.semiauto.*
import uk.co.unclealex.stringlike.StringLike

import java.util.Base64
import scala.util.Try
import uk.co.unclealex.stringlike.StringLikeJsonSupport.given

/** @author
  *   alex
  */

/** An immutable class that contains information about a cover art picture,
  * namely the image data and its mime type.
  * @param imageData
  *   The binary image data for the cover art.
  * @param mimeType
  *   The mime type for the cover art.
  */
case class CoverArt(imageData: Array[Byte], mediaType: MediaType.Binary)
    derives Codec:

  private lazy val encodedData: String =
    Base64.getEncoder.encodeToString(imageData)

  override def equals(obj: Any): Boolean = obj match
    case cv: CoverArt =>
      this.mediaType == cv.mediaType && this.encodedData == cv.encodedData
    case _ => false

object CoverArt:
  private given StringLike[MediaType.Binary] =
    def encoder(binaryMediaType: MediaType.Binary): String =
      binaryMediaType.value

    def decoder(str: String): Either[String, MediaType.Binary] =
      MediaType.parse(str) match
        case Right(binaryMediaType: MediaType.Binary) =>
          Right(binaryMediaType)
        case _ => Left(s"$str is not a valid binary media type")

    StringLike.fromEither(encoder, decoder)

  private given StringLike[Array[Byte]] =
    def encoder(by: Array[Byte]): String =
      Base64.getEncoder.encodeToString(by)

    def decoder(str: String): Try[Array[Byte]] = Try(
      Base64.getDecoder.decode(str)
    )

    StringLike.fromTry(encoder, decoder)
