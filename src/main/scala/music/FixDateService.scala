package music

import files.StagingFile

trait FixDateService:
  def fixDates(stagingFile: StagingFile): StagingFile
