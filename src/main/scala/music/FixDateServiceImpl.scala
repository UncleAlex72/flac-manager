package music

import files.StagingFile

class FixDateServiceImpl extends FixDateService:

  override def fixDates(stagingFile: StagingFile): StagingFile =
    val maybeAlteredStagingFile = for
      tags <- stagingFile.tags.read().toOption
      alteredTags <- alterTags(tags)
    yield stagingFile.writeTags(alteredTags)
    maybeAlteredStagingFile.getOrElse(stagingFile)

  def alterTags(tags: Tags): Option[Tags] =
    tags.originalDate.map { originalDate =>
      tags.copy(date = Some(originalDate), year = Some(originalDate.take(4)))
    }
