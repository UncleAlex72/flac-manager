package health

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source

trait HealthCheckService:

  def runHealthCheck: Source[String, NotUsed]
