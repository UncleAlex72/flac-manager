package health

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.{Flow, Keep, Source}
import cats.data.Validated.{Invalid, Valid}
import cats.data.ValidatedNel
import devices.LossyExtension
import files.{Directory, EncodedFile, File, FlacFile, HashFile, Repositories}
import validation.Validations
import cats.syntax.apply._
import files.Directory.{EncodedDirectory, FlacDirectory, HashDirectory}
import logging.Messages
import users.OwnerService

import scala.concurrent.{ExecutionContext, Future}
class HealthCheckServiceImpl(
    repositories: Repositories,
    ownerService: OwnerService
)(implicit
    ec: ExecutionContext
) extends HealthCheckService:

  def runHealthCheck: Source[String, NotUsed] =
    Source
      .futureSource:
        for owners <- ownerService.owners()
        yield runHealthCheck(owners)
      .mapMaterializedValue(_ => NotUsed)

  def runHealthCheck(owners: Seq[String]): Source[String, NotUsed] =
    val validatedEncodedRootDirectories = Validations.sequence(
      LossyExtension.lossyExtensions.map(repositories.encoded).map(_.root)
    )
    val validatedFlacRootDirectory = repositories.flac.root
    val validatedHashRootDirectories = Validations.sequence(
      owners.map(repositories.hash).map(_.root)
    )

    val validatedMessageSource: ValidatedNel[String, Source[String, NotUsed]] =
      (
        validatedFlacRootDirectory,
        validatedEncodedRootDirectories,
        validatedHashRootDirectories
      ).mapN:
        case (flacRootDirectory, encodedRootDirectories, hashRootDirectories) =>
          runHealthCheck(
            flacRootDirectory,
            encodedRootDirectories,
            hashRootDirectories
          )
    validatedMessageSource match
      case Valid(source)   => source
      case Invalid(errors) => Source(errors.toList)

  def runHealthCheck(
      flacRootDirectory: FlacDirectory,
      encodedRootDirectories: Seq[EncodedDirectory],
      hashRootDirectories: Seq[HashDirectory]
  ): Source[String, NotUsed] =
    val flacSource: Source[String, NotUsed] =
      partialHealthCheck[FlacFile, EncodedFile](
        flacRootDirectory,
        flacFile =>
          LossyExtension.lossyExtensions.map { extension =>
            flacFile.toEncodedFile(extension)
          }
      )
    val flacAndEncodedSources: Source[String, NotUsed] =
      encodedRootDirectories.foldLeft(flacSource):
        (acc, encodedRootDirectory) =>
          val encodedFileSource =
            partialHealthCheck[EncodedFile, FlacFile](
              encodedRootDirectory,
              encodedFile => Seq(encodedFile.toFlacFile)
            )
          acc.concat(encodedFileSource)
    hashRootDirectories.foldLeft(flacAndEncodedSources):
      (acc, hashRootDirectory) =>
        val hashFileSource = partialHealthCheck[HashFile, EncodedFile](
          hashRootDirectory,
          hashFile => LossyExtension.lossyExtensions.map(hashFile.toEncodedFile)
        )
        acc.concat(hashFileSource)

  private def partialHealthCheck[S <: File, T <: File](
      sourceDirectory: Directory[S],
      targetFilesFactory: S => Seq[T]
  ): Source[String, NotUsed] =
    val sourceFilesSource: Source[S, Future[NotUsed]] =
      Source.lazyFutureSource { () =>
        Future:
          Source(sourceDirectory.list.toSeq)
      }
    val sourceAndTargetFlow: Flow[S, (S, T), NotUsed] = Flow[S].flatMapConcat:
      source =>
        Source(targetFilesFactory(source).map(target => source -> target))
    val checkTargetExistsFlow: Flow[(S, T), String, NotUsed] =
      Flow[(S, T)].flatMapConcat { case (sourceFile, targetFile) =>
        Source.futureSource:
          for targetExists <- Future.successful(targetFile.exists)
          yield
            if targetExists then Source.empty
            else Source.single(Messages.missingFile(sourceFile, targetFile))
      }
    sourceFilesSource
      .viaMat(sourceAndTargetFlow)(Keep.right)
      .via(checkTargetExistsFlow)
