package webdav

import java.nio.file.Path

case class WebDavResourceContainer(
    lastModified: Long,
    size: Long,
    url: String,
    webDavResource: WebDavResource
)

sealed trait WebDavResource:
  def children(): Seq[WebDavResourceContainer]

case class FileWebDavResource(localResourcePath: Path) extends WebDavResource:
  override def children(): Seq[WebDavResourceContainer] = Seq.empty

case class DirectoryWebDavResource(
    childrenFactory: () => Seq[WebDavResourceContainer]
) extends WebDavResource:
  override def children(): Seq[WebDavResourceContainer] = childrenFactory()
