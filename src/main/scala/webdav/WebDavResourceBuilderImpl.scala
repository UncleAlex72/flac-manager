package webdav

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.Sink
import devices.Extension.PathExtensions

import java.nio.file.{Files, Path}
import java.time.Instant
import devices.{Device, DeviceDao}
import files.Repositories
import uk.co.unclealex.futures.Futures._

import scala.collection.immutable.Seq
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Using
import scala.jdk.StreamConverters._

class WebDavResourceBuilderImpl(
    val deviceDao: DeviceDao,
    val repositories: Repositories
)(implicit val ec: ExecutionContext, actorSystem: ActorSystem)
    extends WebDavResourceBuilder:

  override def buildDevicesResource(
      baseUrl: String,
      urlBuilder: String => String
  ): FutureOption[WebDavResourceContainer] =
    val eventualChildren =
      deviceDao.allDevices().runWith(Sink.collection).flatMap { devices =>
        val empty: Future[Seq[WebDavResourceContainer]] =
          Future.successful(Seq.empty)
        devices.foldLeft(empty) { (acc, device) =>
          acc.flatMap { children =>
            buildTreeResource(device.identifier, urlBuilder).value.map:
              maybeResource => children ++ maybeResource.toSeq
          }
        }
      }
    fo(eventualChildren.map { children =>
      WebDavResourceContainer(
        lastModified =
          (Instant.EPOCH.toEpochMilli +: children.map(_.lastModified)).max,
        size = 4096L,
        url = baseUrl,
        webDavResource = DirectoryWebDavResource(() => children)
      )
    })

  override def buildTreeResource(
      path: String,
      urlBuilder: String => String
  ): FutureOption[WebDavResourceContainer] =
    val (mIdentifier: Option[String], mPath: Option[String]) =
      val pathRegex = """([a-z]+)/?(.*)""".r
      path match
        case pathRegex(deviceIdentifier, path) =>
          (Some(deviceIdentifier), Some(path))
        case _ => (None, None)
    for
      identifier <- fo(mIdentifier)
      path <- fo(mPath)
      device <- fo(
        deviceDao.findByIdentifier(identifier).runWith(Sink.headOption)
      )
      encodedRoot <- fo(
        repositories
          .encoded(device.extension)
          .root
          .toOption
      ).map(_.absolutePath)
      hashRoot <- fo(
        repositories
          .hash(device.owner)
          .root
          .toOption
      ).map(_.absolutePath)
      resource <- fo(
        buildResource(urlBuilder, device, hashRoot, encodedRoot, path)
      )
    yield resource

  def buildResource(
      urlBuilder: String => String,
      device: Device,
      hashRoot: Path,
      encodedRoot: Path,
      path: String
  ): Option[WebDavResourceContainer] =
    val hashPath = hashRoot.resolve(path)
    if Files.isDirectory(hashPath) then
      val childFiles = Using(Files.list(hashPath))(_.toScala(Seq)).get.sorted
      if childFiles.isEmpty then None
      else
        Some:
          WebDavResourceContainer(
            lastModified = Files.getLastModifiedTime(hashPath).toMillis,
            size = Files.size(hashPath),
            url = urlBuilder(s"${device.identifier}/$path"),
            webDavResource = DirectoryWebDavResource(() =>
              childFiles.flatMap { child =>
                val childPathPrefix = if path.isEmpty then "" else s"$path/"
                val childPath = s"$childPathPrefix${child.getFileName}"
                buildResource(
                  urlBuilder,
                  device,
                  hashRoot,
                  encodedRoot,
                  childPath
                )
              }
            )
          )
    else if Files.isRegularFile(hashPath.withExtension("hash")) then
      for hashFile <- repositories
          .hash(device.owner)
          .file(hashPath.getFileSystem.getPath(path).withExtension("hash"))
          .toOption
      yield
        val encodedFile = hashFile.toEncodedFile(device.extension)

        WebDavResourceContainer(
          lastModified = hashFile.lastModified.toEpochMilli,
          size = encodedFile.size,
          url = urlBuilder(s"${device.identifier}/${encodedFile.relativePath}"),
          webDavResource = FileWebDavResource(encodedFile.absolutePath)
        )
    else None
