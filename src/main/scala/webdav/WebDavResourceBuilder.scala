package webdav

import uk.co.unclealex.futures.Futures._

trait WebDavResourceBuilder:

  def buildDevicesResource(
      baseUrl: String,
      urlBuilder: String => String
  ): FutureOption[WebDavResourceContainer]

  def buildTreeResource(
      path: String,
      urlBuilder: String => String
  ): FutureOption[WebDavResourceContainer]
