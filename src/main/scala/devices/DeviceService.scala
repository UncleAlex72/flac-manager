package devices

import files.EncodedFile

import scala.concurrent.Future

trait DeviceService:
  def updateOffset(identifier: String, offset: Int): Future[Option[Device]]

  def list(device: Device): Future[Seq[EncodedFile]]

  def updateLastSynchronised(
      identifier: String
  ): Future[Option[Device]]

  def addDevice(newDevice: NewDevice): Future[Device]

  def removeDevice(identifier: String): Future[Boolean]
