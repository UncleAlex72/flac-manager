package devices

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.Sink
import com.typesafe.scalalogging.StrictLogging
import delta.DeltaCacheService
import files.{Directory, EncodedFile, HashFile, Repositories}
import uk.co.unclealex.futures.Futures._
import versioning.Repository

import java.time.{Clock, Instant}
import scala.collection.immutable.SortedSet
import scala.concurrent.{ExecutionContext, Future}

class DeviceServiceImpl(
    deviceDao: DeviceDao,
    deviceIdentifierService: DeviceIdentifierService,
    deltaCacheService: DeltaCacheService,
    clock: Clock,
    repository: Repository,
    val repositories: Repositories
)(implicit ec: ExecutionContext, actorSystem: ActorSystem)
    extends DeviceService
    with StrictLogging:

  override def addDevice(newDevice: NewDevice): Future[Device] =
    for
      identifier <- deviceIdentifierService.generate(newDevice.displayName)
      device = newDevice.toDevice(identifier)
      persistedDevice <- deviceDao.store(device).runWith(Sink.head)
    yield persistedDevice

  override def removeDevice(identifier: String): Future[Boolean] =
    deviceDao
      .findByIdentifier(identifier)
      .runWith(Sink.headOption)
      .flatMap:
        case Some(device) =>
          deviceDao
            .removeById(device._id.get)
            .map(_ => true)
            .runWith(Sink.head[Boolean])
        case None => Future.successful(false)

  override def list(device: Device): Future[Seq[EncodedFile]] =
    Future:
      for
        rootDirectory <- repositories.hash(device.owner).root.toOption.toSeq
        hashFile <- rootDirectory.list.toSeq
      yield hashFile.toEncodedFile(device.extension)

  override def updateLastSynchronised(
      identifier: String
  ): Future[Option[Device]] =
    val now = clock.instant()
    val latestCommit = repository.latestCommit()
    logger.info(
      s"Updating device $identifier to last synchronised at $now and commit $latestCommit"
    )
    deviceDao
      .updateLastSynchronised(identifier, now, latestCommit)
      .flatMapConcat { _ =>
        deviceDao.updateLastSynchronised(identifier, now, latestCommit)
      }
      .runWith(Sink.headOption)
      .flatMap:
        case Some(device) =>
          logger.info(s"Clearing the cache for device ${device.identifier}")
          deltaCacheService.clear(device).map(_ => Some(device))
        case None => Future.successful(None)

  override def updateOffset(
      identifier: String,
      offset: Int
  ): Future[Option[Device]] =
    deviceDao.updateOffset(identifier, offset).runWith(Sink.headOption)
