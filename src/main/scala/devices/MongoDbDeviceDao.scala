package devices

import org.apache.pekko.NotUsed
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.Source
import uk.co.unclealex.mongodb.dao.Database.Database
import uk.co.unclealex.mongodb.dao.{Index, MongoDbDao}

import java.time.{Clock, Instant}
import scala.concurrent.{ExecutionContext, Future}

class MongoDbDeviceDao(
    eventualDatabase: Future[Database],
    clock: Clock
)(implicit actorSystem: ActorSystem, ec: ExecutionContext)
    extends MongoDbDao[Device](eventualDatabase, clock, "devices")
    with DeviceDao:

  override def allDevices(): Source[Device, NotUsed] = findAll()

  override def findByDisplayName(
      displayName: String
  ): Source[Device, NotUsed] =
    findOne("displayName" === displayName)

  override def findByIdentifier(identifier: String): Source[Device, NotUsed] =
    findOne("identifier" === identifier)

  override def updateLastSynchronised(
      identifier: String,
      lastSynchronised: Instant,
      lastCommit: String
  ): Source[Device, NotUsed] =
    update(
      "identifier" === identifier,
      _.copy(
        lastSynchronised = Some(lastSynchronised),
        lastCommit = Some(lastCommit),
        offset = None
      )
    )

  override def updateOffset(
      identifier: String,
      offset: Int
  ): Source[Device, NotUsed] =
    update(
      "identifier" === identifier,
      _.copy(offset = Some(offset))
    )

  override val indicies: Seq[Index[Device]] = Seq(
    index.on("identifier").ascending.unique.named("identifier_1"),
    index.on("displayName").ascending.unique.named("displayName_1")
  )
