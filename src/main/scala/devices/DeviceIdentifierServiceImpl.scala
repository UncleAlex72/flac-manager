package devices
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.Sink
import text.Normaliser
import text.Normaliser._

import scala.concurrent.{ExecutionContext, Future}

class DeviceIdentifierServiceImpl(deviceDao: DeviceDao)(implicit
    ec: ExecutionContext,
    actorSystem: ActorSystem,
    normaliser: Normaliser
) extends DeviceIdentifierService:

  override def generate(displayName: String): Future[String] =
    val normalisedDisplayName: String =
      displayName.normalise.toLowerCase().replaceAll("\\s", "")
    deviceDao
      .allDevices()
      .runWith(Sink.collection[Device, Seq[Device]])
      .map: devices =>
        val identifiers = devices.map(_.identifier)
        val stream: LazyList[OriginalNameAndSuffix] =
          LazyList
            .iterate(OriginalNameAndSuffix(normalisedDisplayName))(_.next())
        val result = stream
          .dropWhile(el => identifiers.contains(el.calculatedIdentifier))
          .head
        result.calculatedIdentifier

  case class OriginalNameAndSuffix(
      originalIdentifier: String,
      maybeSuffix: Option[Int] = None
  ):
    val calculatedIdentifier: String = maybeSuffix match
      case Some(suffix) => s"$originalIdentifier$suffix"
      case _            => originalIdentifier

    def next(): OriginalNameAndSuffix =
      val nextSuffix: Int = maybeSuffix.getOrElse(0) + 1
      copy(maybeSuffix = Some(nextSuffix))
