package devices

import java.nio.file.Path
import enumeratum.*
import io.circe.{Codec, Decoder, Encoder}
import org.apache.commons.io.FilenameUtils
import uk.co.unclealex.stringlike.StringLike

import scala.collection.immutable.*
import scala.util.Try
import scala.util.matching.Regex

/** An enumeration of all the known music file extensions.
  * @author
  *   alex
  */
sealed trait Extension extends EnumEntry:

  /** The string to put at the end of a file.
    * @return
    */
  def extension: String

  /** The extension itself.
    * @return
    *   The extension itself.
    */
  override def toString: String = extension

  /** The mime type associated with this extension.
    * @return
    *   The extension itself.
    */
  def mimeType: String = extension

  /** True if this extension is lossy, false otherwise.
    * @return
    */
  def isLossy: Boolean = false

/** A marker trait for lossy extensions.
  */
sealed trait LossyExtension extends Extension:
  override def isLossy: Boolean = true

/** A container for the different extensions.
  */
object Extension extends Enum[Extension]:

  /** The base for all extensions.
    * @param extension
    *   The extension string.
    * @param mimeType
    *   The mime type of the extension.
    */
  sealed case class ExtensionImpl(
      override val extension: String,
      override val mimeType: String
  ) extends Extension

  /** The extension for M4A files.
    */
  object M4A extends ExtensionImpl("m4a", "audio/m4a") with LossyExtension

  /** The extension for MP3 files.
    */

  object MP3 extends ExtensionImpl("mp3", "audio/mpeg") with LossyExtension

  /** The extension for flac files.
    */
  object FLAC extends ExtensionImpl("flac", "audio/flac") {}

  /** Get all known extensions.
    * @return
    *   All known extensions.
    */
  override def values: IndexedSeq[Extension] = findValues

  /** Implicits for [[Path]]s that supply functionality to check the extension
    * of a path or to change it.
    * @param path
    *   The path to extend.
    */
  implicit class PathExtensions(path: Path):

    /** Change the extension of a path.
      * @param extension
      *   The new extension of the path.
      * @return
      *   A path with the given extension, replacing any other extension.
      */

    def withExtension(extension: Extension): Path =
      withExtension(extension.extension)

    def withExtension(extension: String): Path =
      val pathAsString = path.toString
      val pathWithoutExtension = FilenameUtils.removeExtension(path.toString)
      if pathAsString == pathWithoutExtension then path
      else
        val newPath =
          s"${FilenameUtils.removeExtension(path.toString)}.$extension"
        path.getFileSystem.getPath(newPath)

    /** Check to see if a path has an extension.
      * @param extension
      *   The extension to look for.
      * @return
      *   True if the path has the extension, false otherwise.
      */
    def hasExtension(extension: Extension): Boolean =
      path.getFileName.toString.endsWith(s".$extension")

    /** Get the extension of this file.
      * @return
      *   The extension of this file if it has a known extension, none
      *   otherwise.
      */
    def extension: Option[Extension] =
      Extension.values.find(hasExtension)

  /** Allow extensions to be referenced in URLs.
    * @return
    *   A path binder allowing extensions to be referenced in URLs.
    */

  private def fromString[E <: Extension](
      values: Seq[E],
      token: String
  ): Option[E] =
    val lowerCaseToken = token.toLowerCase
    values.find(_.extension.toLowerCase == lowerCaseToken)

object LossyExtension:
  val lossyExtensions: Seq[LossyExtension] = Extension.values.flatMap:
    case lossy: LossyExtension => Seq(lossy)
    case _                     => Seq.empty

  given StringLike[LossyExtension] =
    StringLike.fromEither(
      _.extension,
      str =>
        lossyExtensions
          .find(_.extension == str)
          .toRight(s"$str is not a valid lossy extension")
    )

  def fromString(ext: String): Try[LossyExtension] =
    summon[StringLike[LossyExtension]].tryDecode(ext)

  implicit val lossyExtensionCodec: Codec[LossyExtension] =
    StringLike.circeCodec[LossyExtension]
