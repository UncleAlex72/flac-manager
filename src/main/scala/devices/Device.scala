package devices

import io.circe.Codec
import org.bson.codecs.configuration.{CodecRegistries, CodecRegistry}

import java.time.Instant
import uk.co.unclealex.mongodb.*
import LossyExtension.*
import json.*
import uk.co.unclealex.mongodb.bson.ID
import uk.co.unclealex.mongodb.persistable.{
  HasDateCreated,
  HasLastUpdated,
  Persistable
}

case class Device(
    _id: Option[ID],
    displayName: String,
    identifier: String,
    owner: String,
    extension: LossyExtension,
    lastCommit: Option[String],
    lastSynchronised: Option[Instant],
    offset: Option[Int],
    dateCreated: Option[Instant],
    lastUpdated: Option[Instant]
) derives Persistable,
      HasLastUpdated,
      HasDateCreated,
      Codec:

  def lastSynchronisedOrEpoch: Instant =
    lastSynchronised.getOrElse(Instant.EPOCH)

case class NewDevice(
    displayName: String,
    owner: String,
    extension: LossyExtension
) derives Codec:
  def toDevice(identifier: String): Device =
    Device(
      None,
      displayName,
      identifier,
      owner,
      extension,
      None,
      None,
      None,
      None,
      None
    )

object Device:

  def apply(
      displayName: String,
      identifier: String,
      owner: String,
      extension: LossyExtension
  ): Device =
    Device(
      None,
      displayName,
      identifier,
      owner,
      extension,
      None,
      None,
      None,
      None,
      None
    )
