package devices

import java.time.Instant
import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source
import uk.co.unclealex.mongodb.bson.ID

import scala.concurrent.Future

trait DeviceDao:
  def updateOffset(identifier: String, offset: Int): Source[Device, NotUsed]

  def allDevices(): Source[Device, NotUsed]

  def findByDisplayName(displayName: String): Source[Device, NotUsed]

  def findByIdentifier(identifier: String): Source[Device, NotUsed]

  def store(device: Device): Source[Device, NotUsed]

  def updateLastSynchronised(
      identifier: String,
      lastSynchronised: Instant,
      lastCommit: String
  ): Source[Device, NotUsed]

  def removeById(id: ID): Source[Long, NotUsed]
