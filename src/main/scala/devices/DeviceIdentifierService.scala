package devices

import scala.concurrent.Future

trait DeviceIdentifierService:

  def generate(displayName: String): Future[String]
