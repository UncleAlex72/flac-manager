package logging

import java.time.{Clock, Duration}

import com.typesafe.scalalogging.StrictLogging
import uk.co.unclealex.futures.Futures._

import scala.concurrent.{ExecutionContext, Future}

class TimerImpl(clock: Clock)(implicit ec: ExecutionContext)
    extends Timer
    with StrictLogging:
  override def timeF[V](message: String)(block: => Future[V]): Future[V] =
    val start = clock.instant()
    logger.info(s"Starting: $message")
    block.transform { result =>
      val finish = clock.instant()
      val timeElapsed = Duration.between(start, finish).toMillis
      logger.info(s"Finished: $message (${timeElapsed}ms)")
      result
    }

  override def timeFO[V](
      message: String
  )(block: => FutureOption[V]): FutureOption[V] = fo:
    val maybeEventualResult = block.value;
    timeF(message)(maybeEventualResult)
