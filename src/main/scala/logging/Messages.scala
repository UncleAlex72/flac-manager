/*
 * Copyright 2018 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package logging

import java.io.{PrintWriter, StringWriter}
import java.nio.file.Path
import files.{Directory, File, FlacFile, HashFile, StagingFile}

object Messages:
  def hashFileCreated(source: FlacFile, target: HashFile): String =
    s"Created hash file $target"

  def hashFileRemoved(hashFile: HashFile): String =
    s"Removed hash file $hashFile"

  def progress(index: Int, total: Int): String =
    s"$index of $total"
  def invalidExtension(extension: String): String =
    s"$extension is not a valid extension."

  def done(): String =
    "done"

  def noFiles(files: Iterable[File]): String =
    s"noFiles = Could not find any valid files in ${files.mkString(", ")}."

  /** The key for producing an encoding message.
    */
  def encode(sourceFile: File, targetFile: File): String =
    s"Encoding $sourceFile to $targetFile"

  /** The key for producing a delete message.
    */
  def delete(file: File): String =
    s"Removing $file"

  /** The key for producing a checkin message.
    */
  def checkin(stagingFile: StagingFile): String =
    s"Checking in $stagingFile"

  /** The key for producing a checkin message.
    */
  def checkout(flacFile: FlacFile): String =
    s"Checking out $flacFile"

  /** The key for producing a move message.
    */
  def move(source: File, target: File): String =
    s"Moving $source to $target"

  /** The key for producing a not flac file message.
    */
  def invalidFlac(file: File): String =
    s"$file is not a valid FLAC file."

  /** The key for producing invalid tag messages.
    *
    * @param tagError
    *   The error returned from the tags.
    */
  def invalidTags(source: Path, tagError: String): String =
    s"Invalid tag for file $source: $tagError"

  /** The key for producing an overwrite message.
    */
  def overwrite(source: File, target: File): String =
    s"$source would overwrite $target"

  /** The key for producing non unique messages.
    */
  def nonUnique(flacFile: FlacFile, stagingFiles: Seq[StagingFile]): String =
    s"The following files all resolve to $flacFile: ${stagingFiles.mkString(", ")}"

  /** The key for producing multi disc messages.
    */
  def multiDisc(stagingFile: StagingFile): String =
    s"$stagingFile is not on the first disc of an album."

  /** The key for alter tags messages.
    */
  def alterTags(
      stagingFile: StagingFile,
      key: String,
      originalValue: String,
      newValue: String
  ): String =
    s"alterTags = Changing tag $key on file $stagingFile from $originalValue to $newValue"

  /** The key for producing add owner messages.
    */
  def notOwned(stagingFile: StagingFile): String =
    s"$stagingFile has no owners."

  /** The key for producing a message to say that a file has been found.
    */
  def foundFile(file: File): String =
    s"Found file $file"

  /** The key for producing a message to say that devices are being searched.
    */
  def lookingForDevices(): String =
    "Looking for connected devices."

  def cachesCleared(): String =
    "All caches have been cleared."

  /** The key for producing te no users message.
    */
  def noOwners(): String =
    "You must supply at least one owner."

  def notEnoughThreads(): String =
    "The number of threads for calibration must be at least 1."

  def noDirectories(repositoryType: String): String =
    s"noDirectories = You must supply at least one $repositoryType directory."

  def notADirectory(path: Path, repositoryType: String): String =
    s"$path is not a valid $repositoryType directory."

  def notAFile(path: Path, repositoryType: String): String =
    s"$path is not a valid $repositoryType file."

  def multiActionRequired(): String =
    "You must include an action to deal with multiple disc albums."

  /** The key for producing invalid user messages.
    *
    * @param username
    *   The invalid user name.
    */
  def invalidOwner(owner: String): String =
    s"$owner is not a valid user."

  def missingFile(sourceFile: File, targetFile: File): String =
    s"${sourceFile.relativePath} is missing corresponding ${targetFile.relativePath}"

  def exception(t: Throwable): String =
    val stringWriter = new StringWriter
    val printWriter = new PrintWriter(stringWriter)
    printWriter.println(t.getMessage)
    t.printStackTrace(printWriter)
    stringWriter.toString

  def error(message: String): String =
    message
