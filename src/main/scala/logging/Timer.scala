package logging

import scala.concurrent.Future
import uk.co.unclealex.futures.Futures._

trait Timer:

  def timeF[V](message: String)(block: => Future[V]): Future[V]
  def timeFO[V](message: String)(block: => FutureOption[V]): FutureOption[V]
