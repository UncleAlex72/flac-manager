package controllers

import org.apache.pekko.http.scaladsl.model.{ContentType, HttpEntity}
import org.apache.pekko.http.scaladsl.server.Route
import devices.LossyExtension
import files._
import music.TagsService
import org.apache.commons.io.FilenameUtils
import routes.PathSegment._

import java.nio.file.{FileSystem, Path}
import scala.concurrent.ExecutionContext

class MusicController(
    val repositories: Repositories,
    tagsService: TagsService
)(implicit val fs: FileSystem, ec: ExecutionContext)
    extends Controller:

  override def route: Route =
    concat(
      path(MUSIC / Segments) { segments =>
        get {
          music(pathOf(segments))
        }
      },
      path(COVER_ART / Segments) { segments =>
        get {
          coverArt(pathOf(segments))
        }
      }
    )

  def music(path: String): Route =
    whenPresent(
      for
        ext <- Option(FilenameUtils.getExtension(path))
        lossyExtension <- LossyExtension.fromString(ext).toOption
        source <- maybePath[EncodedFile, EncodedRepository](
          repositories.encoded(lossyExtension),
          path
        )
      yield {
        source.toFile
      },
      s"Cannot find a music file at $path"
    ) { file =>
      getFromFile(file)
    }

  def coverArt(path: String): Route =
    whenPresent(
      for
        directory <- repositories.flac.directory(fs.getPath(path)).toOption
        flacFile <- directory.list(1).headOption
        tags <- tagsService.read(flacFile.absolutePath).toOption
      yield {
        tags.coverArt
      },
      s"Cannot find any music files in $path"
    ) { coverArt =>
      complete:
        HttpEntity(ContentType.Binary(coverArt.mediaType), coverArt.imageData)
    }

  def maybePath[F <: File, R <: Repository[F]](
      repository: R,
      path: String
  ): Option[Path] =
    repository.file(fs.getPath(path)).map(_.absolutePath).toOption
