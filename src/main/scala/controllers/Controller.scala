package controllers

import com.github.pjfanning.pekkohttpcirce.FailFastCirceSupport
import org.apache.pekko.http.scaladsl.model.RequestEntityAcceptance.Expected
import org.apache.pekko.http.scaladsl.model.{HttpMethod, Uri}
import org.apache.pekko.http.scaladsl.server.util.Tupler
import org.apache.pekko.http.scaladsl.server.{Directive, Directives, Route}
import io.circe.Json
import io.circe.syntax._
import routes.PathSegment
import routes.PathSegment.{COVER_ART, DAV, MUSIC}
import uk.co.unclealex.futures.Futures.FutureOption

import scala.concurrent.Future

trait Controller
    extends FailFastCirceSupport
    with Directives
    with CustomMethods
    with PathSegment.ServerExtensions:

  def route: Route

  def pathOf(segments: List[String]): String =
    segments.mkString("/")

  private def musicControllerDirective(
      prefix: PathSegment
  ): Directive[Tuple1[String => String]] =
    extractUri.map { uri => (path: String) =>
      {
        val newPath =
          path
            .split('/')
            .foldLeft(Uri.Path(s"/${prefix.segmentName}")):
              (uriPath, segment) => uriPath / segment
        uri.withPath(newPath).toString()
      }
    }

  val extractMusicUriFunction: Directive[Tuple1[String => String]] =
    musicControllerDirective(MUSIC)
  val extractCoverArtUriFunction: Directive[Tuple1[String => String]] =
    musicControllerDirective(COVER_ART)
  val extractDavUriFunction: Directive[Tuple1[String => String]] =
    musicControllerDirective(DAV)

  trait WhenPresentMagnet:
    type Out

    def directive: Directive[Out]

  object WhenPresentMagnet:
    implicit def apply[T: Tupler](
        info: => (Option[T], String)
    )(using tupler: Tupler[T]): WhenPresentMagnet { type Out = tupler.Out } =
      new WhenPresentMagnet:
        val (maybeValue, errorMessage) = info
        type Out = tupler.Out
        val directive: Directive[tupler.Out] = Directive[tupler.Out] {
          inner => ctx =>
            maybeValue match
              case Some(value) => inner(tupler(value))(ctx)
              case None =>
                complete(
                  Json.obj(
                    "error" -> "NotFound".asJson,
                    "message" -> errorMessage.asJson
                  )
                )(ctx)
        }(using tupler.OutIsTuple)

  def whenPresent(magnet: WhenPresentMagnet): Directive[magnet.Out] =
    magnet.directive

  def onSuccessWhenPresent[T](
      maybeEventualValue: Future[Option[T]],
      onNotFound: => String
  ): Directive[Tuple1[T]] =
    onSuccess(maybeEventualValue).flatMap { maybeValue =>
      whenPresent(maybeValue, onNotFound)
    }

  def onSuccessWhenPresent[T](
      maybeEventualValue: FutureOption[T],
      onNotFound: => String
  ): Directive[Tuple1[T]] =
    onSuccessWhenPresent(maybeEventualValue.value, onNotFound)
