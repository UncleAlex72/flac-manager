package controllers

import org.apache.pekko.http.scaladsl.model.headers.{
  ModeledCustomHeader,
  ModeledCustomHeaderCompanion
}

import scala.util.Try

trait DavHeaders:

  final class DavHeader(token: String) extends ModeledCustomHeader[DavHeader]:
    override def renderInRequests = false

    override def renderInResponses = true

    override val companion: DavHeader.type = DavHeader

    override def value: String = token

  object DavHeader extends ModeledCustomHeaderCompanion[DavHeader]:
    def apply(flag: Boolean) = new DavHeader(if flag then {
      "1"
    } else {
      "0"
    })

    override val name = "DAV"

    override def parse(value: String): Try[DavHeader] = Try(
      new DavHeader(value)
    )

  final class DepthHeader(token: String)
      extends ModeledCustomHeader[DepthHeader]:
    override def renderInRequests = true

    override def renderInResponses = false

    override val companion: DepthHeader.type = DepthHeader

    override def value: String = token

  object DepthHeader extends ModeledCustomHeaderCompanion[DepthHeader]:
    override val name = "Depth"

    override def parse(value: String): Try[DepthHeader] = Try(
      new DepthHeader(value)
    )
