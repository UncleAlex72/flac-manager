package controllers

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.http.scaladsl.model.HttpEntity
import org.apache.pekko.http.scaladsl.server.Route
import org.apache.pekko.stream.scaladsl.Sink
import com.typesafe.scalalogging.StrictLogging
import devices.{Device, DeviceDao, DeviceService}
import routes.PathSegment._
import io.circe.syntax._

import java.nio.file.{Path, Paths}
import scala.concurrent.{ExecutionContext, Future}

/** The main controller used for interacting with browsers.
  */
class DevicesController(
    val deviceDao: DeviceDao,
    val deviceService: DeviceService
)(implicit val actorSystem: ActorSystem, ec: ExecutionContext)
    extends Controller
    with StrictLogging:

  override def route: Route =
    pathPrefix(DEVICES):
      concat(
        pathEnd {
          get {
            devices()
          }
        },
        pathPrefix(Segment) { identifier =>
          concat(
            path(INFO) {
              get {
                device(identifier)
              }
            },
            path(LAST_SYNCHRONISED) {
              put {
                alterLastSynchronised(identifier)
              }
            },
            path(INITIALISE) {
              post {
                parameter("root") { root =>
                  initialise(identifier, root)
                }

              }
            },
            path(OFFSET) {
              put {
                alterOffset(identifier)
              }
            }
          )
        }
      )

  def devices(): Route =
    complete(deviceDao.allDevices())

  def singleDevice(identifier: String)(block: Device => Route): Route =
    val eventualMaybeDevice: Future[Option[Device]] =
      deviceDao.findByIdentifier(identifier).runWith(Sink.headOption[Device])
    onSuccessWhenPresent(eventualMaybeDevice, noSuchDevice(identifier))(block)

  def device(identifier: String): Route =
    singleDevice(identifier) { device =>
      complete(device)
    }

  def initialise(identifier: String, root: String): Route =
    singleDevice(identifier) { device =>
      val rootPath = Paths.get(root)
      val eventualCommands: Future[Seq[String]] =
        deviceService.list(device).map { encodedFiles =>
          val paths: Seq[((Path, Path), Int)] = encodedFiles
            .map(file => (file.encodedRepoPath, file.relativePath.getParent))
            .distinct
            .zipWithIndex
          val total = paths.size
          paths
            .flatMap { case ((encodedRepoPath, relativePath), index) =>
              val sourcePath =
                rootPath.resolve(encodedRepoPath).resolve(relativePath)
              Seq(
                s"""echo $index of $total: $relativePath""",
                s"""mkdir -p "$$1/$relativePath"""",
                s"""cp -rv "$sourcePath"/* "$$1/$relativePath" """
              )
            }
        }
      onSuccess(
        deviceService
          .updateLastSynchronised(identifier)
          .flatMap(_ => eventualCommands)
      ):
        val headerCommands = Seq(
          "#!/bin/bash",
          """mkdir -p "$$1"""",
          s"""echo "${device.identifier}" > "$$1/.device""""
        )
        commands =>
          complete(HttpEntity((headerCommands ++ commands).mkString("\n")))
    }

  private def noSuchDevice(identifier: String): String =
    s"$identifier is not a valid device"

  private def alterLastSynchronised(identifier: String): Route =
    onSuccessWhenPresent(
      deviceService.updateLastSynchronised(identifier),
      noSuchDevice(identifier)
    ) { device =>
      for
        lastSynchronised <- device.lastSynchronised
        lastCommit <- device.lastCommit
      do
        logger.info(
          s"Device ${device.identifier} last synchronised timestamp has been set to $lastSynchronised"
        )
        logger.info(
          s"Device ${device.identifier} last commit has been set to $lastCommit"
        )
      complete(device)
    }

  private def alterOffset(identifier: String): Route =
    entity(as[Int]) { offset =>
      onSuccessWhenPresent(
        deviceService.updateOffset(identifier, offset),
        noSuchDevice(identifier)
      ) { device =>
        {
          logger.info(
            s"Device ${device.identifier} last synchronised offset has been set to $offset"
          )
          complete(device)
        }
      }
    }
