package controllers

import org.apache.pekko.http.scaladsl.model.HttpMethod
import org.apache.pekko.http.scaladsl.model.RequestEntityAcceptance.Expected

trait CustomMethods:

  val PROPFIND: HttpMethod = HttpMethod.custom(
    "PROPFIND",
    safe = true,
    idempotent = true,
    requestEntityAcceptance = Expected
  )

object CustomMethods extends CustomMethods
