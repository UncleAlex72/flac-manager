package controllers

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.http.scaladsl.server.{Route, StandardRoute}
import org.apache.pekko.stream.scaladsl.Sink
import com.typesafe.scalalogging.StrictLogging
import delta.{Delta, DeltaService, DeltaSummary}
import devices.{Device, DeviceDao}
import io.circe.Encoder
import routes.PathSegment.{DELTAS, FULL_SEGMENT, SUMMARIES_SEGMENT}
import uk.co.unclealex.futures.Futures._

import scala.collection.immutable.Seq
import scala.concurrent.{ExecutionContext, Future}

class DeltasController(
    val deltaService: DeltaService,
    val deviceDao: DeviceDao
)(using
    ec: ExecutionContext,
    actorSystem: ActorSystem
) extends Controller
    with StrictLogging:

  override def route: Route =
    pathPrefix(DELTAS):
      concat(
        path(FULL_SEGMENT / Segment) { identifier =>
          get {
            full(identifier)
          }
        },
        path(SUMMARIES_SEGMENT / Segment) { identifier =>
          get {
            summary(identifier)
          }
        }
      )

  private def full(identifier: String): Route = extractMusicUriFunction:
    deltasRoute[Delta](identifier, deltaService.deltas, _.decorateUrl)
  private def summary(identifier: String): Route = extractCoverArtUriFunction:
    deltasRoute[DeltaSummary](
      identifier,
      deltaService.summary,
      _.decorateCoverArtUrl
    )

  private def deltasRoute[T](
      identifier: String,
      factory: Device => Future[Seq[T]],
      linkDecorator: T => (String => String) => T
  )(uriFactory: String => String)(implicit writes: Encoder[T]): Route =
    val emResult = for
      device <- fo(
        deviceDao.findByIdentifier(identifier).runWith(Sink.headOption)
      )
      deltas <- fo(factory(device))
      decoratedDeltas <- fo(deltas.map(linkDecorator(_)(uriFactory)))
    yield decoratedDeltas
    onSuccessWhenPresent(emResult, s"$identifier is not a valid device"):
      result => complete(result)
