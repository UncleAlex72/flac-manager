package controllers

import org.apache.pekko.http.scaladsl.marshallers.xml.ScalaXmlSupport
import org.apache.pekko.http.scaladsl.model.HttpMethods.{GET, OPTIONS}
import org.apache.pekko.http.scaladsl.model.{
  ContentTypes,
  HttpEntity,
  HttpResponse,
  ResponseEntity,
  StatusCodes
}
import org.apache.pekko.http.scaladsl.model.headers.Allow
import org.apache.pekko.http.scaladsl.server.Route
import com.typesafe.scalalogging.StrictLogging
import routes.PathSegment.DAV
import uk.co.unclealex.futures.Futures._
import webdav.{
  DirectoryWebDavResource,
  FileWebDavResource,
  WebDavResourceBuilder,
  WebDavResourceContainer
}

import java.text.SimpleDateFormat
import scala.collection.immutable.Seq
import scala.concurrent.ExecutionContext
import scala.xml.{Elem, Node, NodeSeq, PrettyPrinter}

class WebDavController(
    val resourceBuilder: WebDavResourceBuilder
)(implicit val ec: ExecutionContext)
    extends Controller
    with StrictLogging
    with ScalaXmlSupport
    with DavHeaders:

  private val prettyPrinter: PrettyPrinter = new scala.xml.PrettyPrinter(80, 4)

  override def route: Route =
    pathPrefix(DAV):
      redirectToNoTrailingSlashIfPresent(StatusCodes.MovedPermanently):
        concat(
          pathEnd {
            serveWebDavResource(None)
          },
          path(Segments) { segments =>
            serveWebDavResource(Some(pathOf(segments)))
          }
        )

  private def serveWebDavResource(
      maybePath: Option[String]
  ): Route =
    extractDavUriFunction { uriBuilder =>
      val maybeEventualWebResourceContainer
          : FutureOption[WebDavResourceContainer] = maybePath match
        case Some(path) => resourceBuilder.buildTreeResource(path, uriBuilder)
        case None =>
          resourceBuilder.buildDevicesResource(uriBuilder(""), uriBuilder)
      onSuccessWhenPresent(
        maybeEventualWebResourceContainer,
        "No such resource"
      ) { resource =>
        concat(
          options {
            respondWithHeaders(DavHeader(true), Allow(GET, OPTIONS, PROPFIND)) {
              complete(StatusCodes.OK)
            }
          },
          method(PROPFIND) {
            serveProperties(resource)
          },
          get {
            serveResource(resource)
          }
        )
      }
    }

  private def serveResource(resource: WebDavResourceContainer): Route =
    resource.webDavResource match
      case FileWebDavResource(localResourcePath) =>
        getFromFile(localResourcePath.toFile)
      case DirectoryWebDavResource(childrenFactory) =>
        serveDirectoryAsHtml(childrenFactory())
      /*
    val maybeLocalResourcePath = resource.webDavResource match {
      case FileWebDavResource(localResourcePath) =>
        Some(localResourcePath.toFile)
      case _ => None
    }
    whenPresent(
      maybeLocalResourcePath,
      "There is no resource at this location"
    ) { localResourcePath =>
      getFromFile(localResourcePath)
    }
       */

  private def serveDirectoryAsHtml(
      webdavResourceContainers: Seq[WebDavResourceContainer]
  ): Route =
    val links: String = webdavResourceContainers
      .map { webDavResourceContainer =>
        val url = webDavResourceContainer.url
        val name = url.substring(url.lastIndexOf('/') + 1).replace("%20", " ")
        s"""<p><a href="$url">$name</a></p>"""
      }
      .mkString("\n")
    val response =
      s"""
         |<html>
         |<head/>
         |<body>
         |$links
         |</body>
         |</html>""".stripMargin
    complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, response))

  private def serveProperties(resource: WebDavResourceContainer): Route =
    optionalHeaderValueByType(DepthHeader) { maybeDepthHeader =>
      val depth = maybeDepthHeader.map(_.value).getOrElse("0")
      entity(as[NodeSeq]) { xml =>
        extractUri { uri =>
          val response = propfind(xml \ "prop" \ "_", resource, depth)
          logger.debug(
            s"Returning body for PROPFIND $uri\n${prettyPrinter.format(response)}"
          )
          complete(StatusCodes.MultiStatus, response)
        }
      }
    }

  private def property(
      resource: WebDavResourceContainer,
      prop: Node
  ): Option[Node] =
    def wrapNode(value: Node): Option[Node] =
      prop match
        case Elem(p, l, at, sc) => Some(Elem(p, l, at, sc, false, value))

    def wrap(value: String): Option[Node] =
      wrapNode(scala.xml.Text(value))

    val formatter: SimpleDateFormat =
      val df = new java.text.SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss z")
      df.setTimeZone(java.util.TimeZone.getTimeZone("GMT"))
      df

    def httpDate(time: Long): String =
      formatter.format(new java.util.Date(time))

    prop match
      case <getlastmodified/>  => wrap(httpDate(resource.lastModified))
      case <getcontentlength/> => wrap(resource.size.toString)
      case <resourcetype/> =>
        resource.webDavResource match
          case _: DirectoryWebDavResource => wrapNode(<D:collection/>)
          case _                          => Some(prop)
      case _ => None

  private def propfind(
      props: NodeSeq,
      res: WebDavResourceContainer,
      depth: String
  ): Elem =
    val resources: Seq[WebDavResourceContainer] = depth match
      case "0" => res :: Nil
      case "1" => res.webDavResource.children() ++ (res :: Nil)

    <D:multistatus xmlns:D="DAV:">
      {
      resources.map(res => {
        val mapped: Seq[(Node, Option[Node])] =
          props.map(p => (p, property(res, p)))
        <D:response>
        <D:href>
          {res.url}
        </D:href>
        <D:propstat xmlns:D="DAV:">
          <D:prop>
            {
          mapped.flatMap(_ match {
            case (_, Some(p)) => p :: Nil;
            case (_, None)    => Nil
          })
        }
          </D:prop>
          <D:status>HTTP/1.1 200 OK</D:status>
        </D:propstat>
        <D:propstat xmlns:D="DAV:">
          <D:prop>
            {
          mapped.flatMap(_ match {
            case (_, Some(_)) => Nil;
            case (p, None)    => p
          })
        }
          </D:prop>
          <D:status>HTTP/1.1 404 Not Found</D:status>
        </D:propstat>
      </D:response>
      })
    }
    </D:multistatus>
