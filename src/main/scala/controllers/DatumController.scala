package controllers

import org.apache.pekko.http.scaladsl.server.{Route, StandardRoute}
import configuration.FlacDirectories
import io.circe.syntax._
import routes.PathSegment.DATUM

/** Resolve avatar URLs.
  * @param controllerComponents
  * @param ec
  */
class DatumController(
    val flacDirectories: FlacDirectories
) extends Controller:

  override def route: Route = path(DATUM):
    get:
      complete:
        flacDirectories.datumPath.getFileName.toString.asJson
