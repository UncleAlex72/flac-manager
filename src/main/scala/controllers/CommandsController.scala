package controllers

import org.apache.pekko.http.scaladsl.common.{
  EntityStreamingSupport,
  JsonEntityStreamingSupport
}
import org.apache.pekko.http.scaladsl.server.Route
import org.apache.pekko.stream.Materializer
import commands.Command
import processor.CommandProcessor
import versioning.Repository
import routes.PathSegment.COMMANDS

import java.nio.file.FileSystem
import scala.concurrent.ExecutionContext
class CommandsController(
    val commandProcessor: CommandProcessor,
    val repository: Repository
)(using
    ec: ExecutionContext,
    fs: FileSystem,
    materializer: Materializer
) extends Controller:

  given jsonStreamingSupport: JsonEntityStreamingSupport =
    EntityStreamingSupport.json()

  override def route: Route =
    path(COMMANDS):
      post:
        entity(as[Command]) { command =>
          complete:
            commandProcessor.submit(command)
        }
