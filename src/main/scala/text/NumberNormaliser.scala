package text

/** Normalise a number so it is zero padded.
  */
trait NumberNormaliser:

  def normalise(totalNumberOfTracks: Int, track: Int): String
