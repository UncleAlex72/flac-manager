package text

class NumberNormaliserImpl extends NumberNormaliser:
  override def normalise(totalNumberOfTracks: Int, track: Int): String =
    val trackNumberLength =
      Math.max(2, Math.log10(totalNumberOfTracks).toInt + 1)
    val trackNumberString = track.toString
    LazyList
      .continually('0')
      .take(Math.max(0, trackNumberLength - trackNumberString.length))
      .mkString + trackNumberString
