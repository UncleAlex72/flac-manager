package text

trait Normaliser:

  def normalise(charSequence: CharSequence): String

object Normaliser:

  implicit class NormaliserImplicits(s: String):

    def normalise(implicit normaliser: Normaliser): String =
      normaliser.normalise(s)
