package text

import java.text.Normalizer

class RegularExpressionNormaliser extends Normaliser:

  def normalise(str: CharSequence): String =
    val nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD)
    val nonAccentedString = """\p{InCombiningDiacriticalMarks}+""".r
      .replaceAllIn(nfdNormalizedString, "")
    val validCharacters =
      """(?:[a-zA-Z0-9]|\s)+""".r.findAllIn(nonAccentedString).mkString
    """\s+""".r.replaceAllIn(validCharacters, " ").trim
