package processor
import java.io.IOException
import java.nio.file.Files

import files.{StagingFile, TempFile}

class FfmpegLossyEncoder(
    ffmpegEncoder: FfmpegEncoder,
    library: String,
    encoderOptions: (String, String)*
) extends LossyEncoder:
  override final def encode(
      stagingFile: StagingFile,
      targetFile: TempFile
  ): Unit =
    val sourcePath = stagingFile.absolutePath
    val targetPath = targetFile.absolutePath
    if !Files.exists(sourcePath) then
      throw new IOException(s"Source path $sourcePath does not exist.")
    if Files.isDirectory(sourcePath) then
      throw new IOException(s"Source path $sourcePath is a directory.")
    if !Files.isReadable(sourcePath) then
      throw new IOException(s"Source path $sourcePath is not readable.")
    val targetParent = targetPath.getParent
    if !Files.isDirectory(targetParent) then
      throw new IOException(s"Target path $targetParent is not a directory.")
    if !Files.isWritable(targetParent) then
      throw new IOException(s"Target path $targetParent is not writable.")
    ffmpegEncoder.encode(library, encoderOptions.toMap, sourcePath, targetPath)
    targetFile.writeTags()
