package processor

import files.{FlacFile, HashFile}

trait HashEncoder:

  def encode(flacFile: FlacFile, hashFile: HashFile): Unit

  def remove(hashFile: HashFile): Unit
