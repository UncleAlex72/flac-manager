package processor
import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.{
  BroadcastHub,
  Flow,
  Keep,
  Source,
  SourceQueueWithComplete
}
import org.apache.pekko.stream.{
  KillSwitches,
  Materializer,
  OverflowStrategy,
  QueueOfferResult,
  UniqueKillSwitch
}
import cats.data.Validated.{Invalid, Valid}
import com.typesafe.scalalogging.StrictLogging
import commands.{Command, CommandResponse, KeepAliveResponse, MessageResponse}
import logging.Messages
import processor.AkkaDebugLogging._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.DurationInt

class CommandProcessorImpl(
    val commandParser: CommandParser,
    val actionExecutor: ActionExecutor,
    val jobIdGenerator: JobIdGenerator
)(implicit materializer: Materializer, ec: ExecutionContext)
    extends CommandProcessor
    with Shutdown
    with StrictLogging:

  private type JobFlow[In, Out] = Flow[(String, In), (String, Out), NotUsed]

  private val executor: Flow[Action, String, NotUsed] =
    Flow[Action].flatMapConcat { action =>
      actionExecutor(action).recoverWithRetries(
        1,
        { case ex =>
          Source.single(Messages.exception(ex))
        }
      )
    }

  val parser: Flow[Command, Either[String, Action], NotUsed] =
    Flow[Command].flatMapMerge(
      1,
      { command =>
        Source.futureSource {
          commandParser(command).map {
            case Valid(actions) =>
              Source(actions).debug("action").map(Right(_))
            case Invalid(messages) =>
              val allMessages = messages.toList
              Source(allMessages).debug("message").map(Left(_))
          }
        }
      }
    )

  private def jobIdDecorator[In, Out](
      flow: Flow[In, Out, NotUsed]
  ): JobFlow[In, Out] =
    Flow[(String, In)].flatMapConcat { case (jobId, in) =>
      Source.repeat(jobId).zip(Source.single(in).via(flow))
    }

  private val parserDependentExecutor: JobFlow[Command, String] =
    jobIdDecorator:
      parser
        .flatMapConcat:
          case Left(message) => Source.single(message)
          case Right(action) => Source.single(action).via(executor)
        .concat(Source.single(Messages.done()))

  private val (
    (
      queue: SourceQueueWithComplete[(String, Command)],
      killSwitch: UniqueKillSwitch
    ),
    messageSource: Source[(String, String), NotUsed]
  ) = Source
    .queue(1024, OverflowStrategy.backpressure)
    .via(parserDependentExecutor)
    .viaMat(KillSwitches.single)(Keep.both)
    .toMat(BroadcastHub.sink)(Keep.both)
    .run()

  override def all(): Source[(String, String), NotUsed] =
    messageSource.map { case (jobId, message) =>
      jobId -> message
    }

  def logAll(): CommandProcessor & Shutdown =
    all().runForeach { case (id, message) =>
      logger.info(s"$id: $message")
    }
    this

  override def submit(command: Command): Source[CommandResponse, NotUsed] =
    val jobId = jobIdGenerator.generate()
    logger.info(s"Job $jobId is being queued.")
    Source
      .futureSource:
        queue
          .offer(jobId -> command)
          .map:
            case QueueOfferResult.Enqueued =>
              messageSource
                .filter(_._1 == jobId)
                .map { case (_, message) =>
                  MessageResponse(message)
                }
                .keepAlive(maxIdle = 1.seconds, () => KeepAliveResponse())
                .takeWhile(
                  {
                    case MessageResponse(message)
                        if message == Messages.done() =>
                      false
                    case _ => true
                  },
                  inclusive = true
                )
            case QueueOfferResult.Dropped =>
              throw new IllegalStateException(
                s"The command for job id $jobId was dropped."
              )
            case QueueOfferResult.QueueClosed =>
              throw new IllegalStateException(
                s"The command for job id $jobId was submitted after the queue was closed."
              )
            case QueueOfferResult.Failure(ex) =>
              throw new IllegalStateException(
                s"The command for job id $jobId could not be submitted.",
                ex
              )
      .mapMaterializedValue(_ => NotUsed)

  def shutdown(): Unit =
    queue.complete()
    killSwitch.shutdown()
