package processor

import files.{StagingFile, TempFile}

trait LossyEncoder:

  def encode(stagingFile: StagingFile, targetFile: TempFile): Unit
