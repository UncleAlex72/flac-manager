package processor

import cats.Monad
import cats.data.Validated.{Invalid, Valid}
import cats.data.{NonEmptyList, Validated, ValidatedNel}
import commands.{CheckinCommand, Command}
import devices.{Extension, LossyExtension}
import files.{FlacFile, StagingFile}
import logging.Messages
import music.{FixDateService, Tags}
import users.OwnerService
import validation._

import scala.collection.immutable.Seq
import scala.concurrent.{ExecutionContext, Future}

/** Given a list of [[files.StagingFile]]s, generate a list of [[Action]]s to
  * check them in if they obey the following rules:
  *
  *   - Non-flac files are deleted.
  *   - At least one file must be a flac file.
  *   - All flac files must be fully tagged with at least an album, artist,
  *     track number, title, cover art and
  *     [[http://www.musicbrainz.org MusicBrainz]] IDs.
  *   - No flac file can overwrite an existing flac file.
  *   - No two staged flac files can resolve to the same file in the flac
  *     repository.
  *   - All flac files must have at least one owner.
  */
class CheckinCommandParser(
    val allowMulti: Boolean,
    val ownerService: OwnerService,
    val parametersValidator: ParametersValidator,
    val fixDateService: FixDateService
)(implicit ec: ExecutionContext)
    extends PartialCommandParser:

  override def apply()
      : PartialFunction[Command, Future[ValidatedNel[String, Seq[Action]]]] =
    case command: CheckinCommand =>
      parametersValidator.validateStagingDirectories(
        command.relativeDirectories
      ) match
        case Valid(stagedFlacFiles) =>
          val eventualValidatedActions
              : Future[ValidatedNel[String, Seq[Action]]] =
            generate(
              command.allowUnowned,
              command.owners,
              stagedFlacFiles.flatMap(_.list).toList
            )
          eventualValidatedActions.map { validatedActions =>
            validatedActions.map { actions =>
              actions ++ Seq(CommitAction, ClearCachesAction)
            }
          }
        case Invalid(messages) => Future.successful(Invalid(messages))

  /** @inheritdoc
    */
  def generate(
      allowUnowned: Boolean,
      ownerNames: Seq[String],
      stagedFlacFiles: Seq[StagingFile]
  ): Future[ValidatedNel[String, Seq[Action]]] =
    for validUsers <- validateUsers(ownerNames)
    yield validUsers.andThen { users =>
      val (flacFiles, nonFlacFiles) =
        partitionFlacAndNonFlacFiles(stagedFlacFiles)
      // Validate the flac files only as non flac files just get deleted.
      validate(
        flacFiles,
        allowUnowned,
        users
      ).map(_.flatMap(_.toActions)).map { actions =>
        actions ++ nonFlacFiles.map(DeleteFileAction)
      }
    }

  def validateUsers(
      ownerNames: Seq[String]
  ): Future[ValidatedNel[String, Seq[String]]] =
    ownerService.owners().map { users =>
      Validations.validateSequence(ownerNames) { ownerName =>
        if users.contains(ownerName) then ownerName.validNel
        else Messages.invalidOwner(ownerName).invalidNel
      }
    }

  /** Validate a sequence of staged flac files.
    *
    * @param files
    *   The staged flac files to check.
    * @param allowUnowned
    *   True if unowned files are allowed to be checked in, false otherwise.
    * @param ownerCalculator
    *   A list of users for each album.
    * @return
    *   A [[ValidatedNel]] that contains either a sequence of
    *   [[ValidFlacFileWithNewOwners]]s or a non-empty list of [[Messages]]s to
    *   log in the case of failure.
    */
  def validate(
      files: Seq[StagingFile],
      allowUnowned: Boolean,
      owners: Seq[String]
  ): ValidatedNel[String, Seq[ValidFlacFileWithNewOwners]] =
    val validatedValidFlacFiles
        : ValidatedNel[String, Seq[ValidFlacFileWithNewOwners]] =
      checkThereAreSomeFiles(files).andThen { stagingFiles =>
        checkFullyTaggedFlacFiles(stagingFiles, owners)
      }
    validatedValidFlacFiles.andThen { validFlacFiles =>
      (
        checkDoesNotOverwriteExistingFlacFile(validFlacFiles),
        checkTargetFlacFilesAreUnique(validFlacFiles),
        checkForMultiDisc(validFlacFiles),
        checkFlacFilesAreOwned(
          validFlacFiles,
          allowUnowned,
          owners
        )
      ).mapN((_, _, _, ownedFlacFilesAndNewOwners) =>
        ownedFlacFilesAndNewOwners
      )
    }

  /** Partition a set of [[StagingFile]]s into those that start with a flac
    * magic number and those that don't.
    * @param files
    *   The file locations to partition.
    * @return
    *   A [[Tuple2]] that contains a sequence of flac files and a sequence of
    *   non-flac files.
    */
  def partitionFlacAndNonFlacFiles(
      files: Seq[StagingFile]
  ): (Seq[StagingFile], Seq[StagingFile]) =
    files.partition(_.isFlacFile)

  /** Make sure that there is at least one flac file.
    *
    * @param files
    *   The file locations to check.
    * @return
    *   The file locations if it is not empty or [[Messages.noFiles()]]
    *   otherwise.
    */
  def checkThereAreSomeFiles(
      files: Seq[StagingFile]
  ): ValidatedNel[String, Seq[StagingFile]] =
    if files.isEmpty then Messages.noFiles(files.toSet).invalidNel
    else files.validNel

  /** Make sure that all flac files are fully tagged.
    *
    * @param files
    *   The file locations to check.
    * @return
    *   A [[ValidFlacFileWithNewOwners]] for each fully tagged flac file and
    *   [[Messages.invalidFlac()]] for each non-fully tagged flac file.
    */
  def checkFullyTaggedFlacFiles(
      files: Seq[StagingFile],
      owners: Seq[String]
  ): ValidatedNel[String, Seq[ValidFlacFileWithNewOwners]] =
    Validations.validateSequence(files) { stagingFile =>
      val stagingFileWithCorrectDate = fixDateService.fixDates(stagingFile)
      stagingFileWithCorrectDate.toFlacFileAndTags.map:
        case (flacFile, tags) =>
          ValidFlacFileWithNewOwners(stagingFile, flacFile, tags, owners)
    }

  /** Make sure that no flac file overwrites a file that already exists in the
    * flac repository.
    *
    * @param validFlacFiles
    *   The fully tagged flac files to check.
    * @return
    *   The [[ValidFlacFileWithNewOwners]]s that do not overwrite an existing
    *   flac file or [[Messages.overwrite()]] for each one that does.
    */
  def checkDoesNotOverwriteExistingFlacFile(
      validFlacFiles: Seq[ValidFlacFileWithNewOwners]
  ): ValidatedNel[String, Seq[ValidFlacFileWithNewOwners]] =
    implicit val ignoreCaseOrdering: Ordering[String] =
      (x: String, y: String) => x.toLowerCase.compareTo(y.toLowerCase)
    Validations.validateSequence(validFlacFiles) { validFlacFile =>
      val fileExists = validFlacFile.flacFile.exists
      if fileExists then
        Messages
          .overwrite(validFlacFile.stagedFile, validFlacFile.flacFile)
          .invalidNel
      else Validated.valid(validFlacFile)
    }

  /** Make sure that no two [[ValidFlacFileWithNewOwners]]s resolve to the same
    * file in the flac repository.
    *
    * @param validFlacFiles
    *   The set of valid flac files to check.
    * @return
    *   The sequence of valid flac files that are unique or
    *   [[Messages.nonUnique()]] for each one that isn't.
    */
  def checkTargetFlacFilesAreUnique(
      validFlacFiles: Seq[ValidFlacFileWithNewOwners]
  ): ValidatedNel[String, Seq[ValidFlacFileWithNewOwners]] =
    val (uniqueMappings, nonUniqueMappings) =
      validFlacFiles.groupBy(_.flacFile).partition(kv => kv._2.size == 1)
    val uniqueFlacFiles: ValidatedNel[String, Seq[ValidFlacFileWithNewOwners]] =
      uniqueMappings.values.flatten.toList.validNel
    val nonUniqueFlacFiles: ValidatedNel[String, Seq[Nothing]] =
      Validations.validateSequence(nonUniqueMappings.toList):
        case (flacFile, nonUniqueValidFlacFiles) =>
          Messages
            .nonUnique(flacFile, nonUniqueValidFlacFiles.map(_.stagedFile))
            .invalidNel
    (uniqueFlacFiles, nonUniqueFlacFiles).mapN((uffs, _) => uffs)

  /** Make sure that if albums with multiple discs are not allowed then no files
    * have a disc number greater than 1.
    *
    * @param validFlacFiles
    *   The set of valid flac files to check.
    * @return
    *   The sequence of valid flac files that non-multi disc or
    *   [[Messages.multiDisc()]] for each one that isn't.
    */
  def checkForMultiDisc(
      validFlacFiles: Seq[ValidFlacFileWithNewOwners]
  ): ValidatedNel[String, Seq[ValidFlacFileWithNewOwners]] =
    if allowMulti then validFlacFiles.validNel
    else
      val secondDiscFlacFiles =
        validFlacFiles.filter(_.tags.discNumber > 1).toList
      NonEmptyList.fromList(secondDiscFlacFiles) match
        case Some(invalidFlacFiles) =>
          Monad[NonEmptyList]
            .map(invalidFlacFiles)(f => Messages.multiDisc(f.stagedFile))
            .invalid
        case _ => validFlacFiles.validNel

  /** Make sure that each flac file has at least one owner if unowned flac files
    * are not allowed.
    *
    * @param validFlacFiles
    *   The [[ValidFlacFileWithNewOwners]]s to check.
    * @return
    *   A sequence of [[ValidFlacFileWithNewOwners]]s or [[Messages.notOwned()]]
    *   for those that have no owner.
    */
  def checkFlacFilesAreOwned(
      validFlacFiles: Seq[ValidFlacFileWithNewOwners],
      allowUnowned: Boolean,
      owners: Seq[String]
  ): ValidatedNel[String, Seq[ValidFlacFileWithNewOwners]] =
    Validations.validateSequence(validFlacFiles) { validFlacFile =>
      if !allowUnowned && owners.isEmpty then
        Messages.notOwned(validFlacFile.stagedFile).invalidNel
      else validFlacFile.validNel
    }

  /** A holder for all the information about a valid flac file.
    * @param stagedFile
    *   The location of the flac file in the staging repository.
    * @param flacFile
    *   The location of where the flac file will be in the flac repository.
    * @param tags
    *   The audio information stored in the flac file.
    */
  case class ValidFlacFileWithNewOwners(
      stagedFile: StagingFile,
      flacFile: FlacFile,
      tags: Tags,
      owners: Seq[String]
  ):

    /** Convert this flac file into an [[CheckinFileAction]] action.
      * @return
      *   An [[CheckinFileAction]] action that can be used to encode this flac
      *   file.
      */
    def toActions: Seq[Action] =
      val checkinAction = CheckinFileAction(stagedFile, flacFile, tags)
      val createHashFileActions = owners.distinct.sorted.map { owner =>
        CreateHashFileAction(flacFile, flacFile.toHashFile(owner))
      }
      checkinAction +: createHashFileActions
