package processor

import cats.data.ValidatedNel
import commands.{MultiAction, PathAndRepository}
import devices.LossyExtension
import files.Directory.{FlacDirectory, StagingDirectory}
import validation.Validations

import scala.collection.SortedSet
import scala.collection.immutable.Seq
import scala.concurrent.Future

trait ParametersValidator:

  /** Make sure that all usernames are valid and at least one username was
    * supplied.
    * @param usernames
    *   A list of usernames to check.
    * @return
    *   The users with the usernames or a list of errors.
    */
  def validateUsers(
      usernames: Seq[String]
  ): Future[ValidatedNel[String, SortedSet[String]]]

  /** Make sure that a username is valid and at least one username was supplied.
    * @param owner
    *   A the owner to check.
    * @return
    *   The user with the username or a list of errors.
    */
  def validateOwner(owner: String): Future[ValidatedNel[String, String]]

  /** Make sure that an extension corresponds to a known lossy encoder.
    * @param extension
    *   The extension to check.
    * @return
    *   The [[LossyExtension]] or a list of errors.
    */
  def validateLossyExtension(
      extension: String
  ): ValidatedNel[String, LossyExtension]

  def validateMaximumNumberOfThreads(
      maybeMaximumNumberOfThreads: Option[Int]
  ): ValidatedNel[String, Option[Int]]

  /** Make sure that all staging directories are valid.
    * @param pathAndRepositories
    *   A list of relative paths to check.
    * @return
    *   A list of [[files.Directory.StagingDirectory]]s or a list of errors.
    */
  def validateStagingDirectories(
      pathAndRepositories: Seq[PathAndRepository]
  ): ValidatedNel[String, Seq[StagingDirectory]]

  /** Require a multi action to be provided.
    * @param maybeMultiAction
    *   The multi action that may have been provided.
    * @return
    *   The provided multi action or an error.
    */
  def requireMultiAction(
      maybeMultiAction: Option[MultiAction]
  ): ValidatedNel[String, MultiAction]

  /** Make sure that all flac directories are valid.
    * @param pathAndRepositories
    *   A list of relative paths to check.
    * @return
    *   A list of [[files.Directory.FlacDirectory]]s or a list of errors.
    */
  def validateFlacDirectories(
      pathAndRepositories: Seq[PathAndRepository]
  ): ValidatedNel[String, Seq[FlacDirectory]]
