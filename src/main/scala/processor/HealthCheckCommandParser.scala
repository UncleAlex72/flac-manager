package processor

import cats.data.ValidatedNel
import commands.{Command, HealthCheckCommand}
import files.{FlacFile, StagingFile}
import validation._

import scala.collection.immutable.Seq
import scala.concurrent.Future

/** Run a health check, making sure that there are no dangling nor missing
  * files.
  */
class HealthCheckCommandParser() extends PartialCommandParser:

  override def apply()
      : PartialFunction[Command, Future[ValidatedNel[String, Seq[Action]]]] =
    case HealthCheckCommand() =>
      Future.successful(Seq(HealthCheckAction).validNel)
