package processor

class Mp3LossyEncoder(ffmpegEncoder: FfmpegEncoder)
    extends FfmpegLossyEncoder(ffmpegEncoder, "libmp3lame")
