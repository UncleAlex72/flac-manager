package processor

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source
import logging.Messages
import versioning.Repository

import java.time.{Clock, ZonedDateTime}
import java.time.format.DateTimeFormatter
import scala.concurrent.ExecutionContext

class CommitActionExecutor(val repository: Repository, val clock: Clock)(
    implicit ec: ExecutionContext
) extends PartialActionExecutor:
  override def apply(): PartialFunction[Action, Source[String, NotUsed]] =
    case CommitAction =>
      Source
        .lazySource { () =>
          val message = s"Committing at ${DateTimeFormatter.RFC_1123_DATE_TIME
              .format(ZonedDateTime.now(clock))}"
          repository.commit(message)
          Source.single(message)
        }
        .mapMaterializedValue(_ => NotUsed)
