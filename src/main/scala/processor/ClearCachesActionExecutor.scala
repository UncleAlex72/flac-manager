package processor

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source
import delta.DeltaCacheService
import logging.Messages

import scala.concurrent.ExecutionContext

class ClearCachesActionExecutor(val deltaCacheService: DeltaCacheService)(
    implicit ec: ExecutionContext
) extends PartialActionExecutor:
  override def apply(): PartialFunction[Action, Source[String, NotUsed]] =
    case ClearCachesAction =>
      Source
        .futureSource:
          deltaCacheService
            .clear()
            .map(_ => Source.single(Messages.cachesCleared()))
        .mapMaterializedValue(_ => NotUsed)
