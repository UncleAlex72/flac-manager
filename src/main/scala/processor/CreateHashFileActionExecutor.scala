package processor

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source
import logging.Messages
import versioning.Repository

import scala.concurrent.ExecutionContext

class CreateHashFileActionExecutor(
    val hashEncoder: HashEncoder
)(implicit
    ec: ExecutionContext
) extends PartialActionExecutor:
  override def apply(): PartialFunction[Action, Source[String, NotUsed]] =
    case CreateHashFileAction(source, target) =>
      Source
        .lazySource { () =>
          hashEncoder.encode(source, target)
          Source.single(Messages.hashFileCreated(source, target))
        }
        .mapMaterializedValue(_ => NotUsed)
