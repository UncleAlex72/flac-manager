package processor

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source
import cats.Show
import enumeratum._
import files.StagingFile
import logging.Messages
import music.{CoverArt, Tags}

import scala.collection.immutable

class AlterTagsActionExecutor extends PartialActionExecutor:

  override def apply(): PartialFunction[Action, Source[String, NotUsed]] =
    case AlterTagsAction(source, originalTags, newTags) =>
      Source:
        source.writeTags(newTags)
        TagChangeReporter.values.flatMap(
          _.reportChange(source, originalTags, newTags)
        )

  sealed trait TagChangeReporter extends EnumEntry:
    def reportChange(
        stagingFile: StagingFile,
        originalTags: Tags,
        newTags: Tags
    ): Option[String]

  object TagChangeReporter extends Enum[TagChangeReporter]:

    implicit val stringShow: Show[String] = Show.show(identity)
    implicit val intShow: Show[Int] = Show.fromToString[Int]
    implicit def optShow[A](implicit innerShow: Show[A]): Show[Option[A]] =
      Show.show:
        case Some(value) => innerShow.show(value)
        case _           => "(Nothing)"
    implicit val coverArtShow: Show[CoverArt] = Show.show(_ => "[image]")

    private[AlterTagsActionExecutor] class AbstractTagChangeReporter[A](
        tagName: String,
        extractor: Tags => A
    )(implicit show: Show[A])
        extends TagChangeReporter:
      override def reportChange(
          stagingFile: StagingFile,
          originalTags: Tags,
          newTags: Tags
      ): Option[String] =
        val originalValue = extractor(originalTags)
        val newValue = extractor(newTags)
        if originalValue == newValue then None
        else
          Some(
            Messages.alterTags(
              stagingFile,
              tagName,
              show.show(originalValue),
              show.show(newValue)
            )
          )

    object AlbumArtistSortReporter
        extends AbstractTagChangeReporter[String](
          "albumArtistSort",
          _.albumArtistSort
        )
    object AlbumArtistReporter
        extends AbstractTagChangeReporter[String]("albumArtist", _.albumArtist)
    object AlbumReporter
        extends AbstractTagChangeReporter[String]("album", _.album)
    object ArtistReporter
        extends AbstractTagChangeReporter[String]("artist", _.artist)
    object ArtistSortReporter
        extends AbstractTagChangeReporter[String]("artistSort", _.artistSort)
    object TitleReporter
        extends AbstractTagChangeReporter[String]("title", _.title)
    object TotalDiscsReporter
        extends AbstractTagChangeReporter[Int]("totalDiscs", _.totalDiscs)
    object TotalTracksReporter
        extends AbstractTagChangeReporter[Int]("totalTracks", _.totalTracks)
    object DiscNumberReporter
        extends AbstractTagChangeReporter[Int]("discNumber", _.discNumber)
    object AlbumArtistIdReporter
        extends AbstractTagChangeReporter[String](
          "albumArtistId",
          _.albumArtistId
        )
    object AlbumIdReporter
        extends AbstractTagChangeReporter[String]("albumId", _.albumId)
    object ArtistIdReporter
        extends AbstractTagChangeReporter[String]("artistId", _.artistId)
    object TrackIdReporter
        extends AbstractTagChangeReporter[Option[String]]("trackId", _.trackId)
    object AsinReporter
        extends AbstractTagChangeReporter[Option[String]]("asin", _.asin)
    object CoverArtReporter
        extends AbstractTagChangeReporter[CoverArt]("coverArt", _.coverArt)

    override def values: immutable.IndexedSeq[TagChangeReporter] = findValues
