package processor

import cats.data.ValidatedNel
import commands.Command
import validation.Validations

import scala.collection.immutable.Seq
import scala.concurrent.Future

trait CommandParser:

  def apply(command: Command): Future[ValidatedNel[String, Seq[Action]]]
