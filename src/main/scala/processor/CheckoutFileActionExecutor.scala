package processor

import java.time.Clock

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.{Flow, Source}
import devices.LossyExtension
import files._
import logging.Messages

import scala.concurrent.ExecutionContext

class CheckoutFileActionExecutor(
    val clock: Clock,
    val encoders: Map[LossyExtension, LossyEncoder],
    val fs: FileSystem
)(implicit ec: ExecutionContext)
    extends PartialActionExecutor:

  val extensions: Source[LossyExtension, NotUsed] = Source(encoders.keys.toList)

  val moveFlow: Flow[(FlacFile, StagingFile), String, NotUsed] =
    Flow[(FlacFile, StagingFile)].map { case (flacFile, stagingFile) =>
      fs.move(flacFile, stagingFile)
      Messages.move(flacFile, stagingFile)
    }

  val deleteEncodedFileFlow: Flow[EncodedFile, String, NotUsed] =
    Flow[EncodedFile].map { encodedFile =>
      fs.remove(encodedFile)
      Messages.delete(encodedFile)
    }

  override def apply(): PartialFunction[Action, Source[String, NotUsed]] =
    case CheckoutFileAction(flacFile, stagingFile) =>
      val moveSource: Source[String, NotUsed] =
        Source.single(flacFile -> stagingFile).via(moveFlow)
      val encodedFileSources: Source[EncodedFile, NotUsed] = extensions.map:
        lossyExtension => flacFile.toEncodedFile(lossyExtension)
      val deleteEncodedFileSource: Source[String, NotUsed] =
        encodedFileSources.via(deleteEncodedFileFlow)

      Source
        .single(Messages.checkout(flacFile))
        .concat(moveSource)
        .concat(deleteEncodedFileSource)
