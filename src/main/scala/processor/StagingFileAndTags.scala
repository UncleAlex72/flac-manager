package processor

import cats.data.ValidatedNel
import files.StagingFile
import music.Tags

case class StagingFileAndTags(stagingFile: StagingFile, tags: Tags)

object StagingFileAndTags:
  def apply(
      stagingFile: StagingFile
  ): ValidatedNel[String, StagingFileAndTags] =
    stagingFile.tags.read().map(tags => StagingFileAndTags(stagingFile, tags))
