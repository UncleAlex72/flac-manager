package processor

trait Shutdown:

  def shutdown(): Unit
