/*
 * Copyright 2018 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package processor

import java.nio.file.Path
import cats.Id
import cats.data.ValidatedNel
import cats.{Foldable, Monad}
import cats.syntax.option._
import com.typesafe.scalalogging.StrictLogging
import commands.RepositoryType.{FlacRepositoryType, StagingRepositoryType}
import commands.{MultiAction, PathAndRepository, RepositoryType}
import devices.{Extension, LossyExtension}
import files.Directory.{FlacDirectory, StagingDirectory}
import files.Repositories
import logging.Messages
import users.OwnerService
import validation._

import scala.collection.SortedSet
import scala.collection.immutable.Seq
import scala.concurrent.{ExecutionContext, Future}

/** The default implementation of [[ParametersValidator]].
  */
class ParametersValidatorImpl(
    ownerService: OwnerService,
    repositories: Repositories
)(implicit val fs: java.nio.file.FileSystem, ec: ExecutionContext)
    extends ParametersValidator
    with StrictLogging:

  /** Make sure that all usernames are valid and at least one username was
    * supplied.
    * @param usernames
    *   A list of usernames to check.
    * @return
    *   The users with the usernames or a list of errors.
    */
  def validateUsers(
      usernames: Seq[String]
  ): Future[ValidatedNel[String, SortedSet[String]]] =
    ownerService.owners().map { usersByFirstName =>
      if usernames.isEmpty then Messages.noOwners().invalidNel
      else
        Validations.validateSortedSequence(usernames) { username =>
          validateUserFromKnownUsers(username, usersByFirstName)
        }
    }

  override def validateOwner(
      owner: String
  ): Future[ValidatedNel[String, String]] =
    ownerService.owners().map { owners =>
      validateUserFromKnownUsers(owner, owners)
    }

  private def validateUserFromKnownUsers(
      username: String,
      owners: Seq[String]
  ): ValidatedNel[String, String] =
    val lowerCaseUserName = username.toLowerCase
    if owners.contains(lowerCaseUserName) then lowerCaseUserName.validNel
    else Messages.invalidOwner(username).invalidNel

  def validateMaximumNumberOfThreads(
      maybeMaximumNumberOfThreads: Option[Int]
  ): ValidatedNel[String, Option[Int]] =
    maybeMaximumNumberOfThreads match
      case Some(maximumNumberOfThreads) if maximumNumberOfThreads < 1 =>
        Messages.notEnoughThreads().invalidNel
      case _ => maybeMaximumNumberOfThreads.validNel

  /** Make sure that all staging directories are valid.
    * @param pathAndRepositories
    *   A list of relative paths to check.
    * @return
    *   A list of [[files.Directory.StagingDirectory]]s or a list of errors.
    */
  def validateStagingDirectories(
      pathAndRepositories: Seq[PathAndRepository]
  ): ValidatedNel[String, Seq[StagingDirectory]] =
    validateDirectories("staging", pathAndRepositories):
      case StagingRepositoryType => repositories.staging.directory

  /** Require a multi action to be provided.
    * @param maybeMultiAction
    *   The multi action that may have been provided.
    * @return
    *   The provided multi action or an error.
    */
  def requireMultiAction(
      maybeMultiAction: Option[MultiAction]
  ): ValidatedNel[String, MultiAction] =
    maybeMultiAction.toValidNel(Messages.multiActionRequired())

  /** Make sure that all flac directories are valid.
    * @param pathAndRepositories
    *   A list of relative paths to check.
    * @return
    *   A list of [[files.Directory.FlacDirectory]]s or a list of errors.
    */
  def validateFlacDirectories(
      pathAndRepositories: Seq[PathAndRepository]
  ): ValidatedNel[String, Seq[FlacDirectory]] =
    validateDirectories("flac", pathAndRepositories):
      case FlacRepositoryType => repositories.flac.directory

  /** Make sure that all staging directories are valid. At this point all that
    * is checked is that the supplied list is not empty and each directory is of
    * a specified type.
    * @param repositoryType
    *   The directory type name that will be reported in errors.
    * @param builder
    *   A function that converts a path containing object into an [[F]]
    * @param pathAndRepositories
    *   A list of relative paths to check.
    * @tparam F
    *   The result type.
    * @return
    *   A list of results or a list of errors.
    */
  private def validateDirectories[F](
      repositoryType: String,
      pathAndRepositories: Seq[PathAndRepository]
  )(
      builder: PartialFunction[RepositoryType, Path => ValidatedNel[String, F]]
  ): ValidatedNel[String, Seq[F]] =
    if pathAndRepositories.isEmpty then
      Messages.noDirectories(repositoryType).invalidNel
    else
      Validations.validateSequence(pathAndRepositories) { pathAndRepository =>
        val validatedLocationFunction: Path => ValidatedNel[String, F] =
          builder
            .lift(pathAndRepository.repositoryType)
            .getOrElse((path: Path) =>
              Messages.notADirectory(path, repositoryType).invalidNel
            )
        validatedLocationFunction(pathAndRepository.path)
      }

  /** Make sure that an extension corresponds to a known lossy encoder.
    *
    * @param extension
    *   The extension to check.
    * @return
    *   The [[LossyExtension]] or a list of errors.
    */
  override def validateLossyExtension(
      extension: String
  ): ValidatedNel[String, LossyExtension] =
    LossyExtension
      .fromString(extension)
      .toOption
      .toValidNel(Messages.invalidExtension(extension))
