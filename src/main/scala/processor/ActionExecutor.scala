package processor

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source
import logging.Messages

trait ActionExecutor:

  def apply(action: Action): Source[String, NotUsed]
