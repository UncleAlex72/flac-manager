package processor

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source
import logging.Messages

class ProgressActionExecutor extends PartialActionExecutor:

  override def apply(): PartialFunction[Action, Source[String, NotUsed]] =
    case ProgressAction(index, total) =>
      Source.single(Messages.progress(index, total))
