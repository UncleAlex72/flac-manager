package processor

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.{Flow, Source}
import devices.LossyExtension
import files._
import health.HealthCheckService
import logging.Messages
import music.Tags

import java.time.Clock
import scala.collection.immutable.Seq
import scala.concurrent.{ExecutionContext, Future}

class HealthCheckActionExecutor(
    val healthCheckService: HealthCheckService
)(implicit ec: ExecutionContext)
    extends PartialActionExecutor:
  override def apply(): PartialFunction[Action, Source[String, NotUsed]] =
    case HealthCheckAction => healthCheckService.runHealthCheck
