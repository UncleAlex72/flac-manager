package processor

class SequentialJobIdGenerator(seed: Option[Int] = None) extends JobIdGenerator:

  private var counter: Int = seed.getOrElse(0) - 1

  override def generate(): String =
    counter = (counter + 1) % 65536
    f"$counter%04x"
