package processor

import devices.NewDevice
import files.{FlacFile, HashFile, StagingFile}
import music.Tags

sealed trait Action

case class CheckinFileAction(source: StagingFile, target: FlacFile, tags: Tags)
    extends Action
case class DeleteFileAction(source: StagingFile) extends Action

case class CheckoutFileAction(
    source: FlacFile,
    target: StagingFile
) extends Action

case class AddDeviceAction(newDevice: NewDevice) extends Action
case class RemoveDeviceAction(identifier: String) extends Action

case class AlterTagsAction(
    source: StagingFile,
    originalTags: Tags,
    newTags: Tags
) extends Action

case class ProgressAction(index: Int, total: Int) extends Action
case class CreateHashFileAction(source: FlacFile, target: HashFile)
    extends Action
case class RemoveHashFileAction(target: HashFile) extends Action

object CommitAction extends Action

object ClearCachesAction extends Action

object HealthCheckAction extends Action
