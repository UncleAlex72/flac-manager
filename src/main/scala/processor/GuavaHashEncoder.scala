package processor
import com.google.common.hash.{HashCode, HashFunction, Hashing}
import com.google.common.io.ByteSource
import com.google.common.io.Files.asByteSource
import files.{FileSystem, FlacFile, HashFile}

class GuavaHashEncoder(val fs: FileSystem) extends HashEncoder:
  override def encode(flacFile: FlacFile, hashFile: HashFile): Unit =
    val byteSource: ByteSource = asByteSource(flacFile.absolutePath.toFile)
    val hashCode: HashCode = byteSource.hash(Hashing.sha512())
    val checksum: String = hashCode.toString
    fs.writeString(hashFile, checksum)

  override def remove(hashFile: HashFile): Unit =
    fs.remove(hashFile)
