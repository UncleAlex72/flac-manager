package processor

import cats.data.ValidatedNel

import java.time.Instant
import commands.{Command, OwnCommand, PathAndRepository, UnownCommand}
import files.{FlacFile, HashFile, StagingFile}
import users.OwnerService
import validation._
import cats.syntax.option._
import logging.Messages

import scala.collection.SortedSet
import scala.collection.immutable.Seq
import scala.concurrent.{ExecutionContext, Future}

/** Given a list of [[StagingFile]]s or [[FlacFile]]s, either own or unown them.
  * Non-flac files are ignored.
  */
class OwnCommandsParser(
    val ownerService: OwnerService,
    val parametersValidator: ParametersValidator
)(implicit ec: ExecutionContext)
    extends PartialCommandParser:

  override def apply()
      : PartialFunction[Command, Future[ValidatedNel[String, Seq[Action]]]] =
    case OwnCommand(relativeDirectories, users) =>
      parse(
        relativeDirectories,
        users,
        (flacFile, owner) =>
          CreateHashFileAction(flacFile, flacFile.toHashFile(owner))
      )
    case UnownCommand(relativeDirectories, users) =>
      parse(
        relativeDirectories,
        users,
        (flacFile, owner) => RemoveHashFileAction(flacFile.toHashFile(owner))
      )

  def parse(
      relativeDirectories: Seq[PathAndRepository],
      users: Seq[String],
      actionBuilder: (FlacFile, String) => Action
  ): Future[ValidatedNel[String, Seq[Action]]] =
    parametersValidator.validateUsers(users.toList).map { validatedUsers =>
      val validatedFiles: ValidatedNel[String, Seq[FlacFile]] =
        parametersValidator
          .validateFlacDirectories(relativeDirectories)
          .andThen { flacDirectories =>
            Validations.validateAndFlattenSequence(flacDirectories):
              flacDirectory => flacDirectory.list.toList.validNel
          }
      (validatedUsers, validatedFiles).mapN { (owners, files) =>
        val hashFileActions = for
          file <- files
          owner <- owners.toSeq
        yield actionBuilder(file, owner)
        hashFileActions ++ Seq(CommitAction, ClearCachesAction)
      }
    }
