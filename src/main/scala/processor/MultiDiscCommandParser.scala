package processor
import cats.data.ValidatedNel
import commands.MultiAction.{Extras, Join, Rename}
import commands.{Command, MultiAction, MultiDiscCommand}
import files.Directory.StagingDirectory
import files.StagingFile
import music.Tags
import validation._

import scala.collection.immutable.SortedMap
import scala.concurrent.Future

/** The multi command is used for handling albums that span more than one disc.
  * Some MP3 systems do not cope with these well and it can also be the case
  * when a second disc just contains remixes and the like that the user does not
  * always want these played after the main album. This command allows
  * multi-disc albums to be split into albums with different titles and IDs or
  * to be joined into one large album.
  */
class MultiDiscCommandParser(val parametersValidator: ParametersValidator)
    extends PartialCommandParser:

  override def apply()
      : PartialFunction[Command, Future[ValidatedNel[String, Seq[Action]]]] =
    case MultiDiscCommand(relativeDirectories, maybeMultiAction) =>
      Future.successful:
        val validatedMultiAction =
          parametersValidator.requireMultiAction(maybeMultiAction)
        val validatedStagingFiles =
          parametersValidator.validateStagingDirectories(
            relativeDirectories
          ) andThen { directories =>
            read(directories.toList)
          }
        (validatedStagingFiles, validatedMultiAction).mapN(
          generateMultiDiscActions
        )

  def read(
      directories: Seq[StagingDirectory]
  ): ValidatedNel[String, Seq[StagingFileAndTags]] =
    Validations.validateAndFlattenSequence(directories) { directory =>
      Validations.validateSequence(directory.list.filter(_.isFlacFile)):
        stagingFile => StagingFileAndTags(stagingFile)
    }

  def generateMultiDiscActions(
      stagingFilesAndTags: Seq[StagingFileAndTags],
      multiAction: MultiAction
  ): Seq[Action] =
    multiAction match
      case Join   => createSingleAlbum(stagingFilesAndTags)
      case Extras => createAlbumWithExtras(stagingFilesAndTags)
      case Rename => createAlbumWithRenamedDisks(stagingFilesAndTags)

  def createSingleAlbum(
      stagingFilesAndTags: Seq[StagingFileAndTags]
  ): Seq[Action] =
    mutateAlbums(stagingFilesAndTags) { multiDiscAlbum =>
      val baseTrackNumber: Int = multiDiscAlbum.firstDisc.size + 1
      multiDiscAlbum.otherDiscs.zipWithIndex.map:
        case (stagingFileAndTags, idx) =>
          stagingFileAndTags -> ((tags: Tags) =>
            tags.copy(trackNumber = idx + baseTrackNumber)
          )
    }

  def createAlbumWithExtras(
      stagingFilesAndTags: Seq[StagingFileAndTags]
  ): Seq[Action] =
    mutateAlbums(stagingFilesAndTags) { multiDiscAlbum =>
      val firstAlbum = multiDiscAlbum.firstDisc
      val firstAlbumLength = firstAlbum.length
      val first = firstAlbum.map { stagingFileAndTags =>
        stagingFileAndTags -> ((tags: Tags) =>
          tags.copy(
            totalTracks = firstAlbumLength
          )
        )
      }
      val secondAlbum = multiDiscAlbum.otherDiscs
      val secondAlbumLength = secondAlbum.length
      val second = secondAlbum.zipWithIndex.map:
        case (stagingFileAndTags, idx) =>
          stagingFileAndTags -> ((tags: Tags) =>
            tags.copy(
              trackNumber = idx + 1,
              totalTracks = secondAlbumLength,
              album = tags.album + " (Extras)",
              albumId = tags.albumId + "_EXTRAS"
            )
          )
      first ++ second
    }

  private def createAlbumWithRenamedDisks(
      stagingFilesAndTags: Seq[StagingFileAndTags]
  ): Seq[Action] =
    mutateAlbums(stagingFilesAndTags, normaliseTrackNumbers = false):
      multiDiscAlbum =>
        multiDiscAlbum.otherDiscs.map { stagingFileAndTags =>
          stagingFileAndTags -> ((tags: Tags) => {
            val subtitle = tags.discSubtitle.getOrElse(tags.discNumber.toString)
            tags.copy(
              album = s"${tags.album} ($subtitle)",
              albumId = s"${tags.albumId}_${tags.discNumber}"
            )
          })
        }

  case class MultiDiscAlbum(
      firstDisc: Seq[StagingFileAndTags],
      otherDiscs: Seq[StagingFileAndTags]
  )

  def mutateAlbums(
      stagingFilesAndTags: Seq[StagingFileAndTags],
      normaliseTrackNumbers: Boolean = true
  )(
      mutator: MultiDiscAlbum => Seq[(StagingFileAndTags, Tags => Tags)]
  ): Seq[Action] =
    val multiDiscAlbums: Seq[MultiDiscAlbum] = findMultiDiscAlbums(
      stagingFilesAndTags
    )
    multiDiscAlbums.flatMap(mutateAlbum(mutator, normaliseTrackNumbers))

  def mutateAlbum(
      mutator: MultiDiscAlbum => Seq[(StagingFileAndTags, Tags => Tags)],
      normaliseTrackNumbers: Boolean
  )(multiDiscAlbum: MultiDiscAlbum): Seq[Action] =
    val firstPass = fixTrackAndDiscNumbers(
      multiDiscAlbum.firstDisc ++ multiDiscAlbum.otherDiscs,
      normaliseTrackNumbers
    )
    val secondPass = mutator(multiDiscAlbum)
    val changesByStagingFileAndTags
        : SortedMap[StagingFileAndTags, Tags => Tags] =
      val empty: SortedMap[StagingFileAndTags, Tags => Tags] =
        SortedMap.empty[StagingFileAndTags, Tags => Tags]
      (firstPass ++ secondPass).foldLeft(empty):
        case (acc, (stagingFileAndTags, tagMutator)) =>
          val currentMutation =
            acc.getOrElse(stagingFileAndTags, identity[Tags])
          val newMutation = currentMutation.andThen(tagMutator)
          acc + (stagingFileAndTags -> newMutation)
    changesByStagingFileAndTags.toVector.map:
      case (stagingFileAndTags, tagMutator) =>
        val newTags = tagMutator(stagingFileAndTags.tags)
        AlterTagsAction(
          stagingFileAndTags.stagingFile,
          stagingFileAndTags.tags,
          newTags
        )

  def fixTrackAndDiscNumbers(
      stagingFilesAndTags: Seq[StagingFileAndTags],
      normaliseTrackNumbers: Boolean
  ): Seq[(StagingFileAndTags, Tags => Tags)] =
    val empty: Seq[(StagingFileAndTags, Tags => Tags)] = Seq.empty
    stagingFilesAndTags
      .groupBy(_.tags.albumId)
      .values
      .foldLeft(empty): (acc, stagingFilesAndTagsForDisc) =>
        val totalTracks: Int = stagingFilesAndTagsForDisc.size
        val newStagingFilesAndTagsForDisc
            : Seq[(StagingFileAndTags, Tags => Tags)] =
          stagingFilesAndTagsForDisc.sorted.zipWithIndex.map:
            case (tagsAndFileLocation, idx) =>
              tagsAndFileLocation -> ((tags: Tags) =>
                if normaliseTrackNumbers then
                  tags.copy(
                    trackNumber = idx + 1,
                    totalTracks = totalTracks,
                    discNumber = 1,
                    totalDiscs = 1
                  )
                else
                  tags.copy(
                    discNumber = 1,
                    totalDiscs = 1
                  )
              )
        acc ++ newStagingFilesAndTagsForDisc

  def findMultiDiscAlbums(
      stagingFilesAndTags: Seq[StagingFileAndTags]
  ): Seq[MultiDiscAlbum] =
    val allAlbumsById: Map[String, Seq[StagingFileAndTags]] =
      stagingFilesAndTags.groupBy(tfl => tfl.tags.albumId)
    val multiDiscAlbumsById: Map[String, Seq[StagingFileAndTags]] =
      allAlbumsById.filter { case (_, tracks) =>
        tracks.exists(track =>
          track.tags.discNumber > 1 || track.tags.totalDiscs > 1
        )
      }
    multiDiscAlbumsById.values.map(toMultiDiscAlbum).toVector

  def toMultiDiscAlbum(
      stagingFilesAndTags: Seq[StagingFileAndTags]
  ): MultiDiscAlbum =
    val maybeLowestDiscNumber: Option[Int] =
      stagingFilesAndTags.map(_.tags.discNumber).sorted.headOption
    val (firstDisc, nextDiscs) = stagingFilesAndTags.partition(tfl =>
      maybeLowestDiscNumber.contains(tfl.tags.discNumber)
    )
    def index(tracks: Seq[StagingFileAndTags]): Seq[StagingFileAndTags] =
      tracks.sorted(stagingFileAndTagsOrdering)
    MultiDiscAlbum(index(firstDisc), index(nextDiscs))

  /** Order tags by disc number followed by track number.
    */
  implicit val tagsOrdering: Ordering[Tags] =
    Ordering.by(tags => (tags.discNumber, tags.trackNumber))

  implicit val stagingFileAndTagsOrdering: Ordering[StagingFileAndTags] =
    tagsOrdering.on(_.tags)
