package processor

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source
import logging.Messages
import versioning.Repository

import scala.concurrent.ExecutionContext

class RemoveHashFileActionExecutor(
    val hashEncoder: HashEncoder,
    val repository: Repository
)(implicit
    ec: ExecutionContext
) extends PartialActionExecutor:
  override def apply(): PartialFunction[Action, Source[String, NotUsed]] =
    case RemoveHashFileAction(hashFile) =>
      Source
        .lazySource { () =>
          hashEncoder.remove(hashFile)
          Source.single(Messages.hashFileRemoved(hashFile))
        }
        .mapMaterializedValue(_ => NotUsed)
