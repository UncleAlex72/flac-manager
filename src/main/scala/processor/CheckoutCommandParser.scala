package processor

import cats.data.ValidatedNel
import commands.{CheckoutCommand, Command}
import files.{FlacFile, StagingFile}
import logging.Messages
import users.OwnerService
import validation._

import scala.collection.immutable.Seq
import scala.concurrent.{ExecutionContext, Future}

/** Check out [[FlacFile]]s, making sure that checking out a flac file will not
  * overwrite an existing [[StagingFile]].
  */
class CheckoutCommandParser(
    val parametersValidator: ParametersValidator,
    val ownerService: OwnerService
)(implicit
    ec: ExecutionContext
) extends PartialCommandParser:

  override def apply()
      : PartialFunction[Command, Future[ValidatedNel[String, Seq[Action]]]] =
    case CheckoutCommand(relativeDirectories) =>
      ownerService.owners().map { owners =>
        parametersValidator.validateFlacDirectories(
          relativeDirectories
        ) andThen { flacDirectories =>
          val flacFiles = for
            flacDirectory <- flacDirectories
            flacFile <- flacDirectory.list
          yield flacFile
          checkFlacFilesDoNotOverwrite(flacFiles.toList).map:
            flacAndStagingFiles =>
              flacAndStagingFiles.flatMap { case (flacFile, stagingFile) =>
                val removeHashFileActions = owners.map { owner =>
                  RemoveHashFileAction(flacFile.toHashFile(owner))
                }
                CheckoutFileAction(
                  flacFile,
                  stagingFile
                ) +: removeHashFileActions
              } ++ Seq(CommitAction, ClearCachesAction)
        }
      }

  /** Check that a sequence of flac files would not overwrite existing files in
    * the staging repository if they were to be checked out.
    *
    * @param fileLocations
    *   The sequence of flac file locations to check.
    * @return
    *   A list of flac file locations that wouldn't overwrite a staged file or
    *   [[Messages.overwrite()]] for those that do.
    */
  def checkFlacFilesDoNotOverwrite(
      fileLocations: Seq[FlacFile]
  ): ValidatedNel[String, Seq[(FlacFile, StagingFile)]] =
    Validations.validateSequence(fileLocations) { flacFile =>
      val stagingFile: StagingFile = flacFile.toStagingFile
      if stagingFile.exists then
        Messages.overwrite(flacFile, stagingFile).invalidNel
      else (flacFile, stagingFile).validNel
    }
