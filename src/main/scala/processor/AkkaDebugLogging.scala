package processor

import org.apache.pekko.event.{Logging, LoggingAdapter}
import org.apache.pekko.stream.Attributes
import org.apache.pekko.stream.scaladsl.{Flow, Source}
import org.slf4j.{Logger, LoggerFactory}

object AkkaDebugLogging:

  private val loggingAttributes: Attributes = Attributes
    .logLevels(
      onElement = Logging.DebugLevel,
      onFinish = Logging.DebugLevel,
      onFailure = Logging.DebugLevel
    )

  private def nameOf(name: String): String = s"processor.streams.$name"

  private def loggingAdapter(name: String): LoggingAdapter =
    new LoggingAdapter:
      val fullName: String = nameOf(name)
      val logger: Logger = LoggerFactory.getLogger(fullName)
      def prune(message: String): String = message.substring(3)
      override def isErrorEnabled: Boolean = logger.isErrorEnabled
      override def isWarningEnabled: Boolean = logger.isWarnEnabled
      override def isInfoEnabled: Boolean = logger.isInfoEnabled
      override def isDebugEnabled: Boolean = logger.isDebugEnabled
      override protected def notifyError(message: String): Unit =
        logger.error(prune(message))
      override protected def notifyError(
          cause: Throwable,
          message: String
      ): Unit = logger.error(prune(message), cause)
      override protected def notifyWarning(message: String): Unit =
        logger.warn(prune(message))
      override protected def notifyInfo(message: String): Unit =
        logger.info(prune(message))
      override protected def notifyDebug(message: String): Unit =
        logger.debug(prune(message))

  implicit class FlowDebugLogger[IN, OUT, MAT](flow: Flow[IN, OUT, MAT]):
    def debug(name: String): Flow[IN, OUT, MAT] =
      flow.log("")(loggingAdapter(name)).withAttributes(loggingAttributes)

  implicit class SourceDebugLogger[OUT, MAT](source: Source[OUT, MAT]):
    def debug(name: String): Source[OUT, MAT] =
      source.log("")(loggingAdapter(name)).withAttributes(loggingAttributes)
