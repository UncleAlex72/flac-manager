package processor
import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source
import logging.Messages

class ActionExecutorImpl(partialActionExecutors: PartialActionExecutor*)
    extends PartialCombiner[
      Action,
      Source[String, NotUsed],
      PartialActionExecutor
    ](partialActionExecutors)
    with ActionExecutor:

  def errorBuilder(action: Action): String =
    s"Cannot find an executor for action $action"
