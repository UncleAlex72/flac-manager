package processor

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source
import files._
import logging.Messages

class DeleteFileActionExecutor(val fs: FileSystem)
    extends PartialActionExecutor:

  override def apply(): PartialFunction[Action, Source[String, NotUsed]] =
    case DeleteFileAction(stagingFile) =>
      Source
        .lazySource { () =>
          fs.remove(stagingFile)
          Source.single(Messages.delete(stagingFile))
        }
        .mapMaterializedValue(_ => NotUsed)
