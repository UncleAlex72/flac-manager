package processor

import cats.data.ValidatedNel
import commands.Command
import validation.Validations

import scala.collection.immutable.Seq
import scala.concurrent.Future

class CommandParserImpl(partialCommandParsers: PartialCommandParser*)
    extends PartialCombiner[Command, Future[
      ValidatedNel[String, Seq[Action]]
    ], PartialCommandParser](partialCommandParsers)
    with CommandParser:

  def errorBuilder(command: Command): String =
    s"Cannot find an executor for command $command"
