package processor

trait JobIdGenerator:

  def generate(): String
