package processor
import java.time.Clock
import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.{Flow, Source}
import devices.LossyExtension
import files._
import logging.Messages
import music.Tags

import scala.collection.immutable.Seq
import scala.concurrent.{ExecutionContext, Future}

class CheckinFileActionExecutor(
    val clock: Clock,
    val encoders: Map[LossyExtension, LossyEncoder],
    val fs: FileSystem,
    val repositories: Repositories
)(implicit ec: ExecutionContext)
    extends PartialActionExecutor:

  val encoderSeq: Seq[(LossyExtension, LossyEncoder)] = encoders.toList

  val encoderFlow: Flow[
    (StagingFile, EncodedFile, Tags, LossyEncoder),
    (EncodedFile, Tags, TempFile),
    NotUsed
  ] =
    Flow[(StagingFile, EncodedFile, Tags, LossyEncoder)].mapAsync(1):
      case (stagingFile, encodedFile, tags, encoder) =>
        Future:
          val tempFile = encodedFile.toTempFile
          encoder.encode(stagingFile, tempFile)
          (encodedFile, tags, tempFile)

  val moveEncodedFileFlow
      : Flow[(EncodedFile, Tags, TempFile), String, NotUsed] =
    Flow[(EncodedFile, Tags, TempFile)]
      .flatMapConcat { case (encodedFile, _, tempFile) =>
        Source.lazySource { () =>
          fs.move(tempFile, encodedFile)
          Source.empty
        }
      }

  val moveStagingFileFlow: Flow[(StagingFile, FlacFile), String, NotUsed] =
    Flow[(StagingFile, FlacFile)].flatMapConcat:
      case (stagingFile, flacFile) =>
        Source.lazySource { () =>
          fs.move(stagingFile, flacFile)
          Source.single(Messages.move(stagingFile, flacFile))
        }

  override def apply(): PartialFunction[Action, Source[String, NotUsed]] =
    case CheckinFileAction(source, target, tags) =>
      val inputs
          : Source[Either[(LossyExtension, LossyEncoder), Unit], NotUsed] =
        Source(encoderSeq.map(Left(_))).concat(Source.single(Right(())))
      val encoderSources: Source[String, NotUsed] = inputs.flatMapConcat:
        case Left((extension, encoder)) =>
          val encodedFile = target.toEncodedFile(extension)
          val messageSource =
            Source.single(Messages.encode(source, encodedFile))
          messageSource.concat(
            Source
              .single((source, encodedFile, tags, encoder))
              .via(encoderFlow)
              .via(moveEncodedFileFlow)
          )
        case Right(_) =>
          Source.single((source, target)).via(moveStagingFileFlow)
      Source.single(Messages.checkin(source)).concat(encoderSources)
