package processor

abstract class PartialCombiner[IN, OUT, P <: PartialFunctionFactory[IN, OUT]](
    val partials: Seq[P]
):

  val f: IN => OUT =
    val empty: IN => OUT = in =>
      throw new IllegalStateException(errorBuilder(in))
    partials.foldLeft(empty) { (acc, pf) => action =>
      pf().lift(action).getOrElse(acc(action))
    }

  def errorBuilder(in: IN): String

  def apply(in: IN): OUT =
    f(in)
