package processor

class M4aLossyEncoder(ffmpegEncoder: FfmpegEncoder)
    extends FfmpegLossyEncoder(
      ffmpegEncoder,
      "libfdk_aac",
      "acodec" -> "aac",
      "strict" -> "experimental"
    )
