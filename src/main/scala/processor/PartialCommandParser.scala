package processor

import cats.data.ValidatedNel
import commands.Command
import validation.Validations

import scala.collection.immutable.Seq
import scala.concurrent.Future

trait PartialCommandParser
    extends PartialFunctionFactory[Command, Future[
      ValidatedNel[String, Seq[Action]]
    ]]
