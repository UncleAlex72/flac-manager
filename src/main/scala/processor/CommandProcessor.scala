package processor

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source
import commands.{Command, CommandResponse}

import scala.concurrent.Future

trait CommandProcessor:

  def submit(command: Command): Source[CommandResponse, NotUsed]

  def all(): Source[(String, String), NotUsed]
