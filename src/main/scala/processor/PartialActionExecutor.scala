package processor

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source
import logging.Messages

trait PartialActionExecutor
    extends PartialFunctionFactory[Action, Source[String, NotUsed]]
