package processor

trait PartialFunctionFactory[IN, OUT]:

  def apply(): PartialFunction[IN, OUT]
