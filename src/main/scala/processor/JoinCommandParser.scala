package processor

import cats.data.{NonEmptyList, ValidatedNel}
import com.typesafe.scalalogging.StrictLogging
import commands.{CheckoutCommand, Command, JoinCommand}
import files.{Directory, FlacFile, StagingFile}
import logging.Messages
import users.OwnerService
import validation._

import scala.concurrent.{ExecutionContext, Future}

/** Check out [[FlacFile]]s, making sure that checking out a flac file will not
  * overwrite an existing [[StagingFile]].
  */
class JoinCommandParser(
    val parametersValidator: ParametersValidator
)(implicit
    ec: ExecutionContext
) extends PartialCommandParser
    with StrictLogging:

  override def apply()
      : PartialFunction[Command, Future[ValidatedNel[String, Seq[Action]]]] =
    case JoinCommand(relativeDirectories) =>
      Future:
        parametersValidator
          .validateStagingDirectories(relativeDirectories)
          .andThen { stagingDirectories =>
            stagingDirectories.foreach { stagingDirectory =>
              logger.info(s"Join $stagingDirectory")
            }
            validateAndExtractUniqueFirstAlbum(stagingDirectories).andThen:
              albumInfo =>
                val allFiles: Seq[StagingFile] =
                  stagingDirectories.flatMap(_.list)
                val validatedStagingFilesAndTags
                    : ValidatedNel[String, Seq[StagingFileAndTags]] =
                  Validations.validateSequence(allFiles):
                    StagingFileAndTags(_)
                validatedStagingFilesAndTags.map { stagingFilesAndTags =>
                  val totalTracks = stagingFilesAndTags.size
                  stagingFilesAndTags.zipWithIndex.map:
                    case (stagingFileAndTags, idx) =>
                      AlterTagsAction(
                        stagingFileAndTags.stagingFile,
                        stagingFileAndTags.tags,
                        stagingFileAndTags.tags.copy(
                          albumArtist = albumInfo.albumArtist,
                          albumArtistSort = albumInfo.albumArtistSort,
                          artist = albumInfo.artist,
                          artistSort = albumInfo.artistSort,
                          album = albumInfo.album,
                          discNumber = 1,
                          totalDiscs = 1,
                          totalTracks = totalTracks,
                          trackNumber = idx + 1
                        )
                      )
                }
          }

  def validateAndExtractUniqueFirstAlbum(
      stagingDirectories: Seq[Directory.StagingDirectory]
  ): ValidatedNel[String, AlbumInformation] =
    if stagingDirectories.isEmpty then
      "You must supply at least one directory".invalidNel
    else
      val firstStagingDirectory = stagingDirectories.head
      val validatedAlbumInformations
          : ValidatedNel[String, Seq[AlbumInformation]] =
        Validations.validateSequence(firstStagingDirectory.list.toSeq):
          stagingFile =>
            stagingFile.tags.read().map { tags =>
              AlbumInformation(
                artist = tags.artist,
                artistSort = tags.artistSort,
                albumArtist = tags.albumArtist,
                albumArtistSort = tags.albumArtistSort,
                album = tags.album
              )
            }
      validatedAlbumInformations.andThen { albumInformations =>
        if albumInformations.distinct.size == 1 then
          albumInformations.head.validNel
        else "Cannot uniquely determine the main album".invalidNel
      }

  case class AlbumInformation(
      artist: String,
      artistSort: String,
      albumArtist: String,
      albumArtistSort: String,
      album: String
  )
