package versioning

import com.typesafe.scalalogging.StrictLogging
import files.{HashFile, Repositories}

import java.nio.file.{Files, Path, Paths}
import java.util
import java.util.Collections
import scala.sys.process._
import scala.jdk.CollectionConverters._

class ProcessGitRepository(root: Path, repositories: Repositories)
    extends Repository
    with StrictLogging:

  private val workingDirectory = root.toFile

  if !Files.isDirectory(root.resolve(".git")) then git("init", ".")

  private def git(commands: String*): Seq[String] =
    val fullCommand = "git" +: commands
    val gitCommand = fullCommand.mkString(" ")
    logger.debug(gitCommand)
    val stdOut = Collections.synchronizedList(new util.ArrayList[String]())
    val stdErr = Collections.synchronizedList(new util.ArrayList[String]())
    val processLogger = ProcessLogger(s => stdOut.add(s), s => stdErr.add(s))
    val responseCode = Process(fullCommand, workingDirectory).!(processLogger)
    if responseCode == 0 then stdOut.asScala.toSeq
    else
      val output = stdErr.asScala.toSeq.mkString("\n")
      val errorMessage =
        s"command [$gitCommand] failed with error code [$responseCode]\n$output"
      throw new RuntimeException(errorMessage)

  override def commit(message: String): Unit =
    git("add", ".")
    git("commit", "-m", message, "--allow-empty")
    if git("remote", "-v").nonEmpty then git("push")

  override def latestCommit(): String =
    val log = git("log", "-n", "1", "--pretty=oneline").head
    log.split(' ').head

  private def parseHashFile(
      owner: String,
      path: String,
      builder: HashFile => Revision
  ): Seq[Revision] =
    if path.startsWith(s"$owner/") then
      val relativePath = Paths.get(owner).relativize(Paths.get(path))
      repositories.hash(owner).file(relativePath).toOption.toSeq.map(builder)
    else Seq.empty

  override def revisions(
      owner: String,
      commitId: String
  ): Seq[Revision] =
    val create = """create mode \d+ (.+)""".r
    val delete = """delete mode \d+ (.+)""".r
    val rename = """rename (.+?) => (.+?) \(\d+%\)""".r
    git("diff", "--summary", commitId).flatMap { summary =>
      summary.trim match
        case create(added)   => parseHashFile(owner, added, Revision.added)
        case delete(removed) => parseHashFile(owner, removed, Revision.removed)
        case rename(removed, added) =>
          parseHashFile(owner, added, Revision.added) ++ parseHashFile(
            owner,
            removed,
            Revision.removed
          )
    }
