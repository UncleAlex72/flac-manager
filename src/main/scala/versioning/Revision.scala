package versioning

import files.HashFile

sealed trait Revision:

  val hashFile: HashFile

object Revision:
  case class Added(hashFile: HashFile) extends Revision
  case class Removed(hashFile: HashFile) extends Revision

  def added(hashFile: HashFile): Revision = Added(hashFile)
  def removed(hashFile: HashFile): Revision = Removed(hashFile)
