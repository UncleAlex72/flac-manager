package versioning

import files.HashFile

import scala.util.Try

/** A wrapper around
  */
trait Repository:
  def commit(message: String): Unit

  def revisions(owner: String, commitId: String): Seq[Revision]

  def latestCommit(): String
