package users

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.Sink
import devices.{Device, DeviceDao}

import scala.collection.immutable
import scala.concurrent.{ExecutionContext, Future}

class OwnerServiceImpl(deviceDao: DeviceDao)(implicit
    ec: ExecutionContext,
    actorSystem: ActorSystem
) extends OwnerService:

  override def owners(): Future[Seq[String]] =
    val owners: Future[Seq[String]] = deviceDao
      .allDevices()
      .map(_.owner)
      .runWith(Sink.collection[String, Seq[String]])
    owners.map(_.sorted.distinct)
