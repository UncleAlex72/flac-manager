package users

import scala.concurrent.Future

trait OwnerService:
  def owners(): Future[Seq[String]]
