import cats.data.ValidatedNel
import cats.syntax.{ApplySyntax, ValidatedExtensionSyntax, ValidatedSyntax}
import logging.Messages

import scala.collection.SortedSet
import scala.collection.immutable.Seq

package object validation
    extends ApplySyntax
    with ValidatedSyntax
    with ValidatedExtensionSyntax:

  object Validations:

    def sequence[A](
        as: Iterable[ValidatedNel[String, A]]
    ): ValidatedNel[String, Seq[A]] =
      val empty: ValidatedNel[String, Seq[A]] = Seq.empty.validNel
      as.foldLeft(empty) { (results, a) =>
        (results, a).mapN(_ :+ _)
      }

    def validateSequence[A, B](
        as: Iterable[A]
    )(
        validationStep: A => ValidatedNel[String, B]
    ): ValidatedNel[String, Seq[B]] =
      val empty: ValidatedNel[String, Seq[B]] = Seq.empty.validNel
      as.foldLeft(empty) { (results, a) =>
        (results, validationStep(a)).mapN(_ :+ _)
      }

    def validateAndFlattenSequence[A, B](as: Iterable[A])(
        validationStep: A => ValidatedNel[String, Seq[B]]
    ): ValidatedNel[String, Seq[B]] =
      validateSequence(as)(validationStep).map(_.flatten)

    def validateSortedSequence[A, B](as: Iterable[A])(
        validationStep: A => ValidatedNel[String, B]
    )(implicit ordering: Ordering[B]): ValidatedNel[String, SortedSet[B]] =
      val empty: ValidatedNel[String, SortedSet[B]] = SortedSet.empty.validNel
      as.foldLeft(empty) { (results, a) =>
        (results, validationStep(a)).mapN(_ + _)
      }
