package devices

import java.time.Instant
import org.apache.pekko.NotUsed
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.Source
import devices.Extension.M4A
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AsyncWordSpec
import text.{Normaliser, RegularExpressionNormaliser}
import uk.co.unclealex.mongodb.bson.ID

import scala.concurrent.Future

class DeviceIdentifierServiceImplSpec extends AsyncWordSpec with Matchers:

  implicit val normaliser: Normaliser = new RegularExpressionNormaliser

  implicit val actorSystem: ActorSystem = ActorSystem()

  // noinspection NotImplementedCode
  val deviceDao: DeviceDao = new DeviceDao:
    override def allDevices(): Source[Device, NotUsed] =
      Source(Seq("", "1", "2").map { suffix =>
        Device("Freddie's Phone", s"freddiesphone$suffix", "freddie", M4A)
      })
    override def findByDisplayName(
        displayName: String
    ): Source[Device, NotUsed] = ???
    override def findByIdentifier(identifier: String): Source[Device, NotUsed] =
      ???
    override def store(device: Device): Source[Device, NotUsed] = ???
    override def removeById(id: ID): Source[Long, NotUsed] = ???
    override def updateLastSynchronised(
        identifier: String,
        lastSynchronised: Instant,
        commit: String
    ): Source[Device, NotUsed] = ???

    override def updateOffset(
        identifier: String,
        offset: Int
    ): Source[Device, NotUsed] = ???
  val deviceIdentifierService = new DeviceIdentifierServiceImpl(deviceDao)

  "Creating an identifier for a string that already exists" should:
    "create an identifier with a unique suffix" in:
      for identifier <- deviceIdentifierService.generate("Freddie's Phone")
      yield identifier should ===("freddiesphone3")

  "Creating an identifier for a string that does not exist" should:
    "create an identifier with no suffix" in:
      for identifier <- deviceIdentifierService.generate("Brian's Phone")
      yield identifier should ===("briansphone")
