package text

import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class NumberNormaliserSpec extends AnyWordSpec with Matchers:

  val numberNormaliser: NumberNormaliser = new NumberNormaliserImpl
  def normaliser(totalNumberOfTracks: Int): Int => String =
    track => numberNormaliser.normalise(totalNumberOfTracks, track)

  "A disc with 9 tracks" should:
    "have tracks with length two" in:
      val normalise = normaliser(9)
      normalise(1) should ===("01")

  "A disc with 50 tracks" should:
    "have tracks with length two" in:
      val normalise = normaliser(50)
      normalise(1) should ===("01")
      normalise(15) should ===("15")

  "A disc with 150 tracks" should:
    "have tracks with length three" in:
      val normalise = normaliser(150)
      normalise(1) should ===("001")
      normalise(15) should ===("015")
      normalise(115) should ===("115")
