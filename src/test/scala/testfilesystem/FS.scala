/*
 * Copyright 2018 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package testfilesystem

import org.apache.pekko.http.scaladsl.model.MediaTypes

import java.nio.file.attribute.PosixFilePermission._
import java.nio.file.attribute.{
  FileTime,
  PosixFilePermission,
  PosixFilePermissions
}
import java.nio.file.{FileSystem, Files, Path}
import java.time.Instant
import java.util.UUID
import com.github.marschall.memoryfilesystem.MemoryFileSystemBuilder
import com.typesafe.scalalogging.StrictLogging
import io.circe.Json
import io.circe.config.{parser, printer}
import io.circe.syntax._
import music.{CoverArt, Tags}
import org.scalatest.matchers.Matcher
import org.scalatest.wordspec.FixtureAsyncWordSpec
import org.scalatest.FutureOutcome
import testfilesystem.FS.Permissions.OwnerReadAndWrite

import scala.jdk.CollectionConverters._
import scala.jdk.StreamConverters._
import scala.util.{Failure, Success, Try, Using}

/** Created by alex on 12/06/17
  */
trait FS[T] extends FixtureAsyncWordSpec:

  def setup(fs: FileSystem): T

  type FixtureParam = T

  override def withFixture(test: OneArgAsyncTest): FutureOutcome =
    val fs: FileSystem = MemoryFileSystemBuilder
      .newLinux()
      .setUmask(
        Set(
          GROUP_READ,
          GROUP_WRITE,
          GROUP_EXECUTE,
          OTHERS_READ,
          OTHERS_WRITE,
          OTHERS_EXECUTE
        ).asJava
      )
      .build(s"test-${UUID.randomUUID().toString}")
    val param: FixtureParam = setup(fs)
    withFixture(test.toNoArgAsyncTest(param)).onCompletedThen { _ =>
      Try(closeParam(param))
      Try(fs.close())
    }

  def closeParam(param: T): Unit = {}

object FS:
  sealed trait Permissions:
    val filePosixPermissions: Set[PosixFilePermission]
    lazy val directoryPosixPermissions: Set[PosixFilePermission] =
      filePosixPermissions.flatMap { permission =>
        val maybeExtraPermission: Option[PosixFilePermission] =
          permission match
            case PosixFilePermission.OWNER_READ =>
              Some(PosixFilePermission.OWNER_EXECUTE)
            case PosixFilePermission.GROUP_READ =>
              Some(PosixFilePermission.GROUP_EXECUTE)
            case PosixFilePermission.OTHERS_READ =>
              Some(PosixFilePermission.OTHERS_EXECUTE)
            case _ => None
        maybeExtraPermission.toSeq :+ permission
      }

  object Permissions:
    object OwnerReadOnly extends Permissions:
      override val filePosixPermissions: Set[PosixFilePermission] = Set(
        PosixFilePermission.OWNER_READ
      )
    object AllReadOnly extends Permissions:
      override val filePosixPermissions: Set[PosixFilePermission] = Set(
        PosixFilePermission.OWNER_READ,
        PosixFilePermission.GROUP_READ,
        PosixFilePermission.OTHERS_READ
      )
    object OwnerReadAndWrite extends Permissions:
      override val filePosixPermissions: Set[PosixFilePermission] =
        Set(PosixFilePermission.OWNER_READ, PosixFilePermission.OWNER_WRITE)
    object OwnerWriteAllRead extends Permissions:
      override val filePosixPermissions: Set[PosixFilePermission] =
        Set(AllReadOnly, OwnerReadAndWrite).flatMap(_.filePosixPermissions)

  object Builder extends Builder
  trait Builder extends StrictLogging:
    type JFS = FileSystem
    sealed trait FsEntryBuilder:
      val name: String
      val maybeLastModified: Option[Instant]
    case class FsDirectoryBuilder(
        name: String,
        permissions: Permissions,
        children: Seq[FsEntryBuilder]
    ) extends FsEntryBuilder:
      override val maybeLastModified: Option[Instant] = None
    case class FsFileBuilder(
        name: String,
        permissions: Permissions,
        maybeTags: Option[Tags],
        maybeLastModified: Option[Instant]
    ) extends FsEntryBuilder

    def construct(fs: JFS, fsEntryBuilders: Seq[FsEntryBuilder]): Unit =

      def ensureWritable(path: Path)(block: => Unit): Unit =
        val parent = path.getParent
        val parentPosixPermissions = Files.getPosixFilePermissions(parent)
        val writableParentPosixPermissions =
          parentPosixPermissions.asScala.toSet ++ OwnerReadAndWrite.directoryPosixPermissions
        Files.setPosixFilePermissions(
          parent,
          writableParentPosixPermissions.asJava
        )
        block
        Files.setPosixFilePermissions(parent, parentPosixPermissions)

      def _construct(root: Path, fsEntryBuilders: Seq[FsEntryBuilder]): Unit =
        fsEntryBuilders.foreach:
          case FsFileBuilder(name, permissions, maybeTags, maybeLastModified) =>
            val path: Path = root.resolve(name)
            logger.info(s"Creating file $path")
            ensureWritable(path):
              Files.createFile(path)
              maybeTags.foreach { tags =>
                Files.write(
                  path,
                  printer.print(tags.asJson).split('\n').toSeq.asJava
                )
              }
              maybeLastModified.foreach { lastModified =>
                Files.setLastModifiedTime(path, FileTime.from(lastModified))
              }
              Files.setPosixFilePermissions(
                path,
                permissions.filePosixPermissions.asJava
              )
          case FsDirectoryBuilder(name, permissions, children) =>
            val path: Path = root.resolve(name)
            logger.info(s"Creating directory $path")
            ensureWritable(path):
              Files.createDirectories(path)
              _construct(path, children)
              Files.setPosixFilePermissions(
                path,
                permissions.directoryPosixPermissions.asJava
              )
      _construct(fs.getPath("/"), fsEntryBuilders)

    def tags(
        artist: String,
        album: String,
        albumId: String,
        totalDiscs: Int,
        discNumber: Int,
        totalTracks: Int,
        trackNumber: Int,
        track: String
    ): Tags =
      Tags(
        albumArtistSort = artist,
        albumArtist = artist,
        album = album,
        artist = artist,
        artistSort = artist,
        title = track,
        totalDiscs = totalDiscs,
        totalTracks = totalTracks,
        discNumber = discNumber,
        discSubtitle = None,
        albumArtistId = artist,
        albumId = albumId,
        artistId = artist,
        trackId = None,
        asin = None,
        trackNumber = trackNumber,
        coverArt = CoverArt(Array[Byte](), MediaTypes.`image/jpeg`),
        originalDate = None,
        date = None,
        year = None
      )
  object Dsl extends Dsl
  trait Dsl extends Builder:
    object D:
      def apply(
          name: String,
          childBuilders: FsEntryBuilder*
      ): FsDirectoryBuilder =
        FsDirectoryBuilder(name, Permissions.OwnerWriteAllRead, childBuilders)
      def apply(
          name: String,
          permissions: Permissions,
          childBuilders: FsEntryBuilder*
      ): FsDirectoryBuilder =
        FsDirectoryBuilder(name, permissions, childBuilders)
    object F:
      def apply(name: String, maybeTags: Option[Tags]): FsFileBuilder =
        FsFileBuilder(name, Permissions.OwnerWriteAllRead, maybeTags, None)
      def apply(name: String): FsFileBuilder = apply(name, None)
      def apply(name: String, tags: Tags): FsFileBuilder =
        apply(name, Some(tags))
      def apply(
          name: String,
          permissions: Permissions,
          maybeTags: Option[Tags]
      ): FsFileBuilder =
        FsFileBuilder(name, permissions, maybeTags, None)
      def apply(name: String, permissions: Permissions): FsFileBuilder =
        apply(name, permissions, None)
      def apply(
          name: String,
          permissions: Permissions,
          tags: Tags
      ): FsFileBuilder =
        apply(name, permissions, Some(tags))

    implicit class FileSystemExtensions(fs: JFS):
      val blacklist: Seq[String] = Seq("tmp", "home")
      def entries: Seq[FsEntry] =
        def list(path: Path): Seq[Path] =
          Using(Files.list(path))(_.toScala(Seq)).get
        def deconstruct(path: Path): Seq[FsEntry] =
          if Files.isSymbolicLink(path) then
            Seq(FsLink(path, Files.readSymbolicLink(path)))
          else
            val permissions: Set[PosixFilePermission] =
              Files.getPosixFilePermissions(path).asScala.toSet
            if Files.isRegularFile(path) then
              val content: String =
                Files.readAllLines(path).asScala.mkString("\n").trim
              val maybeTags: Option[Tags] =
                parser.decode[Tags](content).toOption
              Seq(FsFile(path, permissions, maybeTags))
            else if Files.isDirectory(path) then
              val children: Seq[FsEntry] =
                list(path).flatMap(deconstruct).sorted
              Seq(FsDirectory(path, permissions, children))
            else Seq.empty
        list(fs.getPath("/"))
          .flatMap(deconstruct)
          .filterNot(entry =>
            blacklist.contains(entry.path.getFileName.toString)
          )

      def add(fsEntryBuilders: FsEntryBuilder*): Unit =
        construct(fs, fsEntryBuilders)
      def expected(fsEntryBuilders: FsEntryBuilder*): Seq[FsEntry] =
        def toEntries(
            root: Path,
            fsEntryBuilders: Seq[FsEntryBuilder]
        ): Seq[FsEntry] =
          fsEntryBuilders.flatMap:
            case FsFileBuilder(name, permissions, maybeTags, _) =>
              Seq(
                FsFile(
                  root.resolve(name),
                  permissions.filePosixPermissions,
                  maybeTags
                )
              )
            case FsDirectoryBuilder(name, permissions, children) =>
              val path: Path = root.resolve(name)
              val childEntries: Seq[FsEntry] = toEntries(path, children)
              Seq(
                FsDirectory(
                  path,
                  permissions.directoryPosixPermissions,
                  childEntries.sorted
                )
              )
        fsEntryBuilders
          .flatMap(fsEntryBuilder =>
            toEntries(fs.getPath("/"), Seq(fsEntryBuilder))
          )
          .sorted

sealed trait FsEntry:
  val path: Path
  def toJson(includePermissions: Boolean): Json

  protected def permissionsToEntries(
      posixFilePermissions: Set[PosixFilePermission],
      include: Boolean
  ): Map[String, Json] =
    if include then
      Map(
        "permissions" ->
          PosixFilePermissions.toString(posixFilePermissions.asJava).asJson
      )
    else Map.empty

  protected def pathToEntries(path: Path): Map[String, Json] =
    Map("name" -> path.getFileName.toString.asJson)

case class FsDirectory(
    path: Path,
    posixFilePermissions: Set[PosixFilePermission],
    children: Seq[FsEntry]
) extends FsEntry:
  override def toJson(includePermissions: Boolean): Json =
    val childObjects = Map(
      "children" -> children.map(_.toJson(includePermissions)).asJson
    )
    val entries: Map[String, Json] =
      pathToEntries(path) ++ permissionsToEntries(
        posixFilePermissions,
        includePermissions
      )
    (entries ++ childObjects).asJson
case class FsFile(
    path: Path,
    posixFilePermissions: Set[PosixFilePermission],
    maybeTags: Option[Tags]
) extends FsEntry:
  override def toJson(includePermissions: Boolean): Json =
    val map: Map[String, Json] = pathToEntries(path) ++ permissionsToEntries(
      posixFilePermissions,
      includePermissions
    ) ++ maybeTags.map(tags => "tags" -> tags.asJson)
    map.asJson
case class FsLink(path: Path, target: Path) extends FsEntry:
  override def toJson(includePermissions: Boolean): Json =
    (pathToEntries(path) + ("target" -> target.toString.asJson)).asJson

object FsEntry:
  implicit val ordering: Ordering[FsEntry] =
    Ordering.by(_.path.getFileName.toString)

object LatestModified:
  def apply(times: Seq[Instant]): Instant =
    if times.isEmpty then Instant.EPOCH
    else times.max

import org.scalatest.matchers.should.Matchers._
import org.scalatest.matchers.MatcherProducers._

trait FsEntryMatchers:

  private def fsEntryMatcherFactory(
      includePermissions: Boolean,
      fsEntries: Seq[FsEntry]
  ): Matcher[Seq[FsEntry]] =
    def toJson(entries: Seq[FsEntry]): String =
      printer.print(
        entries.sorted.map(_.toJson(includePermissions)).asJson
      )
    val matcher: String => Matcher[String] = be(_: String)
    matcher.composeTwice(toJson)(fsEntries)

  def haveTheSameEntriesAs(fsEntries: Seq[FsEntry]): Matcher[Seq[FsEntry]] =
    fsEntryMatcherFactory(includePermissions = true, fsEntries)

  def haveTheSameEntriesAsIgnoringPermissions(
      fsEntries: Seq[FsEntry]
  ): Matcher[Seq[FsEntry]] =
    fsEntryMatcherFactory(includePermissions = false, fsEntries)
