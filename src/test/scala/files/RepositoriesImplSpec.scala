/*
 * Copyright 2018 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package files

import org.apache.pekko.http.scaladsl.model.MediaTypes
import cats.data.Validated.Valid
import cats.data._
import cats.implicits._
import com.typesafe.scalalogging.StrictLogging
import devices.Extension.{M4A, MP3}
import io.circe.config.parser
import io.circe.syntax._
import music.{CoverArt, Tags}
import org.scalatest.EitherValues
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.FixtureAsyncWordSpec
import testfilesystem.FS.*
import testfilesystem.FS.Builder.*
import testfilesystem.FS.Dsl.*

import java.nio.file.{Files, FileSystem => JFS}
import java.time.{Clock, Instant}
import scala.collection.SortedSet

/** Created by alex on 14/06/17
  */
class RepositoriesImplSpec
    extends FixtureAsyncWordSpec
    with TestRepositories[FileSystemAndRepositories]
    with StrictLogging
    with PathMatchers
    with Matchers
    with EitherValues
    with RepositoryEntry.Dsl:

  val defaultTags: Tags = Tags(
    albumArtistSort = "albumArtistSort",
    albumArtist = "albumArtist",
    album = "album",
    artist = "artist",
    artistSort = "artistSort",
    title = "title",
    totalDiscs = 1,
    totalTracks = 2,
    discNumber = 3,
    discSubtitle = Some("Foo"),
    albumArtistId = "albumArtistId",
    albumId = "albumId",
    artistId = "artistId",
    trackId = Some("trackId"),
    asin = Some("asin"),
    trackNumber = 4,
    coverArt = CoverArt(Array[Byte](0), MediaTypes.`image/jpeg`),
    originalDate = None,
    date = None,
    year = None
  )

  def now: Instant = Clock.systemDefaultZone().instant()

  "Reading files in a repository" should:
    case class FilesTestCase[F <: File, R <: Repository[F]](
        name: String,
        repositoryFactory: Repositories => R,
        rootDirs: String*
    )
    val stagingTestCase =
      FilesTestCase[StagingFile, StagingRepository](
        "staging",
        _.staging,
        "staging"
      )
    val flacTestCase =
      FilesTestCase[FlacFile, FlacRepository]("flac", _.flac, "flac")
    val encodedTestCase =
      FilesTestCase[EncodedFile, EncodedRepository](
        "encoded",
        _.encoded(MP3),
        "encoded",
        "mp3"
      )
    def runTests[F <: File, R <: Repository[F]](
        testCase: FilesTestCase[F, R]
    ): Unit =
      val rootPaths = "music" +: testCase.rootDirs
      val rootPath = "/" + rootPaths.mkString("/")
      def R(
          child: FsEntryBuilder,
          children: FsEntryBuilder*
      ): FsEntryBuilder =
        def rootDir(directories: List[String]): NonEmptyList[FsEntryBuilder] =
          directories match
            case Nil     => NonEmptyList.of(child, children*)
            case d :: ds => NonEmptyList.of(D(d, rootDir(ds).toList*))
        rootDir(rootPaths.toList).head
      s"be able to identify a directory in the ${testCase.name} repository" in:
        fsr =>
          val fs = fsr.fs
          val repositories = fsr.repositories
          val repository = testCase.repositoryFactory(repositories)
          fs.add(
            R(
              D("dir")
            )
          )
          val dir =
            repository.directory(fs.getPath("dir")).toEither.toOption.get
          dir.absolutePath.toString should be(s"$rootPath/dir")
          dir.relativePath.toString should be(s"dir")
          repository
            .file(fs.getPath("dir"))
            .toEither
            .left
            .value
            .toList should not be empty
      s"be able to identify a file in the ${testCase.name} repository" in:
        fsr =>
          val fs = fsr.fs
          val repositories = fsr.repositories
          val repository = testCase.repositoryFactory(repositories)
          fs.add(
            R(
              F("file")
            )
          )
          val f = repository.file(fs.getPath("file")).toEither.toOption.get
          f.absolutePath.toString should be(s"$rootPath/file")
          f.relativePath.toString should be(s"file")
          f.exists should be(true)
          repository
            .directory(fs.getPath("file"))
            .toEither
            .left
            .value
            .toList should not be empty
      s"be able to list and group files in a directory in the ${testCase.name} repository" in:
        fsr =>
          val fs = fsr.fs
          val repositories = fsr.repositories
          val repository = testCase.repositoryFactory(repositories)
          fs.add(
            R(
              D("numbers", F("one"), F("two"), D("biggest", F("three"))),
              D("letters", F("a"), F("b"), D("biggest", F("c")))
            )
          )
          val d = repository.directory(fs.getPath("")).toEither.toOption.get
          d.list.map(
            _.relativePath.toString
          ) should contain theSameElementsAs Seq(
            "letters/a",
            "letters/b",
            "letters/biggest/c",
            "numbers/one",
            "numbers/two",
            "numbers/biggest/three"
          )
          d.group
            .map(kv =>
              kv._1.relativePath.toString -> kv._2.map(_.relativePath.toString)
            )
            .toSeq should contain theSameElementsAs Seq(
            "letters" -> SortedSet("letters/a", "letters/b"),
            "letters/biggest" -> SortedSet("letters/biggest/c"),
            "numbers" -> SortedSet("numbers/one", "numbers/two"),
            "numbers/biggest" -> SortedSet("numbers/biggest/three")
          )
      s"be able to read tags from a file in the ${testCase.name} repository" in:
        fsr =>
          val fs = fsr.fs
          val repositories = fsr.repositories
          val repository = testCase.repositoryFactory(repositories)
          fs.add(
            R(
              F("file", defaultTags)
            )
          )
          val t = repository
            .file(fs.getPath("file"))
            .toEither
            .flatMap(_.tags.read().toEither)
            .toOption
            .get
          t should be(defaultTags)

          repository
            .directory(fs.getPath("file"))
            .toEither
            .left
            .value
            .toList should not be empty

      s"ignore hidden files and directories in the ${testCase.name} repository" in:
        fsr =>
          val fs = fsr.fs
          val repositories = fsr.repositories
          val repository = testCase.repositoryFactory(repositories)
          fs.add(
            R(
              D("visible", F("F1"), F("F2"), F(".hidden.invisible")),
              D(".hidden", F("thumb.1"), F("thumb.2"))
            )
          )
          val root = repository.root.toEither.toOption.get
          val validatedExpectedFiles =
            val empty: ValidatedNel[String, Seq[F]] = Valid(Seq.empty)
            Seq("F1", "F2").foldLeft(empty) { (acc, filename) =>
              val validatedFile =
                repository.file(fs.getPath("visible", filename))
              (acc, validatedFile).mapN(_ :+ _)
            }
          val files = validatedExpectedFiles.toEither.toOption.get
          root.list should contain theSameElementsAs files
    runTests(stagingTestCase)
    runTests(flacTestCase)
    runTests(encodedTestCase)

  "Flac files" should:
    "resolve to staging files and encoded files with the same directory structure" in:
      fsr =>
        val fs = fsr.fs
        val repositories = fsr.repositories
        fs.flac(
          Artists(
            "Queen" -> Albums(
              Album("A Night at the Opera", Tracks("Death on Two Legs"))
            )
          )
        )
        val flacFile = repositories.flac
          .file(
            fs.getPath("Q/Queen/A Night at the Opera/01 Death on Two Legs.flac")
          )
          .toEither
          .toOption
          .get
        flacFile.toStagingFile.absolutePath.toString should ===(
          "/music/staging/Q/Queen/A Night at the Opera/01 Death on Two Legs.flac"
        )

  "Encoded files" should:
    "create a temporary file on demand" in { fsr =>
      val fs = fsr.fs
      val repositories = fsr.repositories
      fs.encoded(
        Artists(
          "Queen" -> Albums(
            Album("A Night at the Opera", Tracks("Death on Two Legs"))
          )
        )
      )
      val encodedFile = repositories
        .encoded(M4A)
        .file(
          fs.getPath("Q/Queen/A Night at the Opera/01 Death on Two Legs.m4a")
        )
        .toEither
        .toOption
        .get
      encodedFile.toTempFile.absolutePath should exist
    }

  "Staging files" should:
    val originalTags = Tags(
      albumArtistSort = "Queen",
      albumArtist = "albumArtist",
      album = "A Night at the Opera",
      artist = "Queen!",
      artistSort = "artistSort",
      title = "Lazing on a Sunday Afternoon",
      totalDiscs = 2,
      totalTracks = 2,
      discNumber = 3,
      discSubtitle = Some("Foo"),
      albumArtistId = "albumArtistId",
      albumId = "albumId",
      artistId = "artistId",
      trackId = Some("trackId"),
      asin = Some("asin"),
      trackNumber = 2,
      coverArt = CoverArt(Array[Byte](0), MediaTypes.`image/jpeg`),
      originalDate = None,
      date = None,
      year = None
    )
    val newTags = Tags(
      albumArtistSort = "Queen",
      albumArtist = "albumArtist",
      album = "A Night at the Opera",
      artist = "Queen!",
      artistSort = "artistSort",
      title = "Lazing on a Sunday Afternoon",
      totalDiscs = 1,
      totalTracks = 2,
      discNumber = 1,
      discSubtitle = Some("Foo"),
      albumArtistId = "albumArtistId",
      albumId = "albumId",
      artistId = "artistId",
      trackId = Some("trackId"),
      asin = Some("asin"),
      trackNumber = 2,
      coverArt = CoverArt(Array[Byte](0), MediaTypes.`image/jpeg`),
      originalDate = None,
      date = None,
      year = None
    )
    "resolve to a flac file with a directory structure determined by its tags" in:
      fsr =>
        val fs = fsr.fs
        val repositories = fsr.repositories
        fs.staging(
          Permissions.OwnerReadAndWrite -> F(
            "Lazing on a Sunday Afternoon.flac",
            originalTags
          )
        )
        val flacFileAndTags = repositories.staging
          .file(fs.getPath("Lazing on a Sunday Afternoon.flac"))
          .toEither
          .flatMap(_.toFlacFileAndTags.toEither)
          .toOption
          .get
        val (flacFile, tags) = flacFileAndTags
        flacFile.relativePath.toString should ===(
          "Q/Queen/A Night at the Opera/02 Lazing on a Sunday Afternoon.flac"
        )
        tags should ===(originalTags)
    "be able to write tags" in { fsr =>
      val fs = fsr.fs
      val repositories = fsr.repositories
      fs.staging(
        Permissions.OwnerReadAndWrite -> F(
          "Lazing on a Sunday Afternoon.flac",
          originalTags
        )
      )
      val path = fs.getPath("Lazing on a Sunday Afternoon.flac")
      val stagingFile = repositories.staging
        .file(path)
        .toEither
        .map(_.writeTags(newTags))
        .toOption
        .get
      stagingFile.tags.read().toEither.toOption.get should ===(newTags)
      parser.parse(Files.readString(stagingFile.absolutePath)) should ===(
        Right(newTags.asJson)
      )
    }

  override def generate(
      fs: JFS,
      repositories: Repositories
  ): FileSystemAndRepositories =
    FileSystemAndRepositories(fs, repositories)

case class FileSystemAndRepositories(fs: JFS, repositories: Repositories)
