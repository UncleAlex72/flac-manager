/*
 * Copyright 2018 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package files

import java.nio.file.{Files, Path}

import org.scalatest.enablers.Existence
import org.scalatest.matchers.{MatchResult, Matcher}

/** Specs2 matchers for Paths. Created by alex on 24/10/14.
  */
trait PathMatchers:

  implicit val pathExists: Existence[Path] = (path: Path) => Files.exists(path)

  val beADirectory: Matcher[Path] = (left: Path) => {
    MatchResult(
      Files.isDirectory(left),
      s"$left is not a directory",
      s"$left is a directory"
    )
  }

  def beTheSameFileAs(otherPath: Path): Matcher[Path] = (left: Path) => {
    MatchResult(
      Files.isSameFile(left, otherPath),
      s"$left is not the same path as $otherPath",
      s"$left is the same path as $otherPath"
    )
  }

  val beAbsolute: Matcher[Path] = (left: Path) => {
    MatchResult(
      left.isAbsolute,
      s"$left is relative",
      s"$left is absolute"
    )
  }
