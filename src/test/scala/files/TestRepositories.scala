/*
 * Copyright 2018 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package files

import java.nio.file.{Files, Path, FileSystem => JFileSystem}
import java.time.Instant
import com.typesafe.scalalogging.StrictLogging
import configuration.TestDirectories
import devices.Extension
import devices.Extension.{FLAC, M4A, MP3}
import io.circe.config.{parser, printer}
import io.circe.syntax.EncoderOps
import music.{Tags, TagsService}
import testfilesystem.FS.Permissions
import testfilesystem._
import text.{
  Normaliser,
  NumberNormaliser,
  NumberNormaliserImpl,
  RegularExpressionNormaliser
}

/** Created by alex on 17/06/17
  */
trait TestRepositories[T] extends FS[T] with StrictLogging:

  implicit val normaliser: Normaliser = new RegularExpressionNormaliser
  implicit val numberNormaliser: NumberNormaliser = new NumberNormaliserImpl

  final override def setup(fs: JFileSystem): T =
    val directories: Directories = TestDirectories(fs, "/tmp", "/music/datum")
    Files.createDirectories(directories.temporaryPath)
    val tagsService: TagsService = new TagsService:
      override def readTags(path: Path): Tags =
        parser.decode[Tags](Files.readString(path)).toTry.get

      def write(path: Path, tags: Tags): Unit =
        Files.writeString(path, printer.print(tags.asJson))
    val flacFileChecker: FlacFileChecker = (path: Path) =>
      path.getFileName.toString.endsWith(".flac")
    generate(
      fs,
      new RepositoriesImpl(directories, tagsService, flacFileChecker)
    )

  def generate(fs: JFileSystem, repositories: Repositories): T

case class UserEntryBuilder(
    user: String,
    artistsEntryBuilder: ArtistsEntryBuilder
)
case class ArtistsEntryBuilder(artistEntryBuilders: Seq[ArtistEntryBuilder])
case class ArtistEntryBuilder(
    artist: String,
    albumEntryBuilders: Seq[AlbumEntryBuilder]
)
case class AlbumEntryBuilder(
    album: String,
    discEntryBuilders: Seq[DiscEntryBuilder],
    maybeLastModified: Option[Instant]
)
case class DiscEntryBuilder(
    albumId: String,
    trackEntryBuilders: Seq[TrackEntryBuilder],
    maybeLastModified: Option[Instant]
)
case class TrackEntryBuilder(track: String, maybeLastModified: Option[Instant])

object RepositoryEntry:
  object Builder extends Builder
  trait Builder extends FS.Builder:

    def toFsEntryBuilder(
        userEntryBuilder: UserEntryBuilder,
        permissions: Permissions,
        extensions: Seq[Extension],
        maybeLastModified: Option[Instant]
    ): FsEntryBuilder =
      FsDirectoryBuilder(
        userEntryBuilder.user,
        permissions,
        toFsEntryBuilders(
          userEntryBuilder.artistsEntryBuilder,
          permissions,
          extensions,
          maybeLastModified
        )
      )

    def toFsEntryBuilders(
        artistsEntry: ArtistsEntryBuilder,
        permissions: Permissions,
        extensions: Seq[Extension],
        maybeLastModified: Option[Instant]
    ): Seq[FsEntryBuilder] =
      val artistEntriesByInitial: Map[String, Seq[ArtistEntryBuilder]] =
        artistsEntry.artistEntryBuilders.groupBy(_.artist.substring(0, 1))
      def extensionChildren(extension: Extension): Seq[FsDirectoryBuilder] =
        artistEntriesByInitial.toSeq.map { case (initial, artistEntries) =>
          FsDirectoryBuilder(
            initial,
            permissions,
            convertArtistEntries(initial, artistEntries, permissions, extension)
          )
        }
      // Don't prepend FLAC as a directory.
      if extensions.forall(_.isLossy) then
        extensions.map { extension =>
          FsDirectoryBuilder(
            extension.extension,
            permissions,
            extensionChildren(extension)
          )
        }
      else extensions.flatMap(extensionChildren)

    private def convertArtistEntries(
        initial: String,
        artistEntryBuilders: Seq[ArtistEntryBuilder],
        permissions: Permissions,
        extension: Extension
    ): Seq[FsEntryBuilder] =
      artistEntryBuilders.map { artistEntryBuilder =>
        val artist: String = artistEntryBuilder.artist
        FsDirectoryBuilder(
          artist,
          permissions,
          convertAlbumEntries(
            initial,
            artist,
            artistEntryBuilder.albumEntryBuilders,
            permissions,
            extension
          )
        )
      }

    private def convertAlbumEntries(
        initial: String,
        artist: String,
        albumEntryBuilders: Seq[AlbumEntryBuilder],
        permissions: Permissions,
        extension: Extension
    ): Seq[FsEntryBuilder] =
      albumEntryBuilders.flatMap { albumEntryBuilder =>
        val album: String = albumEntryBuilder.album
        convertDiscEntries(
          initial,
          artist,
          album,
          albumEntryBuilder.discEntryBuilders,
          permissions,
          extension,
          albumEntryBuilder.maybeLastModified
        )
      }

    private def convertDiscEntries(
        initial: String,
        artist: String,
        album: String,
        discEntryBuilders: Seq[DiscEntryBuilder],
        permissions: Permissions,
        extension: Extension,
        maybeLastModified: Option[Instant]
    ): Seq[FsEntryBuilder] =
      val totalDiscs: Int = discEntryBuilders.size
      discEntryBuilders.zipWithIndex.map { case (discEntryBuilder, idx) =>
        convertDiscEntry(
          initial,
          artist,
          album,
          discEntryBuilder.albumId,
          totalDiscs,
          idx + 1,
          discEntryBuilder,
          permissions,
          extension,
          maybeLastModified
        )
      }

    private def convertDiscEntry(
        initial: String,
        artist: String,
        album: String,
        albumId: String,
        totalDiscs: Int,
        discNumber: Int,
        discEntryBuilder: DiscEntryBuilder,
        permissions: Permissions,
        extension: Extension,
        maybeLastModified: Option[Instant]
    ): FsEntryBuilder =
      val albumDirectory: String =
        if totalDiscs == 1 then album else f"$album $discNumber%02d"
      FsDirectoryBuilder(
        albumDirectory,
        permissions,
        convertTrackEntries(
          initial,
          artist,
          albumDirectory,
          album,
          albumId,
          totalDiscs,
          discNumber,
          discEntryBuilder.trackEntryBuilders,
          permissions,
          extension,
          maybeLastModified
        )
      )

    def convertTrackEntries(
        initial: String,
        artist: String,
        albumDirectory: String,
        albumTitle: String,
        albumId: String,
        totalDiscs: Int,
        discNumber: Int,
        trackEntryBuilders: Seq[TrackEntryBuilder],
        permissions: Permissions,
        extension: Extension,
        maybeLastModified: Option[Instant]
    ): Seq[FsEntryBuilder] =
      val totalTracks: Int = trackEntryBuilders.size
      trackEntryBuilders.zipWithIndex.map { case (trackEntryBuilder, idx) =>
        val trackNumber: Int = idx + 1
        val track: String = trackEntryBuilder.track
        val filename = f"$trackNumber%02d $track.${extension.extension}"
        val trackTags: Tags = tags(
          artist = artist,
          album = albumTitle,
          albumId = albumId,
          totalDiscs = totalDiscs,
          discNumber = discNumber,
          totalTracks = totalTracks,
          trackNumber = trackNumber,
          track = track
        )
        FsFileBuilder(filename, permissions, Some(trackTags), maybeLastModified)
      }

  object Dsl extends Dsl
  trait Dsl extends Builder with FS.Dsl:

    case class Users(artistsByUser: (String, Artists)*)
    case class Artists(albumsByArtist: (String, Seq[Album])*)
    case class Album(
        title: String,
        discs: Discs,
        maybeLastModified: Option[Instant]
    ):
      def at(lastModified: Instant): Album =
        copy(maybeLastModified = Some(lastModified))
      def toLowerCase: Album = copy(title = title.toLowerCase)
    object Album:
      def apply(title: String, tracks: Tracks): Album =
        Album(title, Discs(tracks), None)
      def apply(title: String, discs: Discs): Album = Album(title, discs, None)
    object Albums:
      def apply(albums: Album*): Seq[Album] = albums
    case class Discs(sameId: Boolean = false, tracks: Seq[Tracks]):
      def withSameId: Discs = Discs(sameId = true, tracks)
    object Discs:
      def apply(tracks: Tracks*): Discs = Discs(sameId = false, tracks)
    case class Tracks(titles: String*)
    implicit def tracksToSingleDisc(tracks: Tracks): Seq[Discs] = Seq(
      Discs(tracks)
    )
    sealed trait ArtistsOrEntries
    case class ArtistsOrEntries_Artists(artists: Artists)
        extends ArtistsOrEntries
    case class ArtistsOrEntries_Entries(entryBuilders: Seq[FsEntryBuilder])
        extends ArtistsOrEntries

    implicit def artistsToArtistsOrEntries(artists: Artists): ArtistsOrEntries =
      ArtistsOrEntries_Artists(artists)
    implicit def entriesToArtistsOrEntries(
        entryBuilders: Seq[FsEntryBuilder]
    ): ArtistsOrEntries =
      ArtistsOrEntries_Entries(entryBuilders)

    type Repos = Seq[RepositoryEntry.Dsl.FsEntryBuilder]
    object Repos:

      private def artistsToArtistsEntry(
          artists: Artists
      ): ArtistsEntryBuilder =
        val artistEntries: Seq[ArtistEntryBuilder] =
          artists.albumsByArtist.map { case (artist, albums) =>
            val albumEntries: Seq[AlbumEntryBuilder] = albums.map { album =>
              val discs: Discs = album.discs
              val discTracks: Seq[Tracks] = discs.tracks
              val keepSameId: Boolean = discs.sameId || discTracks.size == 1
              val albumTitle: String = album.title
              val discEntries: Seq[DiscEntryBuilder] =
                discTracks.zipWithIndex.map { case (tracks, idx) =>
                  val discNumber: Int = idx + 1
                  val albumId: String =
                    if keepSameId then albumTitle
                    else f"$albumTitle $discNumber%02d"
                  DiscEntryBuilder(
                    albumId,
                    tracks.titles.map(track =>
                      TrackEntryBuilder(track, album.maybeLastModified)
                    ),
                    album.maybeLastModified
                  )
                }
              AlbumEntryBuilder(
                albumTitle,
                discEntries,
                album.maybeLastModified
              )
            }
            ArtistEntryBuilder(artist, albumEntries)
          }
        ArtistsEntryBuilder(artistEntries)

      private def entryBuilder(
          artists: Artists,
          maybeLastModified: Option[Instant],
          permissions: Permissions,
          extensions: Extension*
      ): Seq[FsEntryBuilder] =
        toFsEntryBuilders(
          artistsToArtistsEntry(artists),
          permissions,
          extensions,
          maybeLastModified
        )

      private def entryBuilder(
          artistsOrEntries: ArtistsOrEntries,
          maybeLastModified: Option[Instant],
          permissions: Permissions,
          extensions: Extension*
      ): Seq[FsEntryBuilder] =
        artistsOrEntries match
          case ArtistsOrEntries_Entries(entries) => entries
          case ArtistsOrEntries_Artists(artists) =>
            entryBuilder(
              artists,
              maybeLastModified,
              permissions,
              extensions*
            )

      def apply(
          flac: Artists = Artists(),
          encoded: Artists = Artists(),
          staging: Map[Permissions, ArtistsOrEntries] = Map.empty
      ): Seq[FsEntryBuilder] =
        val stagingEntries: Seq[FsEntryBuilder] = staging.toSeq.flatMap:
          case (permissions, artistsOrEntries) =>
            entryBuilder(artistsOrEntries, None, permissions, FLAC)
        val repoDirectories = Seq(
          FsDirectoryBuilder(
            "flac",
            Permissions.AllReadOnly,
            entryBuilder(flac, None, Permissions.AllReadOnly, FLAC)
          ),
          FsDirectoryBuilder(
            "encoded",
            Permissions.AllReadOnly,
            entryBuilder(encoded, None, Permissions.AllReadOnly, M4A, MP3)
          ),
          FsDirectoryBuilder(
            "staging",
            Permissions.OwnerReadAndWrite,
            stagingEntries
          )
        )
        Seq(
          FsDirectoryBuilder("music", Permissions.AllReadOnly, repoDirectories)
        )

    implicit class RepositoryFileSystemExtensions(fs: JFS):
      def staging(fsEntryBuilders: (Permissions, FsEntryBuilder)*): Unit =
        val staging: Map[Permissions, ArtistsOrEntries] =
          fsEntryBuilders.toMap.view
            .mapValues(entry => ArtistsOrEntries_Entries(Seq(entry)))
            .toMap
        fs.add(Repos(staging = staging)*)
      def staging(permissions: Permissions, artists: Artists): Unit =
        fs.add(Repos(staging = Map(permissions -> artists))*)
      def flac(artists: Artists): Unit =
        fs.add(Repos(flac = artists)*)
      def encoded(artists: Artists): Unit =
        fs.add(Repos(encoded = artists)*)
