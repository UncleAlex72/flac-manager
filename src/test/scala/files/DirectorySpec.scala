package files

import files.Directory.pathOrdering
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import java.nio.file.Paths

class DirectorySpec extends AnyWordSpec with Matchers:

  "A path" should:
    "be equal to itself" in:
      val path = Paths.get("Q", "Queen")
      pathOrdering.compare(path, path) should ===(0)

    "be less than anything underneath it" in:
      pathOrdering.compare(
        Paths.get("Q", "Queen"),
        Paths.get("Q", "Queen", "Queen")
      ) should be < 0

    "be greater than anything above it" in:
      pathOrdering.compare(
        Paths.get("Q", "Queen", "Queen"),
        Paths.get("Q", "Queen")
      ) should be > 0

    "be less than a another path that has the same root up to a lesser part" in:
      pathOrdering.compare(
        Paths.get("Q", "Queen", "Queen", "01 - Keep Yourself Alive.flac"),
        Paths.get("Q", "Queen", "Queeny", "01 - Keep Yourself Alive.flac")
      ) should be < 0

    "be greater than a another path that has the same root up to a lesser part" in:
      pathOrdering.compare(
        Paths.get("Q", "Queen", "Queeny", "01 - Keep Yourself Alive.flac"),
        Paths.get("Q", "Queen", "Queen", "01 - Keep Yourself Alive.flac")
      ) should be > 0
