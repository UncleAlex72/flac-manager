/*
 * Copyright 2018 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package processor

import java.nio.file.Files

import org.apache.pekko.stream.Materializer
import commands.MultiAction.{Join, Extras}
import commands.{MultiAction, MultiDiscCommand}
import enumeratum.{Enum, EnumEntry}
import files.RepositoryEntry
import files.RepositoryEntry.Dsl._
import music.{CoverArt, Tags}
import org.scalatest
import org.scalatest.{EitherValues, OptionValues}
import play.api.libs.json.Json
import testfilesystem.FS.Permissions

import scala.collection.immutable
import scala.compat.java8.StreamConverters._
import scala.concurrent.Future

/** Created by alex on 18/11/14.
  */
class MultiDiscCommandSpec
    extends AbstractCommandSpec
    with EitherValues
    with OptionValues {

  val entries: Seq[FsEntryBuilder] = Repos(
    staging = Map(
      Permissions.OwnerReadAndWrite ->
        Artists(
          "Queen" ->
            Albums(
              Album(
                "A Night at the Opera",
                Discs(
                  Tracks("Death on Two Legs", "Lazing on a Sunday Afternoon"),
                  Tracks(
                    "I'm in Love with my Car",
                    "You're my Best Friend",
                    "39"
                  ),
                  Tracks("Death on Two Legs (Live)")
                ).withSameId
              ),
              Album(
                "The Game",
                Discs(
                  Tracks("Play the Game", "Dragon Attack"),
                  Tracks()
                ).withSameId
              ),
              Album("Jazz", Tracks("Mustapha"))
            )
        )
    )
  )

  sealed trait TestCase extends EnumEntry {
    val path: String
    val joined: Tags
    val split: Tags
  }

  object TestCase extends Enum[TestCase] {

    case class ExpectedTags(path: String, joined: Tags, split: Tags)
        extends TestCase

    def tags(
        artist: String,
        album: String,
        albumId: String,
        track: String,
        discNumber: Int,
        trackNumber: Int,
        totalDiscs: Int,
        totalTracks: Int
    ): Tags = {
      Tags(
        albumArtistSort = artist,
        albumArtist = artist,
        album = album,
        artist = artist,
        artistSort = artist,
        title = track,
        totalDiscs = totalDiscs,
        totalTracks = totalTracks,
        discNumber = discNumber,
        albumArtistId = artist,
        albumId = albumId,
        artistId = artist,
        trackId = None,
        asin = None,
        trackNumber = trackNumber,
        coverArt = CoverArt.empty,
        originalDate = None,
        date = None,
        year = None
      )
    }

    object DEATH_ON_TWO_LEGS
        extends ExpectedTags(
          "01 Death on Two Legs.flac",
          tags(
            artist = "Queen",
            album = "A Night at the Opera",
            albumId = "A Night at the Opera",
            track = "Death on Two Legs",
            discNumber = 1,
            trackNumber = 1,
            totalDiscs = 1,
            totalTracks = 6
          ),
          tags(
            artist = "Queen",
            album = "A Night at the Opera",
            albumId = "A Night at the Opera",
            track = "Death on Two Legs",
            discNumber = 1,
            trackNumber = 1,
            totalDiscs = 1,
            totalTracks = 2
          )
        )

    object LAZING_ON_A_SUNDAY_AFTERNOON
        extends ExpectedTags(
          "02 Lazing on a Sunday Afternoon.flac",
          tags(
            artist = "Queen",
            album = "A Night at the Opera",
            albumId = "A Night at the Opera",
            track = "Lazing on a Sunday Afternoon",
            discNumber = 1,
            trackNumber = 2,
            totalDiscs = 1,
            totalTracks = 6
          ),
          tags(
            artist = "Queen",
            album = "A Night at the Opera",
            albumId = "A Night at the Opera",
            track = "Lazing on a Sunday Afternoon",
            discNumber = 1,
            trackNumber = 2,
            totalDiscs = 1,
            totalTracks = 2
          )
        )

    object IM_IN_LOVE_WITH_MY_CAR
        extends ExpectedTags(
          "01 I'm in Love with my Car.flac",
          tags(
            artist = "Queen",
            album = "A Night at the Opera",
            albumId = "A Night at the Opera",
            track = "I'm in Love with my Car",
            discNumber = 1,
            trackNumber = 3,
            totalDiscs = 1,
            totalTracks = 6
          ),
          tags(
            artist = "Queen",
            album = "A Night at the Opera (Extras)",
            albumId = "A Night at the Opera_EXTRAS",
            track = "I'm in Love with my Car",
            discNumber = 1,
            trackNumber = 1,
            totalDiscs = 1,
            totalTracks = 4
          )
        )

    object YOURE_MY_BEST_FRIEND
        extends ExpectedTags(
          "02 You're my Best Friend.flac",
          tags(
            artist = "Queen",
            album = "A Night at the Opera",
            albumId = "A Night at the Opera",
            track = "You're my Best Friend",
            discNumber = 1,
            trackNumber = 4,
            totalDiscs = 1,
            totalTracks = 6
          ),
          tags(
            artist = "Queen",
            album = "A Night at the Opera (Extras)",
            albumId = "A Night at the Opera_EXTRAS",
            track = "You're my Best Friend",
            discNumber = 1,
            trackNumber = 2,
            totalDiscs = 1,
            totalTracks = 4
          )
        )

    object THIRTY_NINE
        extends ExpectedTags(
          "03 39.flac",
          tags(
            artist = "Queen",
            album = "A Night at the Opera",
            albumId = "A Night at the Opera",
            track = "39",
            discNumber = 1,
            trackNumber = 5,
            totalDiscs = 1,
            totalTracks = 6
          ),
          tags(
            artist = "Queen",
            album = "A Night at the Opera (Extras)",
            albumId = "A Night at the Opera_EXTRAS",
            track = "39",
            discNumber = 1,
            trackNumber = 3,
            totalDiscs = 1,
            totalTracks = 4
          )
        )

    object DEATH_ON_TWO_LEGS_LIVE
        extends ExpectedTags(
          "01 Death on Two Legs (Live).flac",
          tags(
            artist = "Queen",
            album = "A Night at the Opera",
            albumId = "A Night at the Opera",
            track = "Death on Two Legs (Live)",
            discNumber = 1,
            trackNumber = 6,
            totalDiscs = 1,
            totalTracks = 6
          ),
          tags(
            artist = "Queen",
            album = "A Night at the Opera (Extras)",
            albumId = "A Night at the Opera_EXTRAS",
            track = "Death on Two Legs (Live)",
            discNumber = 1,
            trackNumber = 4,
            totalDiscs = 1,
            totalTracks = 4
          )
        )

    object MUSTAPHA
        extends ExpectedTags(
          "01 Mustapha.flac",
          tags(
            artist = "Queen",
            album = "Jazz",
            albumId = "Jazz",
            track = "Mustapha",
            discNumber = 1,
            trackNumber = 1,
            totalDiscs = 1,
            totalTracks = 1
          ),
          tags(
            artist = "Queen",
            album = "Jazz",
            albumId = "Jazz",
            track = "Mustapha",
            discNumber = 1,
            trackNumber = 1,
            totalDiscs = 1,
            totalTracks = 1
          )
        )

    object PLAY_THE_GAME
        extends ExpectedTags(
          "01 Play the Game.flac",
          tags(
            artist = "Queen",
            album = "The Game",
            albumId = "The Game",
            track = "Play the Game",
            discNumber = 1,
            trackNumber = 1,
            totalDiscs = 1,
            totalTracks = 2
          ),
          tags(
            artist = "Queen",
            album = "The Game",
            albumId = "The Game",
            track = "Play the Game",
            discNumber = 1,
            trackNumber = 1,
            totalDiscs = 1,
            totalTracks = 2
          )
        )

    object DRAGON_ATTACK
        extends ExpectedTags(
          "02 Dragon Attack.flac",
          tags(
            artist = "Queen",
            album = "The Game",
            albumId = "The Game",
            track = "Dragon Attack",
            discNumber = 1,
            trackNumber = 2,
            totalDiscs = 1,
            totalTracks = 2
          ),
          tags(
            artist = "Queen",
            album = "The Game",
            albumId = "The Game",
            track = "Dragon Attack",
            discNumber = 1,
            trackNumber = 2,
            totalDiscs = 1,
            totalTracks = 2
          )
        )

    override def values: immutable.IndexedSeq[TestCase] = findValues
  }

  def executeTest(
      c: TestCommandComponents,
      multiAction: MultiAction,
      tagsExtractor: TestCase => Tags,
      expectedMessages: String*
  ): Future[scalatest.Assertion] = {
    implicit val materializer: Materializer = c.materializer
    c.jfs.add(entries: _*)
    for {
      source <- c.commandProcessor.submit(
        MultiDiscCommand(List(c.staging("Q/Queen")), Some(multiAction))
      )
      messages <- source.runFold(Seq.empty[String])(_ :+ _)
    } yield {
      val stagingFiles =
        c.repositories.staging
          .directory(c.jfs.getPath("Q", "Queen"))
          .toEither
          .toOption
          .get
          .list
      val tagsByName: Map[String, Tags] = {
        val empty: Map[String, Tags] = Map.empty
        stagingFiles.foldLeft(empty) { (acc, stagingFile) =>
          val absolutePath = stagingFile.absolutePath
          val name = absolutePath.getFileName.toString
          val json = Files.lines(absolutePath).toScala[Seq].mkString("\n")
          val tags = Json.fromJson[Tags](Json.parse(json)).get
          acc + (name -> tags)
        }
      }
      for (testCase <- TestCase.values; tags <- tagsByName.get(testCase.path)) {
        tags.copy(coverArt = CoverArt.empty) should ===(
          tagsExtractor(testCase).copy(coverArt = CoverArt.empty)
        )
      }
      messages should contain theSameElementsInOrderAs expectedMessages
    }
  }

  "Joining multi-disc albums" should {
    "create one album with all the tracks on one disc" in { c =>
      executeTest(
        c,
        Join,
        _.joined,
        "alterTags(/music/staging/Q/Queen/The Game 01/01 Play the Game.flac, totalDiscs, 2, 1)",
        "alterTags(/music/staging/Q/Queen/The Game 01/02 Dragon Attack.flac, totalDiscs, 2, 1)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 01/01 Death on Two Legs.flac, totalDiscs, 3, 1)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 01/01 Death on Two Legs.flac, totalTracks, 2, 6)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 01/02 Lazing on a Sunday Afternoon.flac, totalDiscs, 3, 1)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 01/02 Lazing on a Sunday Afternoon.flac, totalTracks, 2, 6)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 02/01 I'm in Love with my Car.flac, totalDiscs, 3, 1)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 02/01 I'm in Love with my Car.flac, totalTracks, 3, 6)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 02/01 I'm in Love with my Car.flac, discNumber, 2, 1)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 02/02 You're my Best Friend.flac, totalDiscs, 3, 1)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 02/02 You're my Best Friend.flac, totalTracks, 3, 6)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 02/02 You're my Best Friend.flac, discNumber, 2, 1)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 02/03 39.flac, totalDiscs, 3, 1)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 02/03 39.flac, totalTracks, 3, 6)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 02/03 39.flac, discNumber, 2, 1)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 03/01 Death on Two Legs (Live).flac, totalDiscs, 3, 1)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 03/01 Death on Two Legs (Live).flac, totalTracks, 1, 6)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 03/01 Death on Two Legs (Live).flac, discNumber, 3, 1)",
        "done()"
      )
    }
  }

  "Splitting multi-disc albums" should {
    "Create two albums" in { c =>
      executeTest(
        c,
        Extras,
        _.split,
        "alterTags(/music/staging/Q/Queen/The Game 01/01 Play the Game.flac, totalDiscs, 2, 1)",
        "alterTags(/music/staging/Q/Queen/The Game 01/02 Dragon Attack.flac, totalDiscs, 2, 1)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 01/01 Death on Two Legs.flac, totalDiscs, 3, 1)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 01/02 Lazing on a Sunday Afternoon.flac, totalDiscs, 3, 1)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 02/01 I'm in Love with my Car.flac, album, A Night at the Opera, A Night at the Opera (Extras))",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 02/01 I'm in Love with my Car.flac, totalDiscs, 3, 1)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 02/01 I'm in Love with my Car.flac, totalTracks, 3, 4)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 02/01 I'm in Love with my Car.flac, discNumber, 2, 1)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 02/01 I'm in Love with my Car.flac, albumId, A Night at the Opera, A Night at the Opera_EXTRAS)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 02/02 You're my Best Friend.flac, album, A Night at the Opera, A Night at the Opera (Extras))",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 02/02 You're my Best Friend.flac, totalDiscs, 3, 1)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 02/02 You're my Best Friend.flac, totalTracks, 3, 4)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 02/02 You're my Best Friend.flac, discNumber, 2, 1)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 02/02 You're my Best Friend.flac, albumId, A Night at the Opera, A Night at the Opera_EXTRAS)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 02/03 39.flac, album, A Night at the Opera, A Night at the Opera (Extras))",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 02/03 39.flac, totalDiscs, 3, 1)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 02/03 39.flac, totalTracks, 3, 4)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 02/03 39.flac, discNumber, 2, 1)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 02/03 39.flac, albumId, A Night at the Opera, A Night at the Opera_EXTRAS)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 03/01 Death on Two Legs (Live).flac, album, A Night at the Opera, A Night at the Opera (Extras))",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 03/01 Death on Two Legs (Live).flac, totalDiscs, 3, 1)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 03/01 Death on Two Legs (Live).flac, totalTracks, 1, 4)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 03/01 Death on Two Legs (Live).flac, discNumber, 3, 1)",
        "alterTags(/music/staging/Q/Queen/A Night at the Opera 03/01 Death on Two Legs (Live).flac, albumId, A Night at the Opera, A Night at the Opera_EXTRAS)",
        "done()"
      )

    }
  }
}
