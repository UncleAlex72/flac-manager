/*
 * Copyright 2018 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package processor

import org.apache.pekko.stream.Materializer
import changes.LabelledTree._
import commands.CheckoutCommand
import devices.Extension.{M4A, MP3}
import files.RepositoryEntry.Dsl._
import org.scalatest.{EitherValues, OptionValues}
import testfilesystem.FS.Permissions

import scala.collection.SortedSet

/** Created by alex on 18/11/14.
  */
class CheckoutCommandSpec
    extends AbstractCommandSpec
    with EitherValues
    with OptionValues {

  val A_KIND_OF_MAGIC: Album = Album(
    "A Kind of Magic",
    Tracks("One Vision", "A Kind of Magic", "One Year of Love")
  )

  val INNUENDO: Album = Album(
    "Innuendo",
    Tracks("Innuendo", "Im Going Slightly Mad", "Headlong")
  )

  val SOUTH_OF_HEAVEN: Album = Album(
    "South of Heaven",
    Tracks("South of Heaven", "Silent Scream", "Live Undead")
  )

  val entriesBeforeCheckout = Repos(
    flac = Artists(
      "Queen" -> Albums(A_KIND_OF_MAGIC, INNUENDO),
      "Slayer" -> Albums(SOUTH_OF_HEAVEN)
    ),
    encoded = Artists(
      "Queen" -> Albums(A_KIND_OF_MAGIC, INNUENDO),
      "Slayer" -> Albums(SOUTH_OF_HEAVEN)
    )
  )

  val expectedEntriesAfterCheckout = Repos(
    flac = Artists(
      "Queen" -> Seq(A_KIND_OF_MAGIC),
      "Slayer" -> Albums(SOUTH_OF_HEAVEN)
    ),
    staging = Map(
      Permissions.OwnerWriteAllRead -> Artists("Queen" -> Albums(INNUENDO))
    ),
    encoded = Artists(
      "Queen" -> Albums(A_KIND_OF_MAGIC),
      "Slayer" -> Albums(SOUTH_OF_HEAVEN)
    )
  )

  val conflictingEntries = Repos(
    flac = Artists(
      "Queen" -> Albums(A_KIND_OF_MAGIC, INNUENDO),
      "Slayer" -> Albums(SOUTH_OF_HEAVEN)
    ),
    staging = Map(
      Permissions.OwnerWriteAllRead -> Artists("Queen" -> Albums(INNUENDO))
    ),
    encoded = Artists(
      "Queen" -> Albums(A_KIND_OF_MAGIC, INNUENDO),
      "Slayer" -> Albums(SOUTH_OF_HEAVEN)
    )
  )

  "Checking out but not unowning an album" should {
    "remove the album but keep it owned" in { c =>
      implicit val materializer: Materializer = c.materializer
      val fs = c.jfs
      fs.add(entriesBeforeCheckout: _*)
      for {
        _ <- c.users(user("brian"), user("freddie"))
        _ <- c.devices(
          device("brian", MP3, "brian mp3"),
          device("freddie", M4A, "freddie m4a")
        )
        _ <- c.addAlbums(
          "Queen" -> Seq(A_KIND_OF_MAGIC, INNUENDO),
          "Slayer" -> Seq(SOUTH_OF_HEAVEN)
        )
        _ <- c.ownAlbums("brian", "Queen", A_KIND_OF_MAGIC, INNUENDO)
        _ <- c.ownAlbums("freddie", "Queen", A_KIND_OF_MAGIC)
        _ <- c.ownAlbums("freddie", "Slayer", SOUTH_OF_HEAVEN)
        source <- c.commandProcessor.submit(
          CheckoutCommand(List(c.flac("Q/Queen/Innuendo")))
        )
        messages <- source.runFold(Seq.empty[String])(_ :+ _)
        briansTree <- c.fileSystemTreeCache.latest("brian mp3")
        freddiesTree <- c.fileSystemTreeCache.latest("freddie m4a")
        owners <- c.ownerService.listOwners()
      } yield {
        messages should contain theSameElementsInOrderAs Seq(
          "checkout(/music/flac/Q/Queen/Innuendo/01 Innuendo.flac)",
          "move(/music/flac/Q/Queen/Innuendo/01 Innuendo.flac, /music/staging/Q/Queen/Innuendo/01 Innuendo.flac)",
          "changeFileDeleted(Q, Queen, Innuendo, 01 Innuendo.mp3)",
          "changeFileDeleted(Q, Queen, Innuendo, 01 Innuendo.m4a)",
          "delete(/music/encoded/mp3/Q/Queen/Innuendo/01 Innuendo.mp3)",
          "delete(/music/encoded/m4a/Q/Queen/Innuendo/01 Innuendo.m4a)",
          "checkout(/music/flac/Q/Queen/Innuendo/02 Im Going Slightly Mad.flac)",
          "move(/music/flac/Q/Queen/Innuendo/02 Im Going Slightly Mad.flac, /music/staging/Q/Queen/Innuendo/02 Im Going Slightly Mad.flac)",
          "changeFileDeleted(Q, Queen, Innuendo, 02 Im Going Slightly Mad.mp3)",
          "changeFileDeleted(Q, Queen, Innuendo, 02 Im Going Slightly Mad.m4a)",
          "delete(/music/encoded/mp3/Q/Queen/Innuendo/02 Im Going Slightly Mad.mp3)",
          "delete(/music/encoded/m4a/Q/Queen/Innuendo/02 Im Going Slightly Mad.m4a)",
          "checkout(/music/flac/Q/Queen/Innuendo/03 Headlong.flac)",
          "move(/music/flac/Q/Queen/Innuendo/03 Headlong.flac, /music/staging/Q/Queen/Innuendo/03 Headlong.flac)",
          "changeFileDeleted(Q, Queen, Innuendo, 03 Headlong.mp3)",
          "changeFileDeleted(Q, Queen, Innuendo, 03 Headlong.m4a)",
          "delete(/music/encoded/mp3/Q/Queen/Innuendo/03 Headlong.mp3)",
          "delete(/music/encoded/m4a/Q/Queen/Innuendo/03 Headlong.m4a)",
          "cachesRefreshed()",
          "done()"
        )
        owners.toSeq.sortBy(_._1) should contain theSameElementsAs Seq(
          "Q/Queen/A Kind of Magic/01 One Vision" -> SortedSet(
            "brian",
            "freddie"
          ),
          "Q/Queen/A Kind of Magic/02 A Kind of Magic" -> SortedSet(
            "brian",
            "freddie"
          ),
          "Q/Queen/A Kind of Magic/03 One Year of Love" -> SortedSet(
            "brian",
            "freddie"
          ),
          "Q/Queen/Innuendo/01 Innuendo" -> SortedSet("brian"),
          "Q/Queen/Innuendo/02 Im Going Slightly Mad" -> SortedSet("brian"),
          "Q/Queen/Innuendo/03 Headlong" -> SortedSet("brian"),
          "S/Slayer/South of Heaven/01 South of Heaven" -> SortedSet("freddie"),
          "S/Slayer/South of Heaven/02 Silent Scream" -> SortedSet("freddie"),
          "S/Slayer/South of Heaven/03 Live Undead" -> SortedSet("freddie")
        )

        briansTree.value
          .toPaths()
          .map(_._1)
          .flatMap(_.lastOption) should contain theSameElementsAs Seq(
          "Q",
          "Queen",
          "A Kind of Magic",
          "01 One Vision.mp3",
          "02 A Kind of Magic.mp3",
          "03 One Year of Love.mp3"
        )
        freddiesTree.value
          .toPaths()
          .map(_._1)
          .flatMap(_.lastOption) should contain theSameElementsAs Seq(
          "Q",
          "Queen",
          "A Kind of Magic",
          "01 One Vision.m4a",
          "02 A Kind of Magic.m4a",
          "03 One Year of Love.m4a",
          "S",
          "Slayer",
          "South of Heaven",
          "01 South of Heaven.m4a",
          "02 Silent Scream.m4a",
          "03 Live Undead.m4a"
        )
        val actualEntries = fs.entries
        val expectedEntries = fs.expected(expectedEntriesAfterCheckout: _*)
        actualEntries should haveTheSameEntriesAs(expectedEntries)
      }
    }
  }

  "Checking out and unowning an album" should {
    "remove the album and unown it" in { c =>
      implicit val materializer: Materializer = c.materializer
      val fs = c.jfs
      fs.add(entriesBeforeCheckout: _*)
      for {
        _ <- c.users(user("brian"), user("freddie"))
        _ <- c.devices(
          device("brian", MP3, "brian mp3"),
          device("freddie", M4A, "freddie m4a")
        )
        _ <- c.addAlbums(
          "Queen" -> Seq(A_KIND_OF_MAGIC, INNUENDO),
          "Slayer" -> Seq(SOUTH_OF_HEAVEN)
        )
        _ <- c.ownAlbums("brian", "Queen", A_KIND_OF_MAGIC, INNUENDO)
        _ <- c.ownAlbums("freddie", "Queen", A_KIND_OF_MAGIC)
        _ <- c.ownAlbums("freddie", "Slayer", SOUTH_OF_HEAVEN)
        source <- c.commandProcessor.submit(
          CheckoutCommand(List(c.flac("Q/Queen/Innuendo")))
        )
        messages <- source.runFold(Seq.empty[String])(_ :+ _)
        briansTree <- c.fileSystemTreeCache.latest("brian mp3")
        freddiesTree <- c.fileSystemTreeCache.latest("freddie m4a")
        owners <- c.ownerService.listOwners()
      } yield {
        messages should contain theSameElementsInOrderAs Seq(
          "checkout(/music/flac/Q/Queen/Innuendo/01 Innuendo.flac)",
          "move(/music/flac/Q/Queen/Innuendo/01 Innuendo.flac, /music/staging/Q/Queen/Innuendo/01 Innuendo.flac)",
          "changeFileDeletedAndUnowned(Q, Queen, Innuendo, 01 Innuendo.mp3)",
          "changeFileDeletedAndUnowned(Q, Queen, Innuendo, 01 Innuendo.m4a)",
          "delete(/music/encoded/mp3/Q/Queen/Innuendo/01 Innuendo.mp3)",
          "delete(/music/encoded/m4a/Q/Queen/Innuendo/01 Innuendo.m4a)",
          "checkout(/music/flac/Q/Queen/Innuendo/02 Im Going Slightly Mad.flac)",
          "move(/music/flac/Q/Queen/Innuendo/02 Im Going Slightly Mad.flac, /music/staging/Q/Queen/Innuendo/02 Im Going Slightly Mad.flac)",
          "changeFileDeletedAndUnowned(Q, Queen, Innuendo, 02 Im Going Slightly Mad.mp3)",
          "changeFileDeletedAndUnowned(Q, Queen, Innuendo, 02 Im Going Slightly Mad.m4a)",
          "delete(/music/encoded/mp3/Q/Queen/Innuendo/02 Im Going Slightly Mad.mp3)",
          "delete(/music/encoded/m4a/Q/Queen/Innuendo/02 Im Going Slightly Mad.m4a)",
          "checkout(/music/flac/Q/Queen/Innuendo/03 Headlong.flac)",
          "move(/music/flac/Q/Queen/Innuendo/03 Headlong.flac, /music/staging/Q/Queen/Innuendo/03 Headlong.flac)",
          "changeFileDeletedAndUnowned(Q, Queen, Innuendo, 03 Headlong.mp3)",
          "changeFileDeletedAndUnowned(Q, Queen, Innuendo, 03 Headlong.m4a)",
          "delete(/music/encoded/mp3/Q/Queen/Innuendo/03 Headlong.mp3)",
          "delete(/music/encoded/m4a/Q/Queen/Innuendo/03 Headlong.m4a)",
          "cachesRefreshed()",
          "done()"
        )
        owners.toSeq.sortBy(_._1) should contain theSameElementsAs Seq(
          "Q/Queen/A Kind of Magic/01 One Vision" -> SortedSet(
            "brian",
            "freddie"
          ),
          "Q/Queen/A Kind of Magic/02 A Kind of Magic" -> SortedSet(
            "brian",
            "freddie"
          ),
          "Q/Queen/A Kind of Magic/03 One Year of Love" -> SortedSet(
            "brian",
            "freddie"
          ),
          "S/Slayer/South of Heaven/01 South of Heaven" -> SortedSet("freddie"),
          "S/Slayer/South of Heaven/02 Silent Scream" -> SortedSet("freddie"),
          "S/Slayer/South of Heaven/03 Live Undead" -> SortedSet("freddie")
        )

        briansTree.value
          .toPaths()
          .map(_._1)
          .flatMap(_.lastOption) should contain theSameElementsAs Seq(
          "Q",
          "Queen",
          "A Kind of Magic",
          "01 One Vision.mp3",
          "02 A Kind of Magic.mp3",
          "03 One Year of Love.mp3"
        )
        freddiesTree.value
          .toPaths()
          .map(_._1)
          .flatMap(_.lastOption) should contain theSameElementsAs Seq(
          "Q",
          "Queen",
          "A Kind of Magic",
          "01 One Vision.m4a",
          "02 A Kind of Magic.m4a",
          "03 One Year of Love.m4a",
          "S",
          "Slayer",
          "South of Heaven",
          "01 South of Heaven.m4a",
          "02 Silent Scream.m4a",
          "03 Live Undead.m4a"
        )
        val actualEntries = fs.entries
        val expectedEntries = fs.expected(expectedEntriesAfterCheckout: _*)
        actualEntries should haveTheSameEntriesAs(expectedEntries)
      }
    }
  }

  "Trying to checkout files that would overwrite files in the staging directory" should {
    "fail and report which files would be overwritten" in { c =>
      implicit val materializer: Materializer = c.materializer
      val fs = c.jfs
      fs.add(conflictingEntries: _*)
      for {
        _ <- c.users(user("brian"), user("freddie"))
        _ <- c.devices(
          device("brian", MP3, "brian mp3"),
          device("freddie", M4A, "freddie m4a")
        )
        _ <- c.addAlbums(
          "Queen" -> Seq(A_KIND_OF_MAGIC, INNUENDO),
          "Slayer" -> Seq(SOUTH_OF_HEAVEN)
        )
        _ <- c.ownAlbums("brian", "Queen", A_KIND_OF_MAGIC, INNUENDO)
        _ <- c.ownAlbums("freddie", "Queen", A_KIND_OF_MAGIC)
        _ <- c.ownAlbums("freddie", "Slayer", SOUTH_OF_HEAVEN)
        _ <- c.cacheMediator.refreshCaches()
        source <- c.commandProcessor.submit(
          CheckoutCommand(List(c.flac("Q/Queen/Innuendo")))
        )
        messages <- source.runFold(Seq.empty[String])(_ :+ _)
        briansTree <- c.fileSystemTreeCache.latest("brian mp3")
        freddiesTree <- c.fileSystemTreeCache.latest("freddie m4a")
        owners <- c.ownerService.listOwners()
      } yield {
        messages should contain theSameElementsInOrderAs List(
          "overwrite(/music/flac/Q/Queen/Innuendo/01 Innuendo.flac, /music/staging/Q/Queen/Innuendo/01 Innuendo.flac)",
          "overwrite(/music/flac/Q/Queen/Innuendo/02 Im Going Slightly Mad.flac, /music/staging/Q/Queen/Innuendo/02 Im Going Slightly Mad.flac)",
          "overwrite(/music/flac/Q/Queen/Innuendo/03 Headlong.flac, /music/staging/Q/Queen/Innuendo/03 Headlong.flac)",
          "done()"
        )
        owners.toSeq.sortBy(_._1) should contain theSameElementsAs Seq(
          "Q/Queen/A Kind of Magic/01 One Vision" -> SortedSet(
            "brian",
            "freddie"
          ),
          "Q/Queen/A Kind of Magic/02 A Kind of Magic" -> SortedSet(
            "brian",
            "freddie"
          ),
          "Q/Queen/A Kind of Magic/03 One Year of Love" -> SortedSet(
            "brian",
            "freddie"
          ),
          "Q/Queen/Innuendo/01 Innuendo" -> SortedSet("brian"),
          "Q/Queen/Innuendo/02 Im Going Slightly Mad" -> SortedSet("brian"),
          "Q/Queen/Innuendo/03 Headlong" -> SortedSet("brian"),
          "S/Slayer/South of Heaven/01 South of Heaven" -> SortedSet("freddie"),
          "S/Slayer/South of Heaven/02 Silent Scream" -> SortedSet("freddie"),
          "S/Slayer/South of Heaven/03 Live Undead" -> SortedSet("freddie")
        )

        briansTree.value
          .toPaths()
          .map(_._1)
          .flatMap(_.lastOption) should contain theSameElementsAs Seq(
          "Q",
          "Queen",
          "A Kind of Magic",
          "01 One Vision.mp3",
          "02 A Kind of Magic.mp3",
          "03 One Year of Love.mp3",
          "Innuendo",
          "01 Innuendo.mp3",
          "02 Im Going Slightly Mad.mp3",
          "03 Headlong.mp3"
        )
        freddiesTree.value
          .toPaths()
          .map(_._1)
          .flatMap(_.lastOption) should contain theSameElementsAs Seq(
          "Q",
          "Queen",
          "A Kind of Magic",
          "01 One Vision.m4a",
          "02 A Kind of Magic.m4a",
          "03 One Year of Love.m4a",
          "S",
          "Slayer",
          "South of Heaven",
          "01 South of Heaven.m4a",
          "02 Silent Scream.m4a",
          "03 Live Undead.m4a"
        )
        val actualEntries = fs.entries
        val expectedEntries = fs.expected(conflictingEntries: _*)
        actualEntries should haveTheSameEntriesAs(expectedEntries)
      }
    }
  }
}
