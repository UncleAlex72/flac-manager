package processor

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.Source
import org.apache.pekko.stream.{ActorMaterializer, Materializer}
import cats.syntax.validated._
import com.typesafe.config.ConfigFactory
import commands._
import devices.Extension.MP3
import devices.NewDevice
import files.{FlacFile, StagingFile}
import logging.Messages
import music.Tags
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.{FutureOutcome, Matchers, fixture}
import validation.Validations

import scala.collection.immutable.Seq
import scala.concurrent.Future

class CommandProcessorImplSpec
    extends fixture.AsyncWordSpec
    with Matchers
    with MockitoSugar {

  "Running a command" should {
    "produce all messages from the parser and the executor" in { f =>
      for {
        messages <- f.collect(OwnCommand())
      } yield {
        messages should contain theSameElementsInOrderAs Seq(
          "lookingForDevices",
          "multiActionRequired",
          "done"
        )
      }
    }
  }

  "Running two commands" should {
    "be able to capture the logs for both independently and together" in { f =>
      val eventuallyAllLogs = f.collectAll()
      val eventuallyCheckinMessages = f.collect(CheckinCommand())
      val eventuallyCheckoutMessages = f.collect(CheckoutCommand())
      for {
        checkinMessages <- eventuallyCheckinMessages
        checkoutMessages <- eventuallyCheckoutMessages
        _ <- f.shutdown()
        allLogs <- eventuallyAllLogs
      } yield {
        checkinMessages should contain theSameElementsInOrderAs Seq(
          "lookingForDevices",
          "done"
        )
        checkoutMessages should contain theSameElementsInOrderAs Seq(
          "multiActionRequired",
          "done"
        )
        allLogs should contain theSameElementsInOrderAs Seq(
          "1" -> "lookingForDevices",
          "1" -> "done",
          "2" -> "multiActionRequired",
          "2" -> "done"
        )
      }
    }
  }

  "Running a command that fails validation" should {
    "return only the error messages" in { f =>
      for {
        messages <- f.collect(InitialiseCommand(Map.empty))
      } yield {
        messages should contain theSameElementsInOrderAs Seq(
          "exception",
          "done"
        )
      }
    }
  }

  "Running a command who's actions throw an exception" should {
    "complete with the error" in { f =>
      for {
        messages <- f.collect(AddDeviceCommand("1", "1", "mp3"))
      } yield {
        messages should contain theSameElementsInOrderAs Seq(
          "exception",
          "done"
        )
      }
    }
  }

  override def withFixture(test: OneArgAsyncTest): FutureOutcome = {
    implicit val actorSystem: ActorSystem =
      ActorSystem.create("test", ConfigFactory.defaultReference())
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    val stagingFile: StagingFile = mock[StagingFile]
    val flacFile: FlacFile = mock[FlacFile]
    val commandParser = new CommandParser {
      override def apply(
          command: Command
      ): Future[ValidatedNel[String, Seq[Action]]] = command match {
        case _: CheckinCommand =>
          Future.successful(
            Seq(CheckinFileAction(stagingFile, flacFile, mock[Tags])).validNel
          )
        case _: CheckoutCommand =>
          Future.successful(
            Seq(CheckoutFileAction(flacFile, stagingFile)).validNel
          )
        case _: OwnCommand =>
          Future.successful(
            Seq(
              CheckinFileAction(stagingFile, flacFile, mock[Tags]),
              CheckoutFileAction(flacFile, stagingFile)
            ).validNel
          )
        case _: AddDeviceAction =>
          Future.successful(
            Seq(AddDeviceAction(NewDevice("1", "1", MP3))).validNel
          )
        case _ =>
          Future.successful(Messages.error("").invalidNel)
      }
    }
    val messagePrinter: MessagePrinter = (message: Message) => message.key
    val actionExecutor: ActionExecutor = {
      case _: CheckinFileAction  => Source.single(Messages.lookingForDevices())
      case _: CheckoutFileAction => Source.single(Messages.multiActionRequired())
      case _ =>
        Source.fromIterator(() =>
          throw new IllegalStateException("Unknown command")
        )
    }
    val jobIdGenerator: JobIdGenerator = new JobIdGenerator {
      var counter = 0
      override def generate(): String = {
        counter += 1
        counter.toString
      }
    }
    val commandProcessor = new CommandProcessorImpl(
      commandParser,
      actionExecutor,
      messagePrinter,
      jobIdGenerator
    )
    val shutdown: () => Future[Unit] = () =>
      Future.successful(commandProcessor.shutdown())
    val param: FixtureParam =
      FixtureParam_(commandProcessor, shutdown)
    withFixture(test.toNoArgAsyncTest(param)).onCompletedThen { _ =>
      actorSystem.terminate()
    }
  }

  case class FixtureParam_(
      commandProcessor: CommandProcessor,
      shutdown: () => Future[Unit]
  )(implicit val materializer: Materializer) {
    def collect(c: Command): Future[Seq[String]] = {
      for {
        source <- commandProcessor.submit(c)
        messages <- source.runFold[Seq[String]](Seq.empty)(_ :+ _)
      } yield {
        messages
      }
    }

    def collectAll(): Future[Seq[(String, String)]] =
      commandProcessor.all().runFold[Seq[(String, String)]](Seq.empty)(_ :+ _)
  }

  override type FixtureParam = FixtureParam_
}
