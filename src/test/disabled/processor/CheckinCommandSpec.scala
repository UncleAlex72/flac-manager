/*
 * Copyright 2018 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package processor

import org.apache.pekko.stream.Materializer
import commands.CheckinCommand
import devices.Extension.{M4A, MP3}
import org.scalatest.{EitherValues, OptionValues}
import testfilesystem.FS.Permissions
import files.RepositoryEntry.Dsl._
import changes.LabelledTree._
import files.RepositoryEntry

/** Created by alex on 18/11/14.
  */
class CheckinCommandSpec
    extends AbstractCommandSpec
    with EitherValues
    with OptionValues {

  val A_KIND_OF_MAGIC: Album = Album(
    "A Kind of Magic",
    Tracks("One Vision", "A Kind of Magic", "One Year of Love")
  )

  val INNUENDO: Album = Album(
    "Innuendo",
    Tracks("Innuendo", "Im Going Slightly Mad", "Headlong")
  )

  val SOUTH_OF_HEAVEN: Album = Album(
    "South of Heaven",
    Tracks("South of Heaven", "Silent Scream", "Live Undead")
  )

  val entriesBeforeCheckin: Repos = Repos(
    staging = Map(
      Permissions.OwnerReadAndWrite ->
        Artists(
          "Queen" -> Albums(A_KIND_OF_MAGIC, INNUENDO),
          "Slayer" -> Albums(SOUTH_OF_HEAVEN)
        )
    )
  )

  val flacEntries: Repos = Repos(
    flac = Artists("Queen" -> Seq(A_KIND_OF_MAGIC)),
    encoded = Artists("Queen" -> Albums(A_KIND_OF_MAGIC))
  )

  val lowerCaseFlacEntries: Repos = Repos(
    flac = Artists("Queen" -> Seq(A_KIND_OF_MAGIC.toLowerCase)),
    encoded = Artists("Queen" -> Albums(A_KIND_OF_MAGIC.toLowerCase))
  )

  val expectedEntriesAfterCheckin: Repos = Repos(
    flac = Artists(
      "Queen" -> Seq(A_KIND_OF_MAGIC, INNUENDO),
      "Slayer" -> Albums(SOUTH_OF_HEAVEN)
    ),
    encoded = Artists(
      "Queen" -> Albums(A_KIND_OF_MAGIC, INNUENDO),
      "Slayer" -> Albums(SOUTH_OF_HEAVEN)
    )
  )

  val expectedEntriesAfterUnownedCheckin: Repos = Repos(
    flac = Artists(
      "Queen" -> Seq(A_KIND_OF_MAGIC, INNUENDO),
      "Slayer" -> Albums(SOUTH_OF_HEAVEN)
    ),
    encoded = Artists(
      "Queen" -> Albums(A_KIND_OF_MAGIC, INNUENDO),
      "Slayer" -> Albums(SOUTH_OF_HEAVEN)
    )
  )

  val expectedEntriesAfterOwningCheckin: Repos = Repos(
    flac = Artists(
      "Queen" -> Seq(A_KIND_OF_MAGIC, INNUENDO),
      "Slayer" -> Albums(SOUTH_OF_HEAVEN)
    ),
    encoded = Artists(
      "Queen" -> Albums(A_KIND_OF_MAGIC, INNUENDO),
      "Slayer" -> Albums(SOUTH_OF_HEAVEN)
    )
  )

  val expectedEntriesAfterOverwriteCheckin: Repos = Repos(
    staging = Map(
      Permissions.OwnerReadAndWrite ->
        Artists(
          "Queen" -> Albums(A_KIND_OF_MAGIC, INNUENDO),
          "Slayer" -> Albums(SOUTH_OF_HEAVEN)
        )
    ),
    flac = Artists("Queen" -> Seq(A_KIND_OF_MAGIC.toLowerCase)),
    encoded = Artists("Queen" -> Albums(A_KIND_OF_MAGIC.toLowerCase))
  )

  "Checking in albums" should {
    "encode the albums and create links in the devices repository" in { c =>
      implicit val materializer: Materializer = c.materializer
      val fs: JFS = c.jfs
      fs.add(entriesBeforeCheckin: _*)
      for {
        _ <- c.users(user("brian"), user("freddie"))
        _ <- c.devices(
          device("brian", MP3, "brianmp3"),
          device("freddie", M4A, "freddiem4a")
        )
        _ <- c.ownAlbums("brian", "Queen", A_KIND_OF_MAGIC, INNUENDO)
        _ <- c.ownAlbums("freddie", "Queen", A_KIND_OF_MAGIC)
        _ <- c.ownAlbums("freddie", "Slayer", SOUTH_OF_HEAVEN)
        _ <- c.cacheMediator.refreshCaches()
        source <- c.commandProcessor.submit(CheckinCommand(List(c.staging(""))))
        _ <- source.runFold(Seq.empty[String])(_ :+ _)
        briansTree <- c.fileSystemTreeCache.latest("brianmp3")
        freddiesTree <- c.fileSystemTreeCache.latest("freddiem4a")
      } yield {
        briansTree.value
          .toPaths()
          .map(_._1)
          .flatMap(_.lastOption) should contain theSameElementsAs Seq(
          "Q",
          "Queen",
          "A Kind of Magic",
          "01 One Vision.mp3",
          "02 A Kind of Magic.mp3",
          "03 One Year of Love.mp3",
          "Innuendo",
          "01 Innuendo.mp3",
          "02 Im Going Slightly Mad.mp3",
          "03 Headlong.mp3"
        )
        freddiesTree.value
          .toPaths()
          .map(_._1)
          .flatMap(_.lastOption) should contain theSameElementsAs Seq(
          "Q",
          "Queen",
          "A Kind of Magic",
          "01 One Vision.m4a",
          "02 A Kind of Magic.m4a",
          "03 One Year of Love.m4a",
          "S",
          "Slayer",
          "South of Heaven",
          "01 South of Heaven.m4a",
          "02 Silent Scream.m4a",
          "03 Live Undead.m4a"
        )
        fs.entries should haveTheSameEntriesAs(
          fs.expected(expectedEntriesAfterCheckin: _*)
        )
      }
    }
    "not allow unowned albums if so desired" in { c =>
      implicit val materializer: Materializer = c.materializer
      val fs: JFS = c.jfs
      fs.add(entriesBeforeCheckin: _*)
      for {
        _ <- c.users(user("brian"), user("freddie"))
        _ <- c.devices(
          device("brian", MP3, "brianmp3"),
          device("freddie", M4A, "freddiem4a")
        )
        _ <- c.ownAlbums("brian", "Queen", A_KIND_OF_MAGIC)
        _ <- c.ownAlbums("freddie", "Queen", A_KIND_OF_MAGIC)
        _ <- c.ownAlbums("freddie", "Slayer", SOUTH_OF_HEAVEN)
        _ <- c.cacheMediator.refreshCaches()
        source <- c.commandProcessor.submit(CheckinCommand(List(c.staging(""))))
        messages <- source.runFold(Seq.empty[String])(_ :+ _)
        briansTree <- c.fileSystemTreeCache.latest("brianmp3")
        freddiesTree <- c.fileSystemTreeCache.latest("freddiem4a")
      } yield {
        briansTree.value.toPaths().map(_._1).flatMap(_.lastOption) should be(
          empty
        )
        freddiesTree.value.toPaths().map(_._1).flatMap(_.lastOption) should be(
          empty
        )
        fs.entries should haveTheSameEntriesAs(
          fs.expected(entriesBeforeCheckin: _*)
        )
        messages should contain theSameElementsInOrderAs Seq(
          "notOwned(/music/staging/Q/Queen/Innuendo/01 Innuendo.flac)",
          "notOwned(/music/staging/Q/Queen/Innuendo/02 Im Going Slightly Mad.flac)",
          "notOwned(/music/staging/Q/Queen/Innuendo/03 Headlong.flac)",
          "done()"
        )
      }
    }
    "allow unowned albums if so desired" in { c =>
      implicit val materializer: Materializer = c.materializer
      val fs: JFS = c.jfs
      fs.add(entriesBeforeCheckin: _*)
      for {
        _ <- c.users(user("brian"), user("freddie"))
        _ <- c.devices(
          device("brian", MP3, "brianmp3"),
          device("freddie", M4A, "freddiem4a")
        )
        _ <- c.ownAlbums("brian", "Queen", A_KIND_OF_MAGIC)
        _ <- c.ownAlbums("freddie", "Queen", A_KIND_OF_MAGIC)
        _ <- c.ownAlbums("freddie", "Slayer", SOUTH_OF_HEAVEN)
        _ <- c.cacheMediator.refreshCaches()
        source <- c.commandProcessor.submit(
          CheckinCommand(List(c.staging("")), allowUnowned = true)
        )
        _ <- source.runFold(Seq.empty[String])(_ :+ _)
        briansTree <- c.fileSystemTreeCache.latest("brianmp3")
        freddiesTree <- c.fileSystemTreeCache.latest("freddiem4a")
      } yield {
        briansTree.value
          .toPaths()
          .map(_._1)
          .flatMap(_.lastOption) should contain theSameElementsAs Seq(
          "Q",
          "Queen",
          "A Kind of Magic",
          "01 One Vision.mp3",
          "02 A Kind of Magic.mp3",
          "03 One Year of Love.mp3"
        )
        freddiesTree.value
          .toPaths()
          .map(_._1)
          .flatMap(_.lastOption) should contain theSameElementsAs Seq(
          "Q",
          "Queen",
          "A Kind of Magic",
          "01 One Vision.m4a",
          "02 A Kind of Magic.m4a",
          "03 One Year of Love.m4a",
          "S",
          "Slayer",
          "South of Heaven",
          "01 South of Heaven.m4a",
          "02 Silent Scream.m4a",
          "03 Live Undead.m4a"
        )
        fs.entries should haveTheSameEntriesAs(
          fs.expected(expectedEntriesAfterUnownedCheckin: _*)
        )
      }
    }
    "add owners to albums whilst checking in" in { c =>
      implicit val materializer: Materializer = c.materializer
      val fs: JFS = c.jfs
      fs.add(entriesBeforeCheckin: _*)
      for {
        _ <- c.users(user("brian"), user("freddie"))
        _ <- c.devices(
          device("brian", MP3, "brianmp3"),
          device("freddie", M4A, "freddiem4a")
        )
        _ <- c.ownAlbums("brian", "Queen", A_KIND_OF_MAGIC)
        _ <- c.cacheMediator.refreshCaches()
        source <- c.commandProcessor.submit(
          CheckinCommand(List(c.staging("")), owners = Seq("brian", "freddie"))
        )
        messages <- source.runFold(Seq.empty[String])(_ :+ _)
        briansTree <- c.fileSystemTreeCache.latest("brianmp3")
        freddiesTree <- c.fileSystemTreeCache.latest("freddiem4a")
      } yield {
        messages should contain theSameElementsInOrderAs Seq(
          "changeOwnersAdded(Q, Queen, A Kind of Magic, 01 One Vision.mp3, freddie)",
          "changeOwnersAdded(Q, Queen, A Kind of Magic, 01 One Vision.m4a, freddie)",
          "checkin(/music/staging/Q/Queen/A Kind of Magic/01 One Vision.flac)",
          "encode(/music/staging/Q/Queen/A Kind of Magic/01 One Vision.flac, /music/encoded/mp3/Q/Queen/A Kind of Magic/01 One Vision.mp3)",
          "changeFileAdded(Q, Queen, A Kind of Magic, 01 One Vision.mp3)",
          "encode(/music/staging/Q/Queen/A Kind of Magic/01 One Vision.flac, /music/encoded/m4a/Q/Queen/A Kind of Magic/01 One Vision.m4a)",
          "changeFileAdded(Q, Queen, A Kind of Magic, 01 One Vision.m4a)",
          "move(/music/staging/Q/Queen/A Kind of Magic/01 One Vision.flac, /music/flac/Q/Queen/A Kind of Magic/01 One Vision.flac)",
          "changeOwnersAdded(Q, Queen, A Kind of Magic, 02 A Kind of Magic.mp3, freddie)",
          "changeOwnersAdded(Q, Queen, A Kind of Magic, 02 A Kind of Magic.m4a, freddie)",
          "checkin(/music/staging/Q/Queen/A Kind of Magic/02 A Kind of Magic.flac)",
          "encode(/music/staging/Q/Queen/A Kind of Magic/02 A Kind of Magic.flac, /music/encoded/mp3/Q/Queen/A Kind of Magic/02 A Kind of Magic.mp3)",
          "changeFileAdded(Q, Queen, A Kind of Magic, 02 A Kind of Magic.mp3)",
          "encode(/music/staging/Q/Queen/A Kind of Magic/02 A Kind of Magic.flac, /music/encoded/m4a/Q/Queen/A Kind of Magic/02 A Kind of Magic.m4a)",
          "changeFileAdded(Q, Queen, A Kind of Magic, 02 A Kind of Magic.m4a)",
          "move(/music/staging/Q/Queen/A Kind of Magic/02 A Kind of Magic.flac, /music/flac/Q/Queen/A Kind of Magic/02 A Kind of Magic.flac)",
          "changeOwnersAdded(Q, Queen, A Kind of Magic, 03 One Year of Love.mp3, freddie)",
          "changeOwnersAdded(Q, Queen, A Kind of Magic, 03 One Year of Love.m4a, freddie)",
          "checkin(/music/staging/Q/Queen/A Kind of Magic/03 One Year of Love.flac)",
          "encode(/music/staging/Q/Queen/A Kind of Magic/03 One Year of Love.flac, /music/encoded/mp3/Q/Queen/A Kind of Magic/03 One Year of Love.mp3)",
          "changeFileAdded(Q, Queen, A Kind of Magic, 03 One Year of Love.mp3)",
          "encode(/music/staging/Q/Queen/A Kind of Magic/03 One Year of Love.flac, /music/encoded/m4a/Q/Queen/A Kind of Magic/03 One Year of Love.m4a)",
          "changeFileAdded(Q, Queen, A Kind of Magic, 03 One Year of Love.m4a)",
          "move(/music/staging/Q/Queen/A Kind of Magic/03 One Year of Love.flac, /music/flac/Q/Queen/A Kind of Magic/03 One Year of Love.flac)",
          "changeOwnersAdded(Q, Queen, Innuendo, 01 Innuendo.mp3, brian, freddie)",
          "changeOwnersAdded(Q, Queen, Innuendo, 01 Innuendo.m4a, brian, freddie)",
          "checkin(/music/staging/Q/Queen/Innuendo/01 Innuendo.flac)",
          "encode(/music/staging/Q/Queen/Innuendo/01 Innuendo.flac, /music/encoded/mp3/Q/Queen/Innuendo/01 Innuendo.mp3)",
          "changeFileAdded(Q, Queen, Innuendo, 01 Innuendo.mp3)",
          "encode(/music/staging/Q/Queen/Innuendo/01 Innuendo.flac, /music/encoded/m4a/Q/Queen/Innuendo/01 Innuendo.m4a)",
          "changeFileAdded(Q, Queen, Innuendo, 01 Innuendo.m4a)",
          "move(/music/staging/Q/Queen/Innuendo/01 Innuendo.flac, /music/flac/Q/Queen/Innuendo/01 Innuendo.flac)",
          "changeOwnersAdded(Q, Queen, Innuendo, 02 Im Going Slightly Mad.mp3, brian, freddie)",
          "changeOwnersAdded(Q, Queen, Innuendo, 02 Im Going Slightly Mad.m4a, brian, freddie)",
          "checkin(/music/staging/Q/Queen/Innuendo/02 Im Going Slightly Mad.flac)",
          "encode(/music/staging/Q/Queen/Innuendo/02 Im Going Slightly Mad.flac, /music/encoded/mp3/Q/Queen/Innuendo/02 Im Going Slightly Mad.mp3)",
          "changeFileAdded(Q, Queen, Innuendo, 02 Im Going Slightly Mad.mp3)",
          "encode(/music/staging/Q/Queen/Innuendo/02 Im Going Slightly Mad.flac, /music/encoded/m4a/Q/Queen/Innuendo/02 Im Going Slightly Mad.m4a)",
          "changeFileAdded(Q, Queen, Innuendo, 02 Im Going Slightly Mad.m4a)",
          "move(/music/staging/Q/Queen/Innuendo/02 Im Going Slightly Mad.flac, /music/flac/Q/Queen/Innuendo/02 Im Going Slightly Mad.flac)",
          "changeOwnersAdded(Q, Queen, Innuendo, 03 Headlong.mp3, brian, freddie)",
          "changeOwnersAdded(Q, Queen, Innuendo, 03 Headlong.m4a, brian, freddie)",
          "checkin(/music/staging/Q/Queen/Innuendo/03 Headlong.flac)",
          "encode(/music/staging/Q/Queen/Innuendo/03 Headlong.flac, /music/encoded/mp3/Q/Queen/Innuendo/03 Headlong.mp3)",
          "changeFileAdded(Q, Queen, Innuendo, 03 Headlong.mp3)",
          "encode(/music/staging/Q/Queen/Innuendo/03 Headlong.flac, /music/encoded/m4a/Q/Queen/Innuendo/03 Headlong.m4a)",
          "changeFileAdded(Q, Queen, Innuendo, 03 Headlong.m4a)",
          "move(/music/staging/Q/Queen/Innuendo/03 Headlong.flac, /music/flac/Q/Queen/Innuendo/03 Headlong.flac)",
          "changeOwnersAdded(S, Slayer, South of Heaven, 01 South of Heaven.mp3, brian, freddie)",
          "changeOwnersAdded(S, Slayer, South of Heaven, 01 South of Heaven.m4a, brian, freddie)",
          "checkin(/music/staging/S/Slayer/South of Heaven/01 South of Heaven.flac)",
          "encode(/music/staging/S/Slayer/South of Heaven/01 South of Heaven.flac, /music/encoded/mp3/S/Slayer/South of Heaven/01 South of Heaven.mp3)",
          "changeFileAdded(S, Slayer, South of Heaven, 01 South of Heaven.mp3)",
          "encode(/music/staging/S/Slayer/South of Heaven/01 South of Heaven.flac, /music/encoded/m4a/S/Slayer/South of Heaven/01 South of Heaven.m4a)",
          "changeFileAdded(S, Slayer, South of Heaven, 01 South of Heaven.m4a)",
          "move(/music/staging/S/Slayer/South of Heaven/01 South of Heaven.flac, /music/flac/S/Slayer/South of Heaven/01 South of Heaven.flac)",
          "changeOwnersAdded(S, Slayer, South of Heaven, 02 Silent Scream.mp3, brian, freddie)",
          "changeOwnersAdded(S, Slayer, South of Heaven, 02 Silent Scream.m4a, brian, freddie)",
          "checkin(/music/staging/S/Slayer/South of Heaven/02 Silent Scream.flac)",
          "encode(/music/staging/S/Slayer/South of Heaven/02 Silent Scream.flac, /music/encoded/mp3/S/Slayer/South of Heaven/02 Silent Scream.mp3)",
          "changeFileAdded(S, Slayer, South of Heaven, 02 Silent Scream.mp3)",
          "encode(/music/staging/S/Slayer/South of Heaven/02 Silent Scream.flac, /music/encoded/m4a/S/Slayer/South of Heaven/02 Silent Scream.m4a)",
          "changeFileAdded(S, Slayer, South of Heaven, 02 Silent Scream.m4a)",
          "move(/music/staging/S/Slayer/South of Heaven/02 Silent Scream.flac, /music/flac/S/Slayer/South of Heaven/02 Silent Scream.flac)",
          "changeOwnersAdded(S, Slayer, South of Heaven, 03 Live Undead.mp3, brian, freddie)",
          "changeOwnersAdded(S, Slayer, South of Heaven, 03 Live Undead.m4a, brian, freddie)",
          "checkin(/music/staging/S/Slayer/South of Heaven/03 Live Undead.flac)",
          "encode(/music/staging/S/Slayer/South of Heaven/03 Live Undead.flac, /music/encoded/mp3/S/Slayer/South of Heaven/03 Live Undead.mp3)",
          "changeFileAdded(S, Slayer, South of Heaven, 03 Live Undead.mp3)",
          "encode(/music/staging/S/Slayer/South of Heaven/03 Live Undead.flac, /music/encoded/m4a/S/Slayer/South of Heaven/03 Live Undead.m4a)",
          "changeFileAdded(S, Slayer, South of Heaven, 03 Live Undead.m4a)",
          "move(/music/staging/S/Slayer/South of Heaven/03 Live Undead.flac, /music/flac/S/Slayer/South of Heaven/03 Live Undead.flac)",
          "cachesRefreshed()",
          "done()"
        )

        briansTree.value
          .toPaths()
          .map(_._1)
          .flatMap(_.lastOption) should contain theSameElementsAs Seq(
          "Q",
          "Queen",
          "A Kind of Magic",
          "01 One Vision.mp3",
          "02 A Kind of Magic.mp3",
          "03 One Year of Love.mp3",
          "Innuendo",
          "01 Innuendo.mp3",
          "02 Im Going Slightly Mad.mp3",
          "03 Headlong.mp3",
          "S",
          "Slayer",
          "South of Heaven",
          "01 South of Heaven.mp3",
          "02 Silent Scream.mp3",
          "03 Live Undead.mp3"
        )
        freddiesTree.value
          .toPaths()
          .map(_._1)
          .flatMap(_.lastOption) should contain theSameElementsAs Seq(
          "Q",
          "Queen",
          "A Kind of Magic",
          "01 One Vision.m4a",
          "02 A Kind of Magic.m4a",
          "03 One Year of Love.m4a",
          "Innuendo",
          "01 Innuendo.m4a",
          "02 Im Going Slightly Mad.m4a",
          "03 Headlong.m4a",
          "S",
          "Slayer",
          "South of Heaven",
          "01 South of Heaven.m4a",
          "02 Silent Scream.m4a",
          "03 Live Undead.m4a"
        )
        fs.entries should haveTheSameEntriesAs(
          fs.expected(expectedEntriesAfterUnownedCheckin: _*)
        )
      }
    }

    "not allow staged albums to overwrite a flac album" in { c =>
      implicit val materializer: Materializer = c.materializer
      val fs: JFS = c.jfs
      fs.add(entriesBeforeCheckin ++ lowerCaseFlacEntries: _*)
      for {
        _ <- c.users(user("brian"), user("freddie"))
        _ <- c.devices(
          device("brian", MP3, "brianmp3"),
          device("freddie", M4A, "freddiem4a")
        )
        _ <- c.addAlbums("Queen" -> Seq(A_KIND_OF_MAGIC))
        _ <- c.ownAlbums("brian", "Queen", A_KIND_OF_MAGIC, INNUENDO)
        _ <- c.ownAlbums("freddie", "Queen", A_KIND_OF_MAGIC)
        _ <- c.ownAlbums("freddie", "Slayer", SOUTH_OF_HEAVEN)
        _ <- c.cacheMediator.refreshCaches()
        source <- c.commandProcessor.submit(
          CheckinCommand(List(c.staging("")), allowUnowned = true)
        )
        messages <- source.runFold(Seq.empty[String])(_ :+ _)
        briansTree <- c.fileSystemTreeCache.latest("brianmp3")
        freddiesTree <- c.fileSystemTreeCache.latest("freddiem4a")
      } yield {
        briansTree.value
          .toPaths()
          .map(_._1)
          .flatMap(_.lastOption) should contain theSameElementsAs Seq(
          "Q",
          "Queen",
          "A Kind of Magic",
          "01 One Vision.mp3",
          "02 A Kind of Magic.mp3",
          "03 One Year of Love.mp3"
        )
        freddiesTree.value
          .toPaths()
          .map(_._1)
          .flatMap(_.lastOption) should contain theSameElementsAs Seq(
          "Q",
          "Queen",
          "A Kind of Magic",
          "01 One Vision.m4a",
          "02 A Kind of Magic.m4a",
          "03 One Year of Love.m4a"
        )
        messages should contain theSameElementsInOrderAs Seq(
          "overwrite(/music/staging/Q/Queen/A Kind of Magic/01 One Vision.flac, /music/flac/Q/Queen/A Kind of Magic/01 One Vision.flac)",
          "overwrite(/music/staging/Q/Queen/A Kind of Magic/02 A Kind of Magic.flac, /music/flac/Q/Queen/A Kind of Magic/02 A Kind of Magic.flac)",
          "overwrite(/music/staging/Q/Queen/A Kind of Magic/03 One Year of Love.flac, /music/flac/Q/Queen/A Kind of Magic/03 One Year of Love.flac)",
          "done()"
        )
        fs.entries should contain theSameElementsAs fs.expected(
          expectedEntriesAfterOverwriteCheckin: _*
        )
      }
    }
  }

}
