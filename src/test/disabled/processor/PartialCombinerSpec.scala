package processor

import org.scalatest.{Matchers, WordSpec}

class PartialCombinerSpec extends WordSpec with Matchers {

  "A number generator partial combiner" should {
    "return 1 for when passed a OneGenerator" in {
      NumberGeneratorCombiner(OneGenerator) should ===(1)
    }
    "return 2 for when passed a TwoGenerator" in {
      NumberGeneratorCombiner(OneGenerator) should ===(1)
    }
    "throw an exception when passed a DodgyGenerator" in {
      an[IllegalStateException] should be thrownBy NumberGeneratorCombiner(
        DodgyGenerator
      )
    }
  }

  sealed trait NumberGenerator
  object OneGenerator extends NumberGenerator
  object TwoGenerator extends NumberGenerator
  object DodgyGenerator extends NumberGenerator

  trait NumberGeneratorFactory
      extends PartialFunctionFactory[NumberGenerator, Int]

  object numberOneGeneratorFactory extends NumberGeneratorFactory {
    override def apply(): PartialFunction[NumberGenerator, Int] = {
      case OneGenerator => 1
    }
  }

  object numberTwoGeneratorFactory extends NumberGeneratorFactory {
    override def apply(): PartialFunction[NumberGenerator, Int] = {
      case OneGenerator => 1
    }
  }

  object NumberGeneratorCombiner
      extends PartialCombiner[NumberGenerator, Int, NumberGeneratorFactory](
        Seq(numberOneGeneratorFactory, numberTwoGeneratorFactory)
      ) {
    override def errorBuilder(in: NumberGenerator): String = "Ouch"
  }
}
