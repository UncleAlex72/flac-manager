/*
 * Copyright 2018 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package processor

import org.apache.pekko.stream.Materializer
import changes.LabelledTree._
import commands.{AddDeviceCommand, CheckinCommand, RemoveDeviceCommand}
import devices.Extension.{M4A, MP3}
import files.RepositoryEntry.Dsl._
import org.scalatest.{EitherValues, OptionValues}
import testfilesystem.FS.Permissions

import scala.concurrent.Future

/** Created by alex on 18/11/14.
  */
class DeviceCommandSpec
    extends AbstractCommandSpec
    with EitherValues
    with OptionValues {

  val A_KIND_OF_MAGIC: Album = Album(
    "A Kind of Magic",
    Tracks("One Vision", "A Kind of Magic", "One Year of Love")
  )

  val INNUENDO: Album = Album(
    "Innuendo",
    Tracks("Innuendo", "Im Going Slightly Mad", "Headlong")
  )

  val SOUTH_OF_HEAVEN: Album = Album(
    "South of Heaven",
    Tracks("South of Heaven", "Silent Scream", "Live Undead")
  )

  val entries = Repos(
    flac = Artists(
      "Queen" -> Seq(A_KIND_OF_MAGIC, INNUENDO),
      "Slayer" -> Albums(SOUTH_OF_HEAVEN)
    ),
    encoded = Artists(
      "Queen" -> Albums(A_KIND_OF_MAGIC, INNUENDO),
      "Slayer" -> Albums(SOUTH_OF_HEAVEN)
    )
  )

  "Adding a device" should {
    "make sure the device identifier is unique" in { c =>
      implicit val materializer: Materializer = c.materializer
      for {
        _ <- c.users(user("brian"), user("freddie"))
        _ <- c.devices(
          device("brian", MP3, "brianmp3"),
          device("freddie", M4A, "freddiem4a")
        )
        source <- c.commandProcessor.submit(
          AddDeviceCommand("Brian mp3", "brian", "mp3")
        )
        messages <- source.runFold(Seq.empty[String])(_ :+ _)
        devices <- c.deviceDao.allDevices()
      } yield {
        devices.map(_.identifier) should contain theSameElementsAs Seq(
          "brianmp3",
          "freddiem4a",
          "brianmp31"
        )
        messages should contain theSameElementsInOrderAs Seq(
          "deviceAdded(Brian mp3)",
          "done()"
        )
      }
    }
    "not allow a device to be added if another device with the same display name already exists" in {
      c =>
        implicit val materializer: Materializer = c.materializer
        for {
          _ <- c.users(user("brian"), user("freddie"))
          _ <- c.devices(
            device("brian", MP3, "brian mp3"),
            device("freddie", M4A, "freddie m4a")
          )
          source <- c.commandProcessor.submit(
            AddDeviceCommand("brian mp3", "brian", "mp3")
          )
          messages <- source.runFold(Seq.empty[String])(_ :+ _)
          devices <- c.deviceDao.allDevices()
        } yield {
          devices.map(_.identifier) should contain theSameElementsAs Seq(
            "brian mp3",
            "freddie m4a"
          )
          messages should contain theSameElementsInOrderAs Seq(
            "deviceNameAlreadyExists(brian mp3)",
            "done()"
          )
        }
    }
    "make sure the device is properly initialised" in { c =>
      implicit val materializer: Materializer = c.materializer
      val fs: JFS = c.jfs
      fs.add(entries: _*)
      for {
        _ <- c.users(user("brian"), user("freddie"))
        _ <- c.addAlbums(
          "Queen" -> Seq(A_KIND_OF_MAGIC, INNUENDO),
          "Slayer" -> Seq(SOUTH_OF_HEAVEN)
        )
        _ <- c.ownAlbums("brian", "Queen", A_KIND_OF_MAGIC, INNUENDO)
        _ <- c.ownAlbums("freddie", "Queen", A_KIND_OF_MAGIC)
        _ <- c.ownAlbums("freddie", "Slayer", SOUTH_OF_HEAVEN)
        _ <- c.cacheMediator.refreshCaches()
        source <- c.commandProcessor.submit(
          AddDeviceCommand("brian mp3", "brian", "mp3")
        )
        messages <- source.runFold(Seq.empty[String])(_ :+ _)
        devices <- c.deviceDao.allDevices()
        briansTree <- c.fileSystemTreeCache.latest("brianmp3")
      } yield {
        briansTree.value
          .toPaths()
          .map(_._1)
          .flatMap(_.lastOption) should contain theSameElementsAs Seq(
          "Q",
          "Queen",
          "A Kind of Magic",
          "01 One Vision.mp3",
          "02 A Kind of Magic.mp3",
          "03 One Year of Love.mp3",
          "Innuendo",
          "01 Innuendo.mp3",
          "02 Im Going Slightly Mad.mp3",
          "03 Headlong.mp3"
        )
        devices.map(_.identifier) should contain theSameElementsAs Seq(
          "brianmp3"
        )
        messages should contain theSameElementsInOrderAs Seq(
          "deviceAdded(brian mp3)",
          "done()"
        )
      }
    }
  }

  "Removing a device" should {
    "remove the device from all caches and the database" in { c =>
      implicit val materializer: Materializer = c.materializer
      val fs: JFS = c.jfs
      fs.add(entries: _*)
      for {
        _ <- c.users(user("brian"), user("freddie"))
        _ <- c.devices(
          device("brian", MP3, "brian mp3"),
          device("freddie", M4A, "freddie m4a")
        )
        _ <- c.addAlbums(
          "Queen" -> Seq(A_KIND_OF_MAGIC, INNUENDO),
          "Slayer" -> Seq(SOUTH_OF_HEAVEN)
        )
        _ <- c.ownAlbums("brian", "Queen", A_KIND_OF_MAGIC, INNUENDO)
        _ <- c.ownAlbums("freddie", "Queen", A_KIND_OF_MAGIC)
        _ <- c.ownAlbums("freddie", "Slayer", SOUTH_OF_HEAVEN)
        source <- c.commandProcessor.submit(RemoveDeviceCommand("brian mp3"))
        messages <- source.runFold(Seq.empty[String])(_ :+ _)
        devices <- c.deviceDao.allDevices()
        briansFsTree <- c.fileSystemTreeCache.latest("brian mp3")
        briansDeviceTree <- c.deviceTreeCache.latest("brian mp3")
      } yield {
        briansFsTree should ===(None)
        briansDeviceTree should ===(None)
        devices.map(_.identifier) should contain theSameElementsAs Seq(
          "freddie m4a"
        )
        messages should contain theSameElementsInOrderAs Seq(
          "deviceRemoved(brian mp3)",
          "done()"
        )
      }
    }
  }
}
