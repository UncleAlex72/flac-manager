package processor

import org.apache.pekko.NotUsed
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.Source
import org.apache.pekko.stream.{ActorMaterializer, Materializer}
import com.typesafe.config.ConfigFactory
import logging.Messages
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.{EitherValues, FutureOutcome, Matchers, fixture}

import scala.collection.immutable.Seq
import scala.concurrent.Future

class ActionExecutorImplSpec
    extends fixture.AsyncWordSpec
    with Matchers
    with MockitoSugar
    with EitherValues {

  "The action executor" should {
    "return the correct messages for the delete file action" in { f =>
      f(mock[DeleteFileAction]).map { messages =>
        messages should contain theSameElementsInOrderAs Seq(Messages.noOwners())
      }
    }

    "return the correct messages for the checkin file action" in { f =>
      f(mock[CheckinFileAction]).map { messages =>
        messages should contain theSameElementsInOrderAs Seq(
          Messages.multiActionRequired()
        )
      }
    }

    "throw an error for an unknown action type" in { f =>
      an[IllegalStateException] shouldBe thrownBy(f(mock[CheckoutFileAction]))
    }
  }

  override def withFixture(test: OneArgAsyncTest): FutureOutcome = {
    implicit val actorSystem: ActorSystem =
      ActorSystem.create("test", ConfigFactory.defaultReference())
    implicit val materializer: Materializer = ActorMaterializer()

    val deletedPartialActionExecutor = new PartialActionExecutor {
      override def apply()
          : PartialFunction[Action, Source[Message, NotUsed]] = {
        case _: DeleteFileAction => Source.single(Messages.noOwners())
      }
    }
    val checkinFileActionExecutor = new PartialActionExecutor {
      override def apply()
          : PartialFunction[Action, Source[Message, NotUsed]] = {
        case _: CheckinFileAction =>
          Source.single(Messages.multiActionRequired())
      }
    }

    val actionExecutor = new ActionExecutorImpl(
      deletedPartialActionExecutor,
      checkinFileActionExecutor
    )

    val fixtureParam: FixtureParam = action => {
      actionExecutor(action).runFold(Seq.empty[Message])(_ :+ _)
    }

    test(fixtureParam).onCompletedThen { _ =>
      actorSystem.terminate()
    }
  }

  override type FixtureParam = Action => Future[Seq[Message]]

}
