/*
 * Copyright 2018 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package processor

import java.nio.file.{Files, StandardCopyOption}
import java.time.{Clock, Instant}

import org.apache.pekko.NotUsed
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.Source
import org.apache.pekko.stream.{ActorMaterializer, Materializer}
import com.mohiva.play.silhouette.api.LoginInfo
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.StrictLogging
import commands.PathAndRepository
import commands.RepositoryType.{FlacRepositoryType, StagingRepositoryType}
import devices.Extension.{M4A, MP3}
import devices._
import files.RepositoryEntry.Dsl._
import files._
import org.scalatest.{EitherValues, Matchers, fixture}
import play.api.Configuration
import play.api.i18n.{DefaultMessagesApi, Lang, MessagesApi}
import play.api.libs.json.OWrites
import testfilesystem.FsEntryMatchers
import uk.co.unclealex.mongodb.ID
import uk.co.unclealex.psm.{User, UserDao}

import scala.concurrent.{ExecutionContext, Future}

/** Created by alex on 18/11/14.
  */
class AbstractCommandSpec
    extends fixture.AsyncWordSpec
    with FsEntryMatchers
    with Matchers
    with TestRepositories[TestCommandComponents]
    with StrictLogging {

  override def generate(
      testFs: JFS,
      testRepositories: Repositories
  ): TestCommandComponents = new TestCommandComponents {

    object SimpleLossyEncoder extends LossyEncoder {
      def encode(stagingFile: StagingFile, targetFile: TempFile): Unit = {
        Files.copy(
          stagingFile.absolutePath,
          targetFile.absolutePath,
          StandardCopyOption.REPLACE_EXISTING
        )
      }
    }

    override implicit val lang: Lang = Lang.defaultLang
    override implicit val jfs: JFS = testFs
    private val config: Config = ConfigFactory.defaultReference()
    override implicit val actorSystem: ActorSystem = ActorSystem("test", config)
    override implicit val materializer: Materializer = ActorMaterializer()
    override implicit def ec: ExecutionContext = actorSystem.dispatcher
    override val repositories: Repositories = testRepositories

    override val changeDao = new InMemoryChangeDao()
    override val changesTreeSnapshotDao =
      new InMemoryChangesTreeSnapshotDao()(materializer, ec)

    override val userDao: UserDao = new InMemoryDao[User]() with UserDao {
      override def sort(): Option[Ordering[User]] = None

      override def findByLoginInfo(loginInfo: LoginInfo): Future[Option[User]] =
        one(_.loginInfo == loginInfo)

      override def findAll(): Future[Seq[User]] = all()
    }

    override val deviceDao: DeviceDao = new InMemoryDao[Device] with DeviceDao {
      override def sort(): Option[Ordering[Device]] = None

      override def allDevices(): Future[Seq[Device]] = all()

      override def streamDevices(): Source[Device, NotUsed] = streamAll()

      override def findByDisplayName(
          displayName: String
      ): Future[Option[Device]] = one(_.displayName == displayName)

      override def findByIdentifier(
          identifier: String
      ): Future[Option[Device]] = one(_.identifier == identifier)

      override def removeById(id: ID): Future[Int] = remove(id)

      override def updateLastSynchronised(
          identifier: String,
          lastSynchronised: Instant
      ): Future[Option[Device]] =
        findByIdentifier(identifier)
    }
    override def configuration: Configuration =
      Configuration(underlying = config)

    override val clock: Clock = Clock.systemDefaultZone()
    override val fileSystem: FileSystem = new ProtectionAwareFileSystem(
      FileSystem()
    )
    override val messagesApi: MessagesApi = new DefaultMessagesApi() {
      override protected def noMatch(key: String, args: Seq[Any])(implicit
          lang: Lang
      ): String = {
        s"$key(${args.mkString(", ")})"
      }
    }
    override val encoders: Map[LossyExtension, LossyEncoder] =
      Map(MP3 -> SimpleLossyEncoder, M4A -> SimpleLossyEncoder)
  }

  def user(name: String): User = User(
    None,
    LoginInfo("", ""),
    Some(name),
    None,
    None,
    None,
    None,
    None,
    None,
    None
  )
  def device(
      owner: String,
      extension: LossyExtension,
      identifier: String
  ): Device =
    Device(None, identifier, identifier, owner, extension, None, None, None)
}

trait TestCommandComponents
    extends CommandComponents
    with EitherValues
    with StrictLogging {

  lazy val changesTreeCache: ChangesTreeCache = {
    implicit val writes: OWrites[LabelledTree[ChangeNode]] =
      LabelledTree.labelledTreeWrites
    new JsonCacheImpl(new InMemoryCache[LossyExtension, ChangesTree]())
      with ChangesTreeCache
  }
  lazy val deviceTreeCache: DeviceTreeCache = {
    implicit val writes: OWrites[LabelledTree[DeviceNode]] =
      LabelledTree.labelledTreeWrites
    new JsonCacheImpl(new InMemoryCache[String, DeviceTree]())
      with DeviceTreeCache
  }
  lazy val fileSystemTreeCache: FileSystemTreeCache = {
    implicit val writes: OWrites[LabelledTree[FsNode]] =
      LabelledTree.labelledTreeWrites
    new JsonCacheImpl((new InMemoryCache[String, FileSystemTree]()))
      with FileSystemTreeCache
  }

  lazy val changesReplayer: ChangesReplayer =
    (timestamp: Instant, extension: LossyExtension) => {
      changeDao
        .changesUntil(timestamp, extension)
        .runFold(ChangesTree(extension))(_.add(_))
    }

  private def initialise[V](
      persister: V => Future[V],
      vs: Seq[V]
  ): Future[Seq[V]] = {
    val empty: Future[Seq[V]] = Future.successful(Seq.empty)
    vs.foldLeft(empty) { (eacc, v) =>
      for {
        acc <- eacc
        newV <- persister(v)
      } yield {
        acc :+ newV
      }
    }
  }
  def users(users: User*): Future[Seq[User]] = initialise(userDao.store, users)
  def devices(devices: Device*): Future[Seq[Device]] =
    initialise(deviceDao.store, devices)
  def changes(changes: Change*): Future[Seq[Change]] =
    initialise(changeService.add, changes)

  def ownArtist(user: String, artists: Artists): Future[Seq[Change]] = {
    val empty: Future[Seq[Change]] = Future.successful(Seq.empty)
    artists.albumsByArtist.foldLeft(empty) { case (eacc, (artist, albums)) =>
      for {
        acc <- eacc
        changes <- ownAlbums(user, artist, albums: _*)
      } yield {
        acc ++ changes
      }
    }
  }

  type ChangeInfoBuilder =
    (String, String, Int, Int, Int, Int, String) => ChangeInfo

  private def staticChangeInfoBuilder(
      changeInfo: ChangeInfo
  ): ChangeInfoBuilder = (_, _, _, _, _, _, _) => changeInfo

  private val fileAddedChangeInfoBuilder: ChangeInfoBuilder =
    (artist, album, disc, totalDiscs, track, totalTracks, title) => {
      val metadata = Metadata(
        albumArtistSort = artist,
        albumArtist = artist,
        album = album,
        artist = artist,
        artistSort = artist,
        title = title,
        totalDiscs = totalDiscs,
        totalTracks = totalTracks,
        discNumber = disc,
        albumArtistId = artist,
        albumId = album,
        artistId = artist,
        trackId = Some(title),
        asin = Some(title),
        trackNumber = track,
        length = track * 100
      )
      FileAdded(metadata)
    }

  private def initialiseAlbums(
      changeInfoBuilder: ChangeInfoBuilder,
      artist: String,
      albums: Seq[Album]
  ): Future[Seq[Change]] = {
    def changesForAlbum(album: Album): Seq[Change] = {
      val discs = album.discs
      val totalDiscs = discs.tracks.size
      for {
        extension <- Seq(MP3, M4A)
        (tracks, discIdx) <- discs.tracks.zipWithIndex
        totalTracks = tracks.size
        (track, trackIdx) <- tracks.titles.zipWithIndex
      } yield {
        val albumTitle =
          if (discs.sameId || discs.tracks.size == 1) album.title
          else f"${album.title} ${discIdx + 1}%02d"
        val change = Change(
          None,
          extension,
          artist.substring(0, 1),
          artist,
          albumTitle,
          f"${trackIdx + 1}%02d $track.${extension.extension}",
          clock.instant(),
          changeInfoBuilder(
            artist,
            albumTitle,
            discIdx + 1,
            totalDiscs,
            trackIdx + 1,
            totalTracks,
            track
          )
        )
        logger.info(s"Adding change $change")
        change
      }
    }
    changes(albums.flatMap(changesForAlbum): _*)
  }

  def ownAlbums(
      user: String,
      artist: String,
      albums: Album*
  ): Future[Seq[Change]] = {
    initialiseAlbums(
      staticChangeInfoBuilder(OwnersAdded(Set(user))),
      artist,
      albums
    )
  }

  def addAlbums(artistAndAlbums: (String, Seq[Album])*): Future[Seq[Change]] = {
    val empty: Future[Seq[Change]] = Future.successful(Seq.empty)
    artistAndAlbums.foldLeft(empty) { case (eacc, (artist, albums)) =>
      for {
        acc <- eacc
        changes <- initialiseAlbums(fileAddedChangeInfoBuilder, artist, albums)
      } yield {
        acc ++ changes
      }
    }
  }

  def removeAlbums(
      artistAndAlbums: (String, Seq[Album])*
  ): Future[Seq[Change]] = {
    val changeInfo = FileDeleted
    val empty: Future[Seq[Change]] = Future.successful(Seq.empty)
    artistAndAlbums.foldLeft(empty) { case (eacc, (artist, albums)) =>
      for {
        acc <- eacc
        changes <- initialiseAlbums(
          staticChangeInfoBuilder(changeInfo),
          artist,
          albums
        )
      } yield {
        acc ++ changes
      }
    }
  }

  def staging(path: String): PathAndRepository = {
    PathAndRepository(
      repositories.staging
        .directory(jfs.getPath(path))
        .toEither
        .toOption
        .get
        .relativePath,
      StagingRepositoryType
    )
  }

  def flac(path: String): PathAndRepository = {
    PathAndRepository(
      repositories.flac
        .directory(jfs.getPath(path))
        .toEither
        .toOption
        .get
        .relativePath,
      FlacRepositoryType
    )
  }

}
