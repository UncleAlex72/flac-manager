/*
 * Copyright 2018 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package processor

import org.apache.pekko.stream.Materializer
import changes.LabelledTree._
import commands.{OwnCommand, UnownCommand}
import devices.Extension.{M4A, MP3}
import files.RepositoryEntry.Dsl._
import org.scalatest.{EitherValues, OptionValues}
import testfilesystem.FS.Permissions

import scala.collection.SortedSet

/** Created by alex on 18/11/14.
  */
class OwnAndUnownCommandSpec
    extends AbstractCommandSpec
    with EitherValues
    with OptionValues {

  val A_KIND_OF_MAGIC: Album = Album(
    "A Kind of Magic",
    Tracks("One Vision", "A Kind of Magic", "One Year of Love")
  )

  val INNUENDO: Album = Album(
    "Innuendo",
    Tracks("Innuendo", "Im Going Slightly Mad", "Headlong")
  )

  val SOUTH_OF_HEAVEN: Album = Album(
    "South of Heaven",
    Tracks("South of Heaven", "Silent Scream", "Live Undead")
  )

  val entries = Repos(
    flac = Artists(
      "Queen" -> Albums(A_KIND_OF_MAGIC),
      "Slayer" -> Albums(SOUTH_OF_HEAVEN)
    ),
    staging = Map(
      Permissions.OwnerReadAndWrite -> Artists("Queen" -> Albums(INNUENDO))
    ),
    encoded = Artists(
      "Queen" -> Albums(A_KIND_OF_MAGIC),
      "Slayer" -> Albums(SOUTH_OF_HEAVEN)
    )
  )

  "Owning an album in the staging directory" should {
    "own it but not encode it" in { c =>
      implicit val materializer: Materializer = c.materializer
      val fs = c.jfs
      fs.add(entries: _*)
      for {
        _ <- c.users(user("brian"), user("freddie"))
        _ <- c.devices(
          device("brian", MP3, "brian mp3"),
          device("freddie", M4A, "freddie m4a")
        )
        _ <- c.addAlbums(
          "Queen" -> Seq(A_KIND_OF_MAGIC),
          "Slayer" -> Seq(SOUTH_OF_HEAVEN)
        )
        _ <- c.ownAlbums("brian", "Queen", A_KIND_OF_MAGIC)
        _ <- c.ownAlbums("freddie", "Queen", A_KIND_OF_MAGIC)
        _ <- c.ownAlbums("freddie", "Slayer", SOUTH_OF_HEAVEN)
        source <- c.commandProcessor.submit(
          OwnCommand(
            List(c.staging("Q/Queen/Innuendo")),
            List("brian", "freddie")
          )
        )
        messages <- source.runFold(Seq.empty[String])(_ :+ _)
        briansTree <- c.fileSystemTreeCache.latest("brian mp3")
        freddiesTree <- c.fileSystemTreeCache.latest("freddie m4a")
        owners <- c.ownerService.listOwners()
      } yield {
        messages should contain theSameElementsInOrderAs List(
          "changeOwnersAdded(Q, Queen, Innuendo, 01 Innuendo.mp3, brian, freddie)",
          "changeOwnersAdded(Q, Queen, Innuendo, 01 Innuendo.m4a, brian, freddie)",
          "changeOwnersAdded(Q, Queen, Innuendo, 02 Im Going Slightly Mad.mp3, brian, freddie)",
          "changeOwnersAdded(Q, Queen, Innuendo, 02 Im Going Slightly Mad.m4a, brian, freddie)",
          "changeOwnersAdded(Q, Queen, Innuendo, 03 Headlong.mp3, brian, freddie)",
          "changeOwnersAdded(Q, Queen, Innuendo, 03 Headlong.m4a, brian, freddie)",
          "cachesRefreshed()",
          "done()"
        )
        owners.toSeq.sortBy(_._1) should contain theSameElementsAs Seq(
          "Q/Queen/A Kind of Magic/01 One Vision" -> SortedSet(
            "brian",
            "freddie"
          ),
          "Q/Queen/A Kind of Magic/02 A Kind of Magic" -> SortedSet(
            "brian",
            "freddie"
          ),
          "Q/Queen/A Kind of Magic/03 One Year of Love" -> SortedSet(
            "brian",
            "freddie"
          ),
          "Q/Queen/Innuendo/01 Innuendo" -> SortedSet("brian", "freddie"),
          "Q/Queen/Innuendo/02 Im Going Slightly Mad" -> SortedSet(
            "brian",
            "freddie"
          ),
          "Q/Queen/Innuendo/03 Headlong" -> SortedSet("brian", "freddie"),
          "S/Slayer/South of Heaven/01 South of Heaven" -> SortedSet("freddie"),
          "S/Slayer/South of Heaven/02 Silent Scream" -> SortedSet("freddie"),
          "S/Slayer/South of Heaven/03 Live Undead" -> SortedSet("freddie")
        )

        briansTree.value
          .toPaths()
          .map(_._1)
          .flatMap(_.lastOption) should contain theSameElementsAs Seq(
          "Q",
          "Queen",
          "A Kind of Magic",
          "01 One Vision.mp3",
          "02 A Kind of Magic.mp3",
          "03 One Year of Love.mp3"
        )
        freddiesTree.value
          .toPaths()
          .map(_._1)
          .flatMap(_.lastOption) should contain theSameElementsAs Seq(
          "Q",
          "Queen",
          "A Kind of Magic",
          "01 One Vision.m4a",
          "02 A Kind of Magic.m4a",
          "03 One Year of Love.m4a",
          "S",
          "Slayer",
          "South of Heaven",
          "01 South of Heaven.m4a",
          "02 Silent Scream.m4a",
          "03 Live Undead.m4a"
        )
        val actualEntries = fs.entries
        val expectedEntries = fs.expected(entries: _*)
        actualEntries should haveTheSameEntriesAs(expectedEntries)
      }
    }
  }

  "Owning an album in the flac directory" should {
    "own it and add it to the relevant devices" in { c =>
      implicit val materializer: Materializer = c.materializer
      val fs = c.jfs
      fs.add(entries: _*)
      for {
        _ <- c.users(user("brian"), user("freddie"))
        _ <- c.devices(
          device("brian", MP3, "brian mp3"),
          device("freddie", M4A, "freddie m4a")
        )
        _ <- c.addAlbums(
          "Queen" -> Seq(A_KIND_OF_MAGIC),
          "Slayer" -> Seq(SOUTH_OF_HEAVEN)
        )
        _ <- c.ownAlbums("freddie", "Queen", A_KIND_OF_MAGIC)
        _ <- c.ownAlbums("freddie", "Slayer", SOUTH_OF_HEAVEN)
        source <- c.commandProcessor.submit(
          OwnCommand(
            List(c.flac("Q/Queen/A Kind of Magic")),
            List("brian", "freddie")
          )
        )
        messages <- source.runFold(Seq.empty[String])(_ :+ _)
        briansTree <- c.fileSystemTreeCache.latest("brian mp3")
        freddiesTree <- c.fileSystemTreeCache.latest("freddie m4a")
        owners <- c.ownerService.listOwners()
      } yield {
        messages should contain theSameElementsInOrderAs Seq(
          "changeOwnersAdded(Q, Queen, A Kind of Magic, 01 One Vision.mp3, brian, freddie)",
          "changeOwnersAdded(Q, Queen, A Kind of Magic, 01 One Vision.m4a, brian, freddie)",
          "changeOwnersAdded(Q, Queen, A Kind of Magic, 02 A Kind of Magic.mp3, brian, freddie)",
          "changeOwnersAdded(Q, Queen, A Kind of Magic, 02 A Kind of Magic.m4a, brian, freddie)",
          "changeOwnersAdded(Q, Queen, A Kind of Magic, 03 One Year of Love.mp3, brian, freddie)",
          "changeOwnersAdded(Q, Queen, A Kind of Magic, 03 One Year of Love.m4a, brian, freddie)",
          "cachesRefreshed()",
          "done()"
        )
        owners.toSeq.sortBy(_._1) should contain theSameElementsAs Seq(
          "Q/Queen/A Kind of Magic/01 One Vision" -> SortedSet(
            "brian",
            "freddie"
          ),
          "Q/Queen/A Kind of Magic/02 A Kind of Magic" -> SortedSet(
            "brian",
            "freddie"
          ),
          "Q/Queen/A Kind of Magic/03 One Year of Love" -> SortedSet(
            "brian",
            "freddie"
          ),
          "S/Slayer/South of Heaven/01 South of Heaven" -> SortedSet("freddie"),
          "S/Slayer/South of Heaven/02 Silent Scream" -> SortedSet("freddie"),
          "S/Slayer/South of Heaven/03 Live Undead" -> SortedSet("freddie")
        )

        briansTree.value
          .toPaths()
          .map(_._1)
          .flatMap(_.lastOption) should contain theSameElementsAs Seq(
          "Q",
          "Queen",
          "A Kind of Magic",
          "01 One Vision.mp3",
          "02 A Kind of Magic.mp3",
          "03 One Year of Love.mp3"
        )
        freddiesTree.value
          .toPaths()
          .map(_._1)
          .flatMap(_.lastOption) should contain theSameElementsAs Seq(
          "Q",
          "Queen",
          "A Kind of Magic",
          "01 One Vision.m4a",
          "02 A Kind of Magic.m4a",
          "03 One Year of Love.m4a",
          "S",
          "Slayer",
          "South of Heaven",
          "01 South of Heaven.m4a",
          "02 Silent Scream.m4a",
          "03 Live Undead.m4a"
        )
        val actualEntries = fs.entries
        val expectedEntries = fs.expected(entries: _*)
        actualEntries should haveTheSameEntriesAs(expectedEntries)
      }
    }
  }

  "Owning an album in the staging directory that had been previously deleted and unowned" should {
    "reown it" in { c =>
      implicit val materializer: Materializer = c.materializer
      val fs = c.jfs
      fs.add(entries: _*)
      for {
        _ <- c.users(user("brian"), user("freddie"))
        _ <- c.devices(
          device("brian", MP3, "brian mp3"),
          device("freddie", M4A, "freddie m4a")
        )
        _ <- c.removeAlbums("Queen" -> Seq(INNUENDO))
        _ <- c.addAlbums(
          "Queen" -> Seq(A_KIND_OF_MAGIC),
          "Slayer" -> Seq(SOUTH_OF_HEAVEN)
        )
        _ <- c.ownAlbums("freddie", "Queen", A_KIND_OF_MAGIC)
        _ <- c.ownAlbums("freddie", "Slayer", SOUTH_OF_HEAVEN)
        source <- c.commandProcessor.submit(
          OwnCommand(List(c.staging("Q/Queen/Innuendo")), List("brian"))
        )
        messages <- source.runFold(Seq.empty[String])(_ :+ _)
        briansTree <- c.fileSystemTreeCache.latest("brian mp3")
        freddiesTree <- c.fileSystemTreeCache.latest("freddie m4a")
        owners <- c.ownerService.listOwners()
      } yield {
        messages should contain theSameElementsInOrderAs Seq(
          "changeOwnersAdded(Q, Queen, Innuendo, 01 Innuendo.mp3, brian)",
          "changeOwnersAdded(Q, Queen, Innuendo, 01 Innuendo.m4a, brian)",
          "changeOwnersAdded(Q, Queen, Innuendo, 02 Im Going Slightly Mad.mp3, brian)",
          "changeOwnersAdded(Q, Queen, Innuendo, 02 Im Going Slightly Mad.m4a, brian)",
          "changeOwnersAdded(Q, Queen, Innuendo, 03 Headlong.mp3, brian)",
          "changeOwnersAdded(Q, Queen, Innuendo, 03 Headlong.m4a, brian)",
          "cachesRefreshed()",
          "done()"
        )
        owners.toSeq.sortBy(_._1) should contain theSameElementsAs Seq(
          "Q/Queen/A Kind of Magic/01 One Vision" -> SortedSet("freddie"),
          "Q/Queen/A Kind of Magic/02 A Kind of Magic" -> SortedSet("freddie"),
          "Q/Queen/A Kind of Magic/03 One Year of Love" -> SortedSet("freddie"),
          "Q/Queen/Innuendo/01 Innuendo" -> SortedSet("brian"),
          "Q/Queen/Innuendo/02 Im Going Slightly Mad" -> SortedSet("brian"),
          "Q/Queen/Innuendo/03 Headlong" -> SortedSet("brian"),
          "S/Slayer/South of Heaven/01 South of Heaven" -> SortedSet("freddie"),
          "S/Slayer/South of Heaven/02 Silent Scream" -> SortedSet("freddie"),
          "S/Slayer/South of Heaven/03 Live Undead" -> SortedSet("freddie")
        )

        briansTree.value.toPaths().map(_._1).flatMap(_.lastOption) should be(
          empty
        )

        freddiesTree.value
          .toPaths()
          .map(_._1)
          .flatMap(_.lastOption) should contain theSameElementsAs Seq(
          "Q",
          "Queen",
          "A Kind of Magic",
          "01 One Vision.m4a",
          "02 A Kind of Magic.m4a",
          "03 One Year of Love.m4a",
          "S",
          "Slayer",
          "South of Heaven",
          "01 South of Heaven.m4a",
          "02 Silent Scream.m4a",
          "03 Live Undead.m4a"
        )
        val actualEntries = fs.entries
        val expectedEntries = fs.expected(entries: _*)
        actualEntries should haveTheSameEntriesAs(expectedEntries)
      }
    }
  }

  "Unowning an album in the flac directory" should {
    "unown it and remove it from the relevant devices" in { c =>
      implicit val materializer: Materializer = c.materializer
      val fs = c.jfs
      fs.add(entries: _*)
      for {
        _ <- c.users(user("brian"), user("freddie"))
        _ <- c.devices(
          device("brian", MP3, "brian mp3"),
          device("freddie", M4A, "freddie m4a")
        )
        _ <- c.addAlbums(
          "Queen" -> Seq(A_KIND_OF_MAGIC),
          "Slayer" -> Seq(SOUTH_OF_HEAVEN)
        )
        _ <- c.ownAlbums("brian", "Queen", A_KIND_OF_MAGIC)
        _ <- c.ownAlbums("freddie", "Queen", A_KIND_OF_MAGIC)
        _ <- c.ownAlbums("freddie", "Slayer", SOUTH_OF_HEAVEN)
        source <- c.commandProcessor.submit(
          UnownCommand(List(c.flac("S")), List("freddie"))
        )
        messages <- source.runFold(Seq.empty[String])(_ :+ _)
        briansTree <- c.fileSystemTreeCache.latest("brian mp3")
        freddiesTree <- c.fileSystemTreeCache.latest("freddie m4a")
        owners <- c.ownerService.listOwners()
      } yield {
        messages should contain theSameElementsInOrderAs Seq(
          "changeOwnersRemoved(S, Slayer, South of Heaven, 01 South of Heaven.mp3, freddie)",
          "changeOwnersRemoved(S, Slayer, South of Heaven, 01 South of Heaven.m4a, freddie)",
          "changeOwnersRemoved(S, Slayer, South of Heaven, 02 Silent Scream.mp3, freddie)",
          "changeOwnersRemoved(S, Slayer, South of Heaven, 02 Silent Scream.m4a, freddie)",
          "changeOwnersRemoved(S, Slayer, South of Heaven, 03 Live Undead.mp3, freddie)",
          "changeOwnersRemoved(S, Slayer, South of Heaven, 03 Live Undead.m4a, freddie)",
          "cachesRefreshed()",
          "done()"
        )
        owners.toSeq.sortBy(_._1) should contain theSameElementsAs Seq(
          "Q/Queen/A Kind of Magic/01 One Vision" -> SortedSet(
            "brian",
            "freddie"
          ),
          "Q/Queen/A Kind of Magic/02 A Kind of Magic" -> SortedSet(
            "brian",
            "freddie"
          ),
          "Q/Queen/A Kind of Magic/03 One Year of Love" -> SortedSet(
            "brian",
            "freddie"
          )
        )

        briansTree.value
          .toPaths()
          .map(_._1)
          .flatMap(_.lastOption) should contain theSameElementsAs Seq(
          "Q",
          "Queen",
          "A Kind of Magic",
          "01 One Vision.mp3",
          "02 A Kind of Magic.mp3",
          "03 One Year of Love.mp3"
        )
        freddiesTree.value
          .toPaths()
          .map(_._1)
          .flatMap(_.lastOption) should contain theSameElementsAs Seq(
          "Q",
          "Queen",
          "A Kind of Magic",
          "01 One Vision.m4a",
          "02 A Kind of Magic.m4a",
          "03 One Year of Love.m4a"
        )
        val actualEntries = fs.entries
        val expectedEntries = fs.expected(entries: _*)
        actualEntries should haveTheSameEntriesAs(expectedEntries)
      }
    }
  }

  "Unowning an album in the staging directory" should {
    "unown it" in { c =>
      implicit val materializer: Materializer = c.materializer
      val fs = c.jfs
      fs.add(entries: _*)
      for {
        _ <- c.users(user("brian"), user("freddie"))
        _ <- c.devices(
          device("brian", MP3, "brian mp3"),
          device("freddie", M4A, "freddie m4a")
        )
        _ <- c.addAlbums(
          "Queen" -> Seq(A_KIND_OF_MAGIC),
          "Slayer" -> Seq(SOUTH_OF_HEAVEN)
        )
        _ <- c.ownAlbums("brian", "Queen", A_KIND_OF_MAGIC)
        _ <- c.ownAlbums("freddie", "Queen", INNUENDO)
        _ <- c.ownAlbums("freddie", "Slayer", SOUTH_OF_HEAVEN)
        _ <- c.cacheMediator.refreshCaches()
        source <- c.commandProcessor.submit(
          UnownCommand(
            List(c.staging("Q/Queen/Innuendo")),
            List("brian", "freddie")
          )
        )
        messages <- source.runFold(Seq.empty[String])(_ :+ _)
        briansTree <- c.fileSystemTreeCache.latest("brian mp3")
        freddiesTree <- c.fileSystemTreeCache.latest("freddie m4a")
        owners <- c.ownerService.listOwners()
      } yield {
        messages should contain theSameElementsInOrderAs Seq(
          "changeOwnersRemoved(Q, Queen, Innuendo, 01 Innuendo.mp3, brian, freddie)",
          "changeOwnersRemoved(Q, Queen, Innuendo, 01 Innuendo.m4a, brian, freddie)",
          "changeOwnersRemoved(Q, Queen, Innuendo, 02 Im Going Slightly Mad.mp3, brian, freddie)",
          "changeOwnersRemoved(Q, Queen, Innuendo, 02 Im Going Slightly Mad.m4a, brian, freddie)",
          "changeOwnersRemoved(Q, Queen, Innuendo, 03 Headlong.mp3, brian, freddie)",
          "changeOwnersRemoved(Q, Queen, Innuendo, 03 Headlong.m4a, brian, freddie)",
          "cachesRefreshed()",
          "done()"
        )
        owners.toSeq.sortBy(_._1) should contain theSameElementsAs Seq(
          "Q/Queen/A Kind of Magic/01 One Vision" -> SortedSet("brian"),
          "Q/Queen/A Kind of Magic/02 A Kind of Magic" -> SortedSet("brian"),
          "Q/Queen/A Kind of Magic/03 One Year of Love" -> SortedSet("brian"),
          "S/Slayer/South of Heaven/01 South of Heaven" -> SortedSet("freddie"),
          "S/Slayer/South of Heaven/02 Silent Scream" -> SortedSet("freddie"),
          "S/Slayer/South of Heaven/03 Live Undead" -> SortedSet("freddie")
        )

        briansTree.value
          .toPaths()
          .map(_._1)
          .flatMap(_.lastOption) should contain theSameElementsAs Seq(
          "Q",
          "Queen",
          "A Kind of Magic",
          "01 One Vision.mp3",
          "02 A Kind of Magic.mp3",
          "03 One Year of Love.mp3"
        )
        freddiesTree.value
          .toPaths()
          .map(_._1)
          .flatMap(_.lastOption) should contain theSameElementsAs Seq(
          "S",
          "Slayer",
          "South of Heaven",
          "01 South of Heaven.m4a",
          "02 Silent Scream.m4a",
          "03 Live Undead.m4a"
        )
        val actualEntries = fs.entries
        val expectedEntries = fs.expected(entries: _*)
        actualEntries should haveTheSameEntriesAs(expectedEntries)
      }
    }
  }

}
