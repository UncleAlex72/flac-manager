package processor

import org.scalatest.{Matchers, WordSpec}

class SequentialJobIdGeneratorSpec extends WordSpec with Matchers {

  val jobIdGenerator = new SequentialJobIdGenerator(Some(201))

  "The sequential jobId generator" should {
    "generate sequential 4-digit lowercase hex strings" in {
      Seq(
        jobIdGenerator.generate(),
        jobIdGenerator.generate(),
        jobIdGenerator.generate()
      ) should contain theSameElementsInOrderAs Seq("00c9", "00ca", "00cb")
    }
  }
}
