/*
 * Copyright 2017 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package client

import java.nio.file.{FileSystem, Files, Path}
import java.util.UUID
import cats.data.NonEmptyList
import client.ParametersParserSpec._
import com.github.marschall.memoryfilesystem.MemoryFileSystemBuilder
import commands.Command
import io.circe.Json.obj
import io.circe.{Json, JsonObject}
import io.circe.syntax._
import org.scalatest._
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

/**
  * Created by alex on 21/04/17
  **/
class ParametersParserSpec extends AnyWordSpec with Matchers with EitherValues:


  val datumFilename: String = random()
  implicit val fs: FileSystem = mkfs(
    datumFilename = datumFilename,
    stagingPaths = Seq("N" / "Napalm Death" / "Scum" / "01 You Suffer.mp3"),
    flacPaths = Seq("Q" / "Queen" / "A Night at the Opera" / "01 Death on Two Legs.mp3"),
    initialOwners = Map("freddie" -> Seq("123456"), "brian" -> Seq("987654")))

  "Checking out files" should:
    "allow directories in the flac directory to be checked out" in:
      parse(datumFilename, "checkout", "music/flac/Q") should ===(Right(
        obj(
          "command" -> "checkout".asJson,
          "relativeDirectories" -> Json.arr(jsonFlac("Q"))
        ))
      )
    "not allow directories in the staging directory to be checked out" in:
      parse(datumFilename, "checkout", "music/staging/N/Napalm Death") should
        ===(Left(NonEmptyList.one("/data/music/staging/N/Napalm Death is not relative to one of the following repositories: FLAC")))
    "not allow files in the flac directory to be checked out" in:
      parse(datumFilename, "checkout", "music/flac/Q/Queen/A Night at the Opera/01 Death on Two Legs.mp3") should
        ===(Left(NonEmptyList.one("/data/music/flac/Q/Queen/A Night at the Opera/01 Death on Two Legs.mp3 is not a directory.")))

  "Checking in files" should:
    "allow directories in the staging directory to be checked in" in:
      parse(datumFilename, "checkin", "--allow-unowned", "music/staging/N") should ===(Right(
        obj(
          "command" -> "checkin".asJson,
          "relativeDirectories" -> Json.arr(jsonStaging("N")),
          "owners" -> Json.arr(),
          "allowUnowned" -> true.asJson
        ))
      )
    "allow owners to be added during checked in" in:
      parse(datumFilename, "checkin", "--owners", "brian,freddie", "music/staging/N") should ===(Right(
        obj(
          "command" -> "checkin".asJson,
          "relativeDirectories" -> Json.arr(jsonStaging("N")),
          "owners" -> Seq("brian", "freddie").asJson,
          "allowUnowned" -> false.asJson
        ))
      )
    "not allow directories in the flac directory to be checked in" in:
      parse(datumFilename, "checkin", "music/flac/Q") should
        ===(Left(NonEmptyList.one("/data/music/flac/Q is not relative to one of the following repositories: STAGING")))
    "not allow files in the staging directory to be checked out" in:
      parse(datumFilename, "checkin", "music/staging/N/Napalm Death/Scum/01 You Suffer.mp3") should
        ===(Left(NonEmptyList.one("/data/music/staging/N/Napalm Death/Scum/01 You Suffer.mp3 is not a directory.")))
    "not allow directories not under the /music directory to be checked out" in:
      parse(datumFilename, "checkin", "/video") should
        ===(Left(NonEmptyList.one("/video is not relative to a datum file.")))

  "Owning files" should:
    "allow directories in the staging or flac directory to be owned" in:
      parse(datumFilename, "own", "--owners", "alex,trevor", "music/staging/N", "music/flac/Q") should ===(Right(
        obj(
          "command" -> "own".asJson,
          "relativeDirectories" -> Json.arr(jsonStaging("N"), jsonFlac("Q")),
          "owners" -> Json.arr("alex".asJson, "trevor".asJson)
        )
      ))
    "not allow files in be owned" in:
      parse(
        datumFilename,
        "own",
        "--owners",
        "alex,trevor",
        "music/staging/N/Napalm Death/Scum/01 You Suffer.mp3") should ===(Left(NonEmptyList.one(
        "/data/music/staging/N/Napalm Death/Scum/01 You Suffer.mp3 is not a directory.")
      ))

  def jsonRepo(repoType: String)(relativePath: String): Json =
    obj("path" -> relativePath.asJson, "repositoryType" -> repoType.asJson)

  def jsonStaging: String => Json = jsonRepo("STAGING")
  def jsonFlac: String => Json = jsonRepo("FLAC")

  def parse(datumFilename: String, args: String*)(implicit fs: FileSystem): Either[NonEmptyList[String], Json] =
    ParametersParser(datumFilename, args).toEither.map { command =>
      command.asJson
    }

  def random(): String = s".${UUID.randomUUID()}"

  def mkfs(
            datumFilename: String = "",
            stagingPaths: Seq[PathBuilder] = Seq.empty,
            flacPaths: Seq[PathBuilder] = Seq.empty,
            initialOwners: Map[String, Seq[String]] = Map.empty): FileSystem =
    val fs = MemoryFileSystemBuilder.
      newEmpty().
      setCurrentWorkingDirectory("/data").
      build(datumFilename)
    val dataPath = fs.getPath("/data")
    val initialOwnersPath = dataPath.resolve("owners.json")
    val musicPath = dataPath.resolve("music")
    val videoPath = dataPath.resolve("/video")
    val datumPath = musicPath.resolve(datumFilename)
    val flacPath = musicPath.resolve("flac")
    val stagingPath = musicPath.resolve("staging")
    Seq(flacPath, stagingPath, videoPath).foreach(Files.createDirectories(_))
    val allFiles = Seq(datumPath, initialOwnersPath) ++ stagingPaths.map(_.toPath(stagingPath)) ++ flacPaths.map(_.toPath(flacPath))
    allFiles.foreach { file =>
      Files.createDirectories(file.getParent)
      Files.createFile(file)
    }
    val writer = Files.newBufferedWriter(initialOwnersPath)
    val albumIdsByOwner = initialOwners.asJson
    writer.write(albumIdsByOwner.spaces2)
    writer.close()
    fs

object ParametersParserSpec {

  case class PathBuilder(segments: Seq[String]):
    def /(s: String) = PathBuilder(segments :+ s)
    def toPath(parent: Path): Path = segments.foldLeft(parent) { (path, segment) =>
      path.resolve(segment)
    }

  implicit class StringImplicits(path: String):
    def /(s: String): PathBuilder = PathBuilder(Seq(path, s))
}