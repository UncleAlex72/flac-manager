package client

import com.github.pjfanning.pekkohttpcirce.FailFastCirceSupport
import org.apache.pekko.NotUsed
import org.apache.pekko.http.scaladsl.HttpExt
import org.apache.pekko.http.scaladsl.marshalling.Marshal
import org.apache.pekko.http.scaladsl.model.HttpMethods.{GET, PUT}
import org.apache.pekko.http.scaladsl.model.{HttpRequest, MessageEntity, Uri}
import org.apache.pekko.http.scaladsl.unmarshalling.Unmarshal
import org.apache.pekko.stream.Materializer
import org.apache.pekko.stream.scaladsl.{FileIO, Source}
import org.apache.pekko.util.ByteString
import delta._
import io.circe.Encoder._
import routes.PathSegment
import routes.PathSegment._

import java.io.PrintStream
import java.net.URI
import java.nio.file.{Files, Path}
import java.time.Clock
import scala.annotation.tailrec
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Using}

case class Synchroniser(
                         http: HttpExt,
                         clock: Clock,
                         devicesAndPaths: Seq[(String, Path)],
                         serverUri: URI,
                         out: PrintStream)
                       (implicit ec: ExecutionContext, materializer: Materializer) extends FailFastCirceSupport
                       with PathSegment.ClientExtensions:

  def synchronise(): Future[Unit] =
    devicesAndPaths.foldLeft(Future.successful({})) { case (unit, (deviceIdentifier, path)) =>
        unit.flatMap(_ => new DeviceSynchroniser(deviceIdentifier, path).synchronise())
    }

  private class DeviceSynchroniser(deviceIdentifier: String, rootPath: Path):

    private def findDeltas(): Future[Seq[Delta]] =
      val deltasUrl = serverUri.withPath(DELTAS / FULL_SEGMENT / deviceIdentifier)
      val getDeltasUrlRequest = HttpRequest(method = GET, uri = Uri(deltasUrl))
      http.singleRequest(getDeltasUrlRequest).flatMap { response =>
        response.status.intValue() match
          case 200 =>
            Unmarshal(response).to[Seq[Delta]]
          case status =>
            out.println(s"Received response $status from $deltasUrl. Ignoring this device.")
            Future.successful(Seq.empty)
      }

    private def updateOffset(offset: Int): Future[Unit] =
      out.println(s"Updating last updated offset to  $offset")
      val updateLastSynchronisedUrl =
        serverUri.withPath(DEVICES / deviceIdentifier / OFFSET)
      for
        body <- Marshal(offset).to[MessageEntity]
        request = HttpRequest(method = PUT, uri = Uri(updateLastSynchronisedUrl), entity = body)
        response <- http.singleRequest(request)
      yield
        response.status.intValue() match
          case 200 => out.println(s"Successfully updated the last updated offset to $offset")
          case status => out.println(s"Updating the last updated offset failed with status $status")

    private def updateLastSynchronised(): Future[Unit] =
      out.println(s"Updating last synchronised")
      val updateLastSynchronisedUrl =
        serverUri.withPath(DEVICES / deviceIdentifier / LAST_SYNCHRONISED)
      http.singleRequest(HttpRequest(method = PUT, uri = Uri(updateLastSynchronisedUrl))).map { response =>
        response.status.intValue() match
          case 200 => out.println(s"Successfully updated the last synchronised time")
          case status => out.println(s"Updating the last synchronised time failed with status $status")
      }

    private def processDelta(delta: Delta, index: Int, total: Int): Future[Unit] =
      val fullPath = rootPath.resolve(delta.path)
      out.println(s"Processing change $index of $total")
      delta match
        case rfd: RemoveFileDelta => removeFile(fullPath).flatMap { _ =>
          updateOffset(rfd.ordering)
        }
        case cfd: CreateFileDelta => createFile(fullPath, cfd.url).flatMap { _ =>
          updateOffset(cfd.ordering)
        }

    private def processDeltas(deltas: Seq[Delta]): Future[Unit] =
      val deltaCount = deltas.length
      out.println(s"Found $deltaCount changes.")
      deltas.zipWithIndex.foldLeft(Future.successful({})) { case (acc, (delta, idx)) =>
        acc.flatMap(_ => processDelta(delta, idx + 1, deltaCount))
      }

    def synchronise(): Future[Unit] =
      for
        deltas <- findDeltas()
        _ <- processDeltas(deltas)
        _ <- updateLastSynchronised()
      yield {
      }


    private def removeFile(path: Path): Future[Unit] = Future.successful:
      out.println(s"Removing file $path")
      Files.deleteIfExists(path)
      removeDirectories(path.getParent)

    @tailrec
    private def removeDirectories(path: Path): Unit =
      if Files.isDirectory(path) && Using(Files.list(path)) {
        _.findAny().isEmpty
      }.getOrElse(false) then
        Files.delete(path)
        val parent = path.getParent
        if parent != rootPath then
          removeDirectories(parent)

    private def createFile(path: Path, url: String): Future[Unit] =
      out.println(s"Downloading $url to $path")
      Files.createDirectories(path.getParent)
      Files.deleteIfExists(path)
      val request = HttpRequest(method = GET, uri = Uri(url))
      val musicFileSource: Source[ByteString, NotUsed] = Source.single {
        (request, {})
      }.via(http.superPool()).map(_._1).flatMapConcat:
        case Success(response) =>
          response.entity.dataBytes
        case Failure(ex) => throw ex
      for
        ioResult <- musicFileSource.runWith(FileIO.toPath(path))
        _ <- Future.fromTry(ioResult.status)
      yield {
      }
