/*
 * Copyright 2017 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package client

import com.github.pjfanning.pekkohttpcirce.FailFastCirceSupport
import org.apache.pekko.{Done, NotUsed}
import org.apache.pekko.http.scaladsl.HttpExt
import org.apache.pekko.http.scaladsl.common.{EntityStreamingSupport, JsonEntityStreamingSupport}
import org.apache.pekko.http.scaladsl.marshalling.Marshal
import org.apache.pekko.http.scaladsl.model.HttpMethods.POST
import org.apache.pekko.http.scaladsl.model.{HttpRequest, MessageEntity, Uri}
import org.apache.pekko.http.scaladsl.unmarshalling.Unmarshal
import org.apache.pekko.stream.Materializer
import org.apache.pekko.stream.scaladsl.{Flow, Sink, Source}
import org.apache.pekko.util.ByteString
import commands.{Command, CommandResponse, KeepAliveResponse, MessageResponse}
import io.circe
import io.circe.syntax._
import io.circe._
import routes.PathSegment
import routes.PathSegment._

import java.io.PrintStream
import java.net.URI
import java.nio.charset.StandardCharsets
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

/** Connect to the server and run a command remotely.
  * Created by alex on 17/04/17
  */
object RemoteCommandRunner extends FailFastCirceSupport with PathSegment.ClientExtensions:

  private val jsonStreamingSupport: JsonEntityStreamingSupport =
    EntityStreamingSupport.json()
  def runCommand(http: HttpExt, command: Command, curl: Boolean, serverUri: URI, out: PrintStream)(
      implicit
      materializer: Materializer,
      executionContext: ExecutionContext
  ): Future[Done] =
    val commandUri: String = serverUri.withPath(COMMANDS)
    if curl then
      Future.successful:
        out.println(s"""curl -d '${command.asJson.noSpaces}' -H "Accept: application/json" -H "Content-Type: application/json" $commandUri""")
        Done
    else
      val requestSource: Source[HttpRequest, ?] = Source.future:
        Marshal(command).to[MessageEntity].map { entity =>
          HttpRequest(
            method = POST,
            uri = Uri(commandUri),
            entity = entity
          )
        }
      val httpFlow = Flow[HttpRequest]
        .map((_, {}))
        .via(http.superPool())
        .map(_._1)
        .flatMapConcat:
          case Success(response) => response.entity.dataBytes
          case Failure(ex) => throw ex
      requestSource
        .via(httpFlow)
        .via(jsonStreamingSupport.framingDecoder)
        .mapAsync(1)(bytes => Unmarshal(bytes).to[CommandResponse])
        .flatMapConcat:
          case MessageResponse(message) => Source.single(message)
          case KeepAliveResponse(_) => Source.empty
        .mapMaterializedValue(_ => NotUsed)
        .runForeach(out.println)
