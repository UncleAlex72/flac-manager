/*
 * Copyright 2017 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package client

import java.nio.file.{Files, Path}

import cats.Applicative
import cats.data._
import cats.syntax.option._
import cats.syntax.validated._
import commands._

import scala.collection.immutable.Seq
import scala.io.Source
import scala.util.{Failure, Success, Try}

/**
  * A typeclass used to generate command from command line options.
  * @tparam C A command type.
  */
trait Parameters[C <: Command]:

  type ValidationResult = ValidatedNel[String, C]

  /**
    * Create a new command.
    * @return
    */
  def newCommand(): C

  /**
    * Add an extra directory specified in the command line to the command object.
    *
    * @param repositoryTypes The types of directory, either staging or flac.
    * @param directory The directory to add.
    * @return either a new command object with the extra directory or a list of errors.
    */
  def withExtraDirectory(command: C,
                         datumFilename: String,
                         repositoryTypes: Seq[RepositoryType],
                         directory: Path): ValidationResult

  /**
    * Add an extra device directory specified in the command line to the command object.
    *
    * @param directory The directory to add.
    * @return either a new command object with the extra directory or a list of errors.
    */
  def withExtraDevice(command: C, directory: Path): ValidationResult

  /**
    * Add users specified in the command line to the command object.
    * @param owners The names of the users to add.
    * @return either a new command object with the users or a list of errors.
    */
  def withOwners(command: C, owners: Seq[String]): ValidationResult


  /**
    * Add an unown flag in the command line to the command object.
    * @param unown The value of the unown flag.
    * @return either a new command object with the unown flag or a list of errors.
    */
  def withUnown(command: C, unown: Boolean): ValidationResult

  /**
    * Add an allowUnowned flag in the command line to the command object.
    * @param allowUnowned The value of the allowUnowned flag.
    * @return either a new command object with the allowUnowned flag or a list of errors.
    */
  def withAllowUnowned(command: C, allowUnowned: Boolean): ValidationResult

  /**
    * Add a multi action to the command object.
    * @param multiAction The multi action to add.
    * @return either a new command object with the new multi action or a list of errors.
    */
  def withMultiAction(command: C, multiAction: MultiAction): ValidationResult

  /**
    * Add a number of threads to the command object.
    * @param numberOfThreads The number of threads to add.
    * @return either a new command object with the number of threads or a list of errors.
    */
  def withNumberOfThreads(command: C, numberOfThreads: Int): ValidationResult

  /**
    * Add a display name to the command object.
    * @param command The command
    * @param displayName The display name.
    * @return
    */
  def withDisplayName(command: C, displayName: String): ValidationResult

  /**
    * Add an owner to the command object.
    * @param command The command
    * @param owner The owner.
    * @return
    */
  def withOwner(command: C, owner: String): ValidationResult

  /**
    * Add an owner to the command object.
    * @param command The command
    * @param extension The extension.
    * @return
    */
  def withExtension(command: C, extension: String): ValidationResult

  /**
    * Add an initial ownership map taken from a JSON file.
    * @param command The command.
    * @param file The file containing a JSON object whose keys are user's names and values are lists of MusicBrainz'
    *             album IDs.
    * @return
    */
  def withInitialOwnershipFile(command: C, file: Path): ValidationResult

  /**
    * Run a final check on the supplied command.
    * @return either the command or a list of errors.
    */
  def checkValid(command: C): ValidationResult


/**
  * A base for command classes. This class assumes that everything fails so subclasses only need
  * to override the methods that are pursuant to the the parameter type P.
  *
  * @param commandName The name of the command to be used in reporting errors and help messages.
  * @tparam C A [[Command]] type.
  */
abstract class FailingParameters[C <: Command](val commandName: String) extends Parameters[C]:

  private def fail(message: String): ValidationResult =
    s"The $commandName command does not take $message.".invalidNel

  /**
    * Fail to add an extra directory.
    * @param datumFilename The name of the server's datum file.
    * @param repositoryTypes The type of directory, either staging or flac.
    * @param directory The directory to add.
    * @return either a new command object with the extra directory or a list of errors.
    */
  override def withExtraDirectory(command: C,
                                  datumFilename: String,
                                  repositoryTypes: Seq[RepositoryType],
                                  directory: Path): ValidationResult =
    fail("directory command")

  /**
    * Fail to add any users.
    * @param users The names of the users to add.
    * @return either a new command object with the users or a list of errors.
    */
  override def withOwners(command: C, users: Seq[String]): ValidationResult =
    fail("owner command")

  /**
    * Fail to add an unown flag.
    * @param unown The value of the unown flag.
    * @return either a new command object with the unown flag or a list of errors.
    */
  override def withUnown(command: C, unown: Boolean): ValidationResult =
    fail("an unown flag")


  /**
    * Fail to add an allowUnowned flag.
    * @param allowUnowned The value of the allowUnowned flag.
    * @return either a new command object with the unown flag or a list of errors.
    */
  override def withAllowUnowned(command: C, allowUnowned: Boolean): ValidationResult =
    fail("an allow unowned flag")

  /**
    * Convert a directory in to a relative path and add it to a list of relative directories.
    *
    * @param relativeDirectories The current list of relative directories.
    * @param datumFilename The name of the server's datum file.
    * @param repositoryTypes The directory type, either staging or flac.
    * @param directory The absolute directory to add.
    * @return Either a new list of relative directories containing the new directory or a list of errors.
    */
  def extraDirectory(relativeDirectories: Seq[PathAndRepository],
                     datumFilename: String,
                     repositoryTypes: Seq[RepositoryType],
                     directory: Path): ValidatedNel[String, Seq[PathAndRepository]] =
    DirectoryRelativiser.relativise(datumFilename, repositoryTypes, directory).map { relativeDirectory =>
      relativeDirectories :+ relativeDirectory
    }

  /**
    * Fail to add a multi action to the command object.
    *
    * @param multiAction The multi action to add.
    * @return either a new command object with the new multi action or a list of errors.
    */
  override def withMultiAction(command: C, multiAction: MultiAction): ValidationResult =
    fail("a multi action")

  /**
    * Fail to add a number of threads.
    * @param numberOfThreads The number of threads to add.
    * @return either a new command object with the number of threads or a list of errors.
    */
  override def withNumberOfThreads(command: C, numberOfThreads: Int): ValidationResult =
    fail("a number of threads")

  /**
    * Fail to add a device directory.
    * @param path The path to add.
    * @return either a new command object with the number of threads or a list of errors.
    */
  override def withExtraDevice(command: C, path: Path): ValidationResult =
    fail("a device directory")

  override def withDisplayName(command: C, displayName: String): ValidationResult =
    fail("a display name")

  override def withExtension(command: C, extension: String): ValidationResult =
    fail("an extension")

  override def withOwner(command: C, owner: String): ValidationResult =
    fail("an owner")

  override def withInitialOwnershipFile(command: C, file: Path): ValidationResult =
    fail("an initial ownership file")

  override def checkValid(command: C): ValidationResult =
    command.validNel

object ParametersInstances {

  /**
    * Build command for the `checkin` command.
    */
  object CheckinParameters extends FailingParameters[CheckinCommand]("checkin"):

    override def newCommand(): CheckinCommand = CheckinCommand()

    /**
      * @inheritdoc
      */
    override def withExtraDirectory(command: CheckinCommand,
                                    datumFilename: String,
                                    repositoryTypes: Seq[RepositoryType],
                                    directory: Path): ValidationResult =
      extraDirectory(command.relativeDirectories, datumFilename, repositoryTypes, directory).map { paths =>
        command.copy(relativeDirectories = paths)
      }

    /**
      * Add an allowUnowned flag in the command line to the command object.
      *
      * @param allowUnowned The value of the allowUnowned flag.
      * @return either a new command object with the allowUnowned flag or a list of errors.
      */
    override def withAllowUnowned(command: CheckinCommand, allowUnowned: Boolean): ValidationResult =
      command.copy(allowUnowned = allowUnowned).validNel

    override def withOwners(command: CheckinCommand, owners: Seq[String]): CheckinParameters.ValidationResult =
      command.copy(owners = owners).validNel

  /**
    * Build command for the `checkout` command.
    */
  object CheckoutParameters extends FailingParameters[CheckoutCommand]("checkout"):

    override def newCommand(): CheckoutCommand = CheckoutCommand()

    /**
      * @inheritdoc
      */
    override def withExtraDirectory(
                                     command: CheckoutCommand,
                                     datumFilename: String,
                                     repositoryTypes: Seq[RepositoryType],
                                     directory: Path): ValidationResult =
      extraDirectory(command.relativeDirectories, datumFilename: String, repositoryTypes, directory).map { paths =>
        command.copy(relativeDirectories = paths)
      }

  /**
    * Build command for the `multi` command.
    */
  object MultiParameters extends FailingParameters[MultiDiscCommand]("multi"):

    override def newCommand(): MultiDiscCommand = MultiDiscCommand()

    /**
     * @inheritdoc
     */
    override def withExtraDirectory(
                                     command: MultiDiscCommand,
                                     datumFilename: String,
                                     repositoryTypes: Seq[RepositoryType],
                                     directory: Path): ValidationResult =
      extraDirectory(command.relativeDirectories, datumFilename, repositoryTypes, directory).map { paths =>
        command.copy(relativeDirectories = paths)
      }

    override def withMultiAction(
                                  command: MultiDiscCommand,
                                  multiAction: MultiAction): ValidationResult =
      command.copy(maybeMultiAction = Some(multiAction)).validNel

    override def checkValid(command: MultiDiscCommand): ValidationResult =
      command.maybeMultiAction.map(_ => command).toValidNel("You must supply a multi action")

    /**
     * Build command for the `multi` command.
     */
    object JoinParameters extends FailingParameters[JoinCommand]("join"):

      override def newCommand(): JoinCommand = JoinCommand()

      /**
       * @inheritdoc
       */
      override def withExtraDirectory(
                                       command: JoinCommand,
                                       datumFilename: String,
                                       repositoryTypes: Seq[RepositoryType],
                                       directory: Path): ValidationResult =
        extraDirectory(command.relativeDirectories, datumFilename, repositoryTypes, directory).map { paths =>
          command.copy(relativeDirectories = paths)
        }
  /**
    * Build command for the `own` command.
    */
  object OwnParameters extends FailingParameters[OwnCommand]("own"):

    override def newCommand(): OwnCommand = OwnCommand()

    /**
      * @inheritdoc
      */
    override def withOwners(
                            command: OwnCommand,
                            users: Seq[String]): ValidationResult =
      command.copy(owners = users).validNel

    /**
      * @inheritdoc
      */
    override def withExtraDirectory(
                                     command: OwnCommand,
                                     datumFilename: String,
                                     repositoryTypes: Seq[RepositoryType],
                                     directory: Path): ValidationResult =
      extraDirectory(command.relativeDirectories, datumFilename, repositoryTypes, directory).map { paths =>
        command.copy(relativeDirectories = paths)
      }

  /**
    * Build command for the `unown` command.
    */
  object UnownParameters extends FailingParameters[UnownCommand]("unown"):

    override def newCommand(): UnownCommand = UnownCommand()

    /**
      * @inheritdoc
      */
    override def withOwners(
                            command: UnownCommand,
                            users: Seq[String]): ValidationResult =
      command.copy(owners = users).validNel

    /**
      * @inheritdoc
      */
    override def withExtraDirectory(
                                     command: UnownCommand,
                                     datumFilename: String,
                                     repositoryTypes: Seq[RepositoryType],
                                     directory: Path): ValidationResult =
      extraDirectory(command.relativeDirectories, datumFilename, repositoryTypes, directory).map { paths =>
        command.copy(relativeDirectories = paths)
      }

  /**
   * Build command for the `unown` command.
   */
  object HealthCheckParameters extends FailingParameters[HealthCheckCommand]("healthcheck"):

    override def newCommand(): HealthCheckCommand = HealthCheckCommand()

  object SynchroniseDeviceParameters extends FailingParameters[SynchroniseCommand]("synchronise"):

    override def newCommand(): SynchroniseCommand = SynchroniseCommand()

    override def withExtraDevice(command: SynchroniseCommand, directory: Path): ValidationResult =
      val deviceFile = directory.resolve(".device")
      if !Files.isReadable(deviceFile) then
        s"Cannot find a device file in $directory".invalidNel
      else
        Try:
          val source = Source.fromFile(deviceFile.toString)
          val deviceIdentifier = source.mkString.trim
          source.close()
          deviceIdentifier
        match
          case Success(deviceIdentifier) =>
            command.copy(command.paths + (deviceIdentifier -> directory.toAbsolutePath)).validNel
          case Failure(_) =>
            s"Could not read the contents of $deviceFile".invalidNel
}