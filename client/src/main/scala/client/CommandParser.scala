/*
 * Copyright 2017 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package client

import cats.data.ValidatedNel
import cats.implicits._
import client.ParametersInstances.MultiParameters.JoinParameters
import client.ParametersInstances._
import commands.RepositoryType.{FlacRepositoryType, StagingRepositoryType}
import commands._
import enumeratum.{Enum, EnumEntry}
import scopt.{OptionDef, OptionParser, Read}

import java.nio.file.{FileSystem, Path}
import scala.collection.immutable
import scala.collection.immutable.Seq

/**  A trait that encapsulates how arguments from the command line are parsed.
  *  Created by alex on 17/04/17
  */
sealed trait CommandParser extends EnumEntry:

  /** The name of the command as seen by client users.
    */
  val name: String

  /** Text on what this command does.
    */
  val usageText: String

  /** The text to show for the mandatory owners options, or none if not applicable.
    */
  val maybeMandatoryOwnersDescriptionText: Option[String]

  /** The text to show for the optional owners options, or none if not applicable.
    */
  val maybeOptionalOwnersDescriptionText: Option[String]

  /** The text to describe files, or none if not applicable.
    */
  val maybeDirectoriesDescriptionText: Option[String]

  /** The text to describe device directories, or none if not applicable.
    */
  val maybeDeviceDirectoriesDescriptionText: Option[String]

  /** The text to describe the unown flag, or none if not applicable.
    */
  val maybeUnownText: Option[String]

  /** The text to describe the allow unowned flag, or none if not applicable.
    */
  val maybeAllowUnownedText: Option[String]

  /** The text to describe the number of threads for calibration, or none if not applicable.
    */
  val maybeNumberOfThreadsText: Option[String]

  /** The text for multi action flags.
    */
  val multiActionTexts: Map[MultiAction, String]

  /** The text to describe the initial ownership file.
    */
  val maybeInitialOwnershipFileText: Option[String]

  /** The allowed repository types.
    */
  val repositoryTypes: Seq[RepositoryType] = Seq.empty

  /** Parse arguments from the command line.
    * @param arguments The command line arguments.
    * @param datumFilename The datum filename supplied by the server.
    * @param fs The Java NIO file system.
    * @return Either a JSON RPC payload that can be sent to the server to execute a command or a list of errors.
    */
  def parseArguments(arguments: Seq[String], datumFilename: String)(implicit
      fs: FileSystem
  ): ValidatedNel[String, Command]

/** Used to enumerate and build the available commands.
  */
object CommandParser extends Enum[CommandParser]:

  private[CommandParser] abstract class AbstractCommandParser[C <: Command](
      val parameters: Parameters[C]
  ) extends CommandParser:

    def parseArguments(arguments: Seq[String], datumFilename: String)(implicit
        fs: FileSystem
    ): ValidatedNel[String, Command] =

      implicit val pathParameterReader: Read[Path] = Read.reads(fs.getPath(_))

      implicit def immutableSeqRead[A](implicit
          read: Read[scala.collection.Seq[A]]
      ): Read[Seq[A]] =
        read.map(_.toList)

      val parser: OptionParser[ValidatedNel[String, C]] =
        new scopt.OptionParser[ValidatedNel[String, C]](s"flacman-$name"):
          head(usageText)

          type OptionDefC = OptionDef[Seq[String], ValidatedNel[String, C]]
          def parseOwners(
              optionMutator: OptionDefC => OptionDefC
          ): String => OptionDefC = { text =>
            optionMutator(opt[Seq[String]]('o', "owners"))
              .valueName("<owner1>,<owner2>...")
              .action { (users, eCommand) =>
                eCommand.andThen { command =>
                  parameters.withOwners(command, users)
                }
              }
              .text(text)
          }

          maybeMandatoryOwnersDescriptionText.foreach(parseOwners(_.required()))

          maybeOptionalOwnersDescriptionText.foreach(parseOwners(_.optional()))

          maybeUnownText.foreach { unownText =>
            opt[Unit]("unown")
              .optional()
              .action { (_, eCommand) =>
                eCommand.andThen { command =>
                  parameters.withUnown(command, unown = true)
                }
              }
              .text(unownText)
          }

          maybeAllowUnownedText.foreach { allowUnownedText =>
            opt[Unit]("allow-unowned")
              .optional()
              .action { (_, eCommand) =>
                eCommand.andThen { command =>
                  parameters.withAllowUnowned(command, allowUnowned = true)
                }
              }
              .text(allowUnownedText)
          }

          multiActionTexts.foreach { case (action, text) =>
            opt[Unit](action.token)
              .action { (_, eCommand) =>
                eCommand.andThen { command =>
                  parameters.withMultiAction(command, action)
                }
              }
              .text(text)
          }

          help("help").text("Prints this usage text.")

          maybeDeviceDirectoriesDescriptionText.foreach:
            deviceDirectoriesDescriptionText =>
              arg[Path]("<file>...")
                .unbounded()
                .required()
                .action { (dir, eCommand) =>
                  eCommand.andThen { command =>
                    parameters.withExtraDevice(command, dir)
                  }
                }
                .text(deviceDirectoriesDescriptionText)

          maybeDirectoriesDescriptionText.foreach:
            directoriesDescriptionText =>
              arg[Path]("<file>...")
                .unbounded()
                .required()
                .action { (dir, eCommand) =>
                  eCommand.andThen { command =>
                    if repositoryTypes.isEmpty then
                      s"Command $name does not take file parameters.".invalidNel
                    else
                      parameters.withExtraDirectory(
                        command,
                        datumFilename,
                        repositoryTypes,
                        dir
                      )
                  }
                }
                .text(directoriesDescriptionText)

          maybeInitialOwnershipFileText.foreach { initialiseOwnershipFileText =>
            arg[Path]("<file>")
              .maxOccurs(1)
              .required()
              .action { (file, eCommand) =>
                eCommand.andThen { command =>
                  parameters.withInitialOwnershipFile(command, file)
                }
              }
              .text(initialiseOwnershipFileText)
          }

          maybeNumberOfThreadsText.foreach { numberOfThreadsText =>
            arg[Int]("<numberOfThreads>")
              .maxOccurs(1)
              .optional()
              .action { (threads, eCommand) =>
                eCommand.andThen { command =>
                  parameters.withNumberOfThreads(command, threads)
                }
              }
              .text(numberOfThreadsText)
          }


      val firstPassValidation: ValidatedNel[String, C] =
        parser
          .parse(arguments, parameters.newCommand().validNel)
          .getOrElse("Arguments could not be parsed.".invalidNel)

      val secondPassValidation: ValidatedNel[String, Command] =
        firstPassValidation.andThen { validatedParameters =>
          parameters.checkValid(validatedParameters)
        }
      secondPassValidation

  /** The `own` command
    */
  object OwnCommandParserParser
      extends AbstractCommandParser[OwnCommand](OwnParameters)
      with CommandParser:
    val name = "own"
    val usageText = "Add owners to flac files."
    val maybeMandatoryOwnersDescriptionText: Option[String] = Some(
      "The owners who will own the files."
    )
    val maybeOptionalOwnersDescriptionText: Option[String] = None
    val maybeDirectoriesDescriptionText: Option[String] = Some(
      "The files to be owned."
    )
    val maybeDeviceDirectoriesDescriptionText: Option[String] = None
    val maybeUnownText: Option[String] = None
    val maybeAllowUnownedText: Option[String] = None
    val maybeNumberOfThreadsText: Option[String] = None
    val maybeInitialOwnershipFileText: Option[String] = None
    val multiActionTexts: Map[MultiAction, String] = Map.empty
    override val repositoryTypes: Seq[RepositoryType] =
      Seq(FlacRepositoryType, StagingRepositoryType)

  /** The `unown` command
    */
  object UnOwnCommandParser
      extends AbstractCommandParser[UnownCommand](UnownParameters)
      with CommandParser:
    val name = "unown"
    val usageText = "Remove owners from staged flac files."
    val maybeMandatoryOwnersDescriptionText: Option[String] = Some(
      "The owners who will no longer own the files."
    )
    val maybeOptionalOwnersDescriptionText: Option[String] = None
    val maybeDirectoriesDescriptionText: Option[String] = Some(
      "The files to be unowned."
    )
    val maybeDeviceDirectoriesDescriptionText: Option[String] = None
    val maybeUnownText: Option[String] = None
    val maybeAllowUnownedText: Option[String] = None
    val maybeNumberOfThreadsText: Option[String] = None
    val maybeInitialOwnershipFileText: Option[String] = None
    val multiActionTexts: Map[MultiAction, String] = Map.empty
    override val repositoryTypes: Seq[RepositoryType] =
      Seq(FlacRepositoryType, StagingRepositoryType)

  /** The `checkin` command
    */
  object CheckInCommandParser
      extends AbstractCommandParser[CheckinCommand](CheckinParameters)
      with CommandParser:
    val name = "checkin"
    val usageText = "Check in staged flac files."
    val maybeMandatoryOwnersDescriptionText: Option[String] = None
    val maybeOptionalOwnersDescriptionText: Option[String] = Some(
      "Also add the owners to the files on check in."
    )
    val maybeDirectoriesDescriptionText: Option[String] = Some(
      "The files to be checked in."
    )
    val maybeDeviceDirectoriesDescriptionText: Option[String] = None
    val maybeUnownText: Option[String] = None
    val maybeAllowUnownedText: Option[String] = Some(
      "Allow files without owners to be checked in."
    )
    val maybeNumberOfThreadsText: Option[String] = None
    val maybeInitialOwnershipFileText: Option[String] = None
    val multiActionTexts: Map[MultiAction, String] = Map.empty
    override val repositoryTypes: Seq[RepositoryType] = Seq(
      StagingRepositoryType
    )

  /** The `checkout` command
    */
  object CheckOutCommandParser
      extends AbstractCommandParser[CheckoutCommand](CheckoutParameters)
      with CommandParser:
    val name = "checkout"
    val usageText = "Check out flac files."
    val maybeMandatoryOwnersDescriptionText: Option[String] = None
    val maybeOptionalOwnersDescriptionText: Option[String] = None
    val maybeDirectoriesDescriptionText: Option[String] = Some(
      "The files to be checked out."
    )
    val maybeDeviceDirectoriesDescriptionText: Option[String] = None
    val maybeUnownText: Option[String] = Some(
      "Also unown any checked out files."
    )
    val maybeAllowUnownedText: Option[String] = None
    val maybeNumberOfThreadsText: Option[String] = None
    val maybeInitialOwnershipFileText: Option[String] = None
    val multiActionTexts: Map[MultiAction, String] = Map.empty
    override val repositoryTypes: Seq[RepositoryType] = Seq(FlacRepositoryType)

  object JoinCommandParser
    extends AbstractCommandParser[JoinCommand](JoinParameters)
      with CommandParser:
    val name = "join"
    val usageText = "Join multiple albums."
    val maybeMandatoryOwnersDescriptionText: Option[String] = None
    val maybeOptionalOwnersDescriptionText: Option[String] = None
    val maybeDirectoriesDescriptionText: Option[String] = Some(
      "The files to be joined in order"
    )
    val maybeDeviceDirectoriesDescriptionText: Option[String] = None
    val maybeUnownText: Option[String] = None
    val maybeAllowUnownedText: Option[String] = None
    val maybeNumberOfThreadsText: Option[String] = None
    val maybeInitialOwnershipFileText: Option[String] = None
    val multiActionTexts: Map[MultiAction, String] = Map.empty
    override val repositoryTypes: Seq[RepositoryType] = Seq(StagingRepositoryType)
  /** The `checkin` command
    */
  object MultiCommandParser
      extends AbstractCommandParser[MultiDiscCommand](MultiParameters)
      with CommandParser:
    val name = "multidisc"
    val usageText = "Split or join multiple disc albums."
    val maybeMandatoryOwnersDescriptionText: Option[String] = None
    val maybeOptionalOwnersDescriptionText: Option[String] = None
    val maybeDirectoriesDescriptionText: Option[String] = Some(
      "The files to be split or joined"
    )
    val maybeDeviceDirectoriesDescriptionText: Option[String] = None
    val maybeUnownText: Option[String] = None
    val maybeAllowUnownedText: Option[String] = None
    val maybeNumberOfThreadsText: Option[String] = None
    val maybeInitialOwnershipFileText: Option[String] = None
    val multiActionTexts: Map[MultiAction, String] = Map(
      MultiAction.Join -> "Join albums into one long album",
      MultiAction.Extras -> "Keep albums split but with (Extra) as a prefix for any discs but the first.",
      MultiAction.Rename -> "Keep albums split and rename all but the first album with the disc subtitle."
    )
    override val repositoryTypes: Seq[RepositoryType] = Seq(
      StagingRepositoryType
    )

  object HealthCheckCommandParser
      extends AbstractCommandParser[HealthCheckCommand](HealthCheckParameters)
      with CommandParser:
    val name = "healthcheck"
    val usageText =
      "Check there is a 1-1 correspondence between flac files and compressed files."
    val maybeMandatoryOwnersDescriptionText: Option[String] = None
    val maybeOptionalOwnersDescriptionText: Option[String] = None
    val maybeDirectoriesDescriptionText: Option[String] = None
    val maybeDeviceDirectoriesDescriptionText: Option[String] = None
    val maybeUnownText: Option[String] = None
    val maybeAllowUnownedText: Option[String] = None
    val maybeNumberOfThreadsText: Option[String] = None
    val maybeInitialOwnershipFileText: Option[String] = None
    val multiActionTexts: Map[MultiAction, String] = Map.empty
    override val repositoryTypes: Seq[RepositoryType] = Seq.empty

  /** The `synchronise` command
    */
  object SynchroniseCommandParser
      extends AbstractCommandParser[SynchroniseCommand](SynchroniseDeviceParameters)
      with CommandParser:
    val name = "synchronise"
    val usageText = "Synchronise on or more devices."
    val maybeMandatoryOwnersDescriptionText: Option[String] = None
    val maybeOptionalOwnersDescriptionText: Option[String] = None
    val maybeDirectoriesDescriptionText: Option[String] = None
    val maybeDeviceDirectoriesDescriptionText: Option[String] = Some(
      "Mount points for users' devices."
    )
    val maybeUnownText: Option[String] = None
    val maybeAllowUnownedText: Option[String] = None
    val maybeNumberOfThreadsText: Option[String] = None
    val maybeInitialOwnershipFileText: Option[String] = None
    val multiActionTexts: Map[MultiAction, String] = Map.empty

  val values: immutable.IndexedSeq[CommandParser] = findValues

  /** Find a command by its name.
    * @param commandName The name of the command to find.
    * @return The command with the given name or none if no command could be found.
    */
  def apply(commandName: String): ValidatedNel[String, CommandParser] =
    values
      .find(_.name == commandName)
      .toValidNel(s"$commandName is not a valid command.")
