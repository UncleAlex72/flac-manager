/*
 * Copyright 2017 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package client

import org.apache.pekko.http.scaladsl.HttpExt
import org.apache.pekko.http.scaladsl.model.HttpMethods.GET
import org.apache.pekko.http.scaladsl.model.{HttpRequest, Uri}
import org.apache.pekko.http.scaladsl.unmarshalling.Unmarshal

import java.net.URI
import org.apache.pekko.stream.Materializer
import cats.data.Validated.{Invalid, Valid}
import cats.data._
import cats.implicits._
import com.github.pjfanning.pekkohttpcirce.FailFastCirceSupport
import io.circe.{Decoder, Json}
import routes.PathSegment
import routes.PathSegment.DATUM

import scala.concurrent.{ExecutionContext, Future}

/**
  * A case class used to hold server details gained from configuration and a web service call.
  * @param uri The base URL of the server.
  * @param datumFilename The name of the server's datum file.
  */
case class ServerDetails(uri: URI, datumFilename: String)

/**
  * Get the datum file and server URL from configuration and a web service call.
  **/
object ServerDetails extends FailFastCirceSupport with PathSegment.ClientExtensions:

  /**
    * Eventually find the Flac Manager server.
    * @return The found server details or a list of errors.
    */
  def apply(http: HttpExt, configurationName: String)(implicit materializer: Materializer, ec: ExecutionContext): Future[ValidatedNel[String, ServerDetails]] =
    ClientConfiguration(configurationName) match
      case Valid(serverUri) =>
        findDatumFile(http, Uri(serverUri.toString)).map { validatedDatumFilename =>
          validatedDatumFilename.map { datumFilename => ServerDetails(serverUri, datumFilename)}
        }
      case Invalid(e) => Future.successful(e.invalid)

  private def findDatumFile(http: HttpExt, uri: Uri)(implicit materializer: Materializer, ec: ExecutionContext): Future[ValidatedNel[String, String]] =
    val datumUri = Uri(DATUM).resolvedAgainst(uri)
    val request = HttpRequest(method = GET, uri = datumUri)
    val eventualDatumFileLocation = for
      response <- http.singleRequest(request)
      body <- Unmarshal(response).to[Json]
    yield
      body.as[String] match
        case Right(str) => str.validNel[String]
        case Left(_) => s"The server at $datumUri did not return a json string".invalidNel
    eventualDatumFileLocation.recover:
      case ex => ex.getMessage.invalidNel
