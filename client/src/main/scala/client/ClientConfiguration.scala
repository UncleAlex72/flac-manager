package client

import java.net.URI

import cats.data._
import cats.syntax.validated._
import com.typesafe.config.{ConfigException, ConfigFactory}

import scala.util.{Failure, Success, Try}
object ClientConfiguration:

  def apply(resourceName: String): ValidatedNel[String, URI] =
    val config = ConfigFactory.load(resourceName)
    Try(config.getString("url")) match
      case Success(url) =>
        val urlWithTrailingSlash = if url.endsWith("/") then url else s"$url/"
        val uri = URI.create(urlWithTrailingSlash)
        if uri.isOpaque then
          s"$url is not absolute".invalidNel
        else if !Seq("http", "https").contains(uri.getScheme) then
          s"$url is neither an HTTP nor HTTPS url".invalidNel
        else
          uri.validNel
      case Failure(_: ConfigException.Missing) => "A server URL must be configured in your environment variables".invalidNel
      case Failure(ex) => s"Could not read the server URL: ${ex.getMessage}".invalidNel
