/*
 * Copyright 2017 Alex Jones
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package client
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.http.scaladsl.{Http, HttpExt}
import cats.data.NonEmptyList
import commands.{Command, SynchroniseCommand}
import uk.co.unclealex.futures.Futures._

import java.io.PrintStream
import java.nio.file.{FileSystem, FileSystems}
import java.time.Clock
import java.util.logging.LogManager
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.util.Try

/**
  * The main entry point for all commands. The first argument is expected to be the command name.
  **/
object Client extends App:

  implicit class OptionExtensions[A](maybeValue: Option[A]):
    def failWith(message: String): Try[A] =
      maybeValue.toRight(new IllegalArgumentException(message)).toTry
  LogManager.getLogManager.reset()

  private implicit val system: ActorSystem = ActorSystem()
  import system.dispatcher

  private implicit val fs: FileSystem = FileSystems.getDefault

  val clock = Clock.systemDefaultZone()

  val http: HttpExt = Http()

  try
    val eventualAction: FutureEither[NonEmptyList[String], Unit] = for
      serverDetails <- fe(fetchServerDetails(http))
      commandAndCurl <- fe(parseParameters(serverDetails))
      _ <- fe(runCommand(commandAndCurl._1, commandAndCurl._2, serverDetails))
    yield {}
    Await.result(eventualAction.value, Duration.Inf) match
      case Left(messages) => messages.toList.foreach(System.err.println)
      case _ => println("Finished.")
  finally
    Try(http.shutdownAllConnectionPools())
    Try(system.terminate())

  private type Response[T] = Future[Either[NonEmptyList[String], T]]

  private def fetchServerDetails(http: HttpExt): Response[ServerDetails] =
    ServerDetails(http, "client.conf").map(_.toEither)

  // Parse parameters from the server.
  private def parseParameters(serverDetails: ServerDetails): Response[(Command, Boolean)] =
    Future.successful(ParametersParser(serverDetails.datumFilename, args).toEither)

  // Run the command on the server.
  private def runCommand(command: Command, curl: Boolean, serverDetails: ServerDetails): Response[Unit] =
    val out: PrintStream = System.out
    command match
      case SynchroniseCommand(pathsByDeviceIdentifier) =>
        Synchroniser(http, clock, pathsByDeviceIdentifier.toSeq, serverDetails.uri, out).synchronise().map(u => Right(u))
      case remoteCommand: Command =>
        RemoteCommandRunner.runCommand(http, remoteCommand, curl, serverDetails.uri, out).map(u => Right(u))


