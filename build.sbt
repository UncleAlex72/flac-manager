import com.typesafe.sbt.packager.docker._
import sbt.Keys._
import sbt._
import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations._

resolvers ++= Seq("releases").map(Resolver.sonatypeRepo)
resolvers += "JaudioTagger Repository" at "https://dl.bintray.com/ijabz/maven"
resolvers += "Atlassian Repository" at "https://packages.atlassian.com/mvn/maven-public/"

lazy val mongoDbVersion = "5.0.0"
lazy val _scalaVersion = "3.6.2"
lazy val PekkoVersion = "1.1.3"
lazy val PekkoHttpVersion = "1.1.0"
lazy val scalatestVersion = "3.2.19"

lazy val _scalacOptions = Seq.empty[String]

lazy val shared = (project in file("shared")).settings(
  name := "flac-manager-shared",
  libraryDependencies ++= Seq(
    "com.beachape" %% "enumeratum" % "1.7.5",
    "uk.co.unclealex" %% "futures" % "2.0.0",
    "uk.co.unclealex" %% "string-like" % "2.0.4",
    "com.github.pjfanning" %% "pekko-http-circe" % "3.0.0",
    "uk.co.unclealex" %% "mongodb-scala" % mongoDbVersion,
    "org.scalatest" %% "scalatest" % scalatestVersion % "test",
    "uk.co.unclealex" %% "thank-you-for-the-days" % "2.0.0" % "test"
  )  ++
    Seq(
      "org.apache.pekko" %% "pekko-actor-typed" % PekkoVersion,
      "org.apache.pekko" %% "pekko-stream" % PekkoVersion,
      "org.apache.pekko" %% "pekko-http" % PekkoHttpVersion,
      "org.apache.pekko" %% "pekko-http-xml" % PekkoHttpVersion
    ) ++
    Seq("com.typesafe.scala-logging" %% "scala-logging" % "3.9.5",
        "ch.qos.logback" % "logback-classic" % "1.5.16"),
  scalaVersion := _scalaVersion,
)

lazy val root = (project in file("."))
  .settings(
    name := "flac-manager",
    libraryDependencies ++= Seq(
      "org.scala-lang.modules" %% "scala-xml" % "2.3.0",
      "net.jthink" % "jaudiotagger" % "3.0.1",
      "io.circe" %% "circe-config" % "0.10.1",
      "uk.co.unclealex" %% "thank-you-for-the-days" % "2.0.0" % "test",
      "org.scalatest" %% "scalatest" % scalatestVersion % "test",
      "org.eclipse.jetty" % "jetty-server" % "12.0.16" % "test",
      "uk.co.unclealex" %% "mongodb-scala-test" % mongoDbVersion,
      "org.mockito" % "mockito-core" % "5.15.2" % "test",
      "com.github.marschall" % "memoryfilesystem" % "2.8.1" % "test",
      "commons-io" % "commons-io" % "2.18.0",
      "com.google.guava" % "guava" % "33.4.0-jre"
    ),
    // Disable documentation generation
    sources in (Compile, doc) := Seq.empty,
    publishArtifact in (Compile, packageDoc) := false,
    unmanagedResourceDirectories in Compile += baseDirectory(_ / "resources").value,
    // Docker
    dockerBaseImage := "unclealex72/docker-ffmpeg:latest",
    dockerExposedPorts := Seq(9000),
    dockerEnvVars := Map("PRODUCTION" -> "true"),
    maintainer := "Alex Jones <alex.jones@unclealex.co.uk>",
    dockerRepository := Some("unclealex72"),
    dockerUpdateLatest := true,
    daemonUserUid in Docker := Some("1001"),
    daemonUser in Docker := "music",
    dockerExposedVolumes := Seq("/music"),
    javaOptions in Universal += "-Dpidfile.path=/dev/null",
    scalaVersion := _scalaVersion,
    scalacOptions ++= _scalacOptions
  )
  .enablePlugins(DockerPlugin)
  .dependsOn(shared)
  .enablePlugins(JavaAppPackaging)

lazy val client = (project in file("client"))
  .settings(
    name := "flac-manager-client",
    maintainer := "Alex Jones <alex.jones@unclealex.co.uk>",
    packageSummary := "Flac Manager Client Debian Package",
    packageDescription := "Flac Manager Client Debian Package",
    libraryDependencies ++= Seq(
      "com.beachape" %% "enumeratum" % "1.7.5",
      "com.github.scopt" %% "scopt" % "4.1.0",
      "org.typelevel" %% "cats-core" % "2.13.0",
      "com.github.marschall" % "memoryfilesystem" % "2.8.1" % "test",
      "org.scalatest" %% "scalatest" % scalatestVersion % "test"
    ),
    // Remove the /usr/bin/ symlinks
    linuxPackageSymlinks := linuxPackageSymlinks.value.filterNot {
      linuxSymlink =>
        linuxSymlink.link.startsWith("/usr/bin/")
    },
    version in Debian := ((v: String) =>
      v + (if (v.endsWith("-")) "" else "-") + "build-aj")(version.value),
    debianPackageDependencies := Seq("java17-runtime-headless"),
    scalaVersion := _scalaVersion,
    scalacOptions ++= _scalacOptions
  )
  .enablePlugins(DebianPlugin, DebianDeployPlugin, JavaAppPackaging)
  .dependsOn(shared)

javacOptions ++= Seq("-source", "17", "-target", "17")

run / fork := false
Global / cancelable := false // ctrl-c

/* Releases */
releaseProcess := Seq[ReleaseStep](
  checkSnapshotDependencies, // : ReleaseStep
  inquireVersions, // : ReleaseStep
  runTest, // : ReleaseStep
  setReleaseVersion, // : ReleaseStep
  commitReleaseVersion, // : ReleaseStep, performs the initial git checks
  tagRelease, // : ReleaseStep
  releaseStepCommand("packageBin"), // : ReleaseStep, build server docker image.
  releaseStepCommand("docker:publish"), // : ReleaseStep, build server docker image.
  releaseStepCommand("client/debian:packageBin"), // : ReleaseStep, client build deb file.
  setNextVersion, // : ReleaseStep
  commitNextVersion, // : ReleaseStep
  pushChanges // : ReleaseStep, also checks that an upstream branch is properly configured
)

Global / onChangedBuildSource := ReloadOnSourceChanges
